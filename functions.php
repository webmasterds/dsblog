<?php
/**
 * =====================================================
 * テーマのための関数
 * @package   DS BLOG THEME
 * @author    夢リタ
 * @license   http://creativecommons.org/licenses/by/2.1/jp/
 * @link      http://yumerita.jp/blog
 * @copyright 2014 夢リタ
 * =====================================================
 */

// General require
require get_template_directory() . '/inc/config.php';
require get_template_directory() . '/inc/general.php';
require get_template_directory() . '/inc/cleanup.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/custom-pointer.php';

// tempaltes require
require get_template_directory() . '/inc/scripts.php';
require get_template_directory() . '/inc/wrapper.php';
require get_template_directory() . '/inc/widget.php';
require get_template_directory() . '/inc/color.php';
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
require get_template_directory() . '/inc/theme-modified.php';
require get_template_directory() . '/inc/comments.php';

// wp sass require
require get_template_directory() . '/inc/wp-sass/wp-sass.php';

// shortcode require
require get_template_directory() . '/inc/shortcode.php';
require get_template_directory() . '/inc/shortcodes/tinymce-shortcode.php';

// Theme Customizer require
require get_template_directory() . '/inc/theme-customizer/theme-customization.php';

// plugin install
require get_template_directory() . '/plugins/plugin-install.php';
require get_template_directory() . '/plugins/menu-icons/menu-icons.php';
add_action( 'init', array( 'Menu_Icons', 'load' ) );

// framework require
require get_template_directory() . '/inc/framework.php';
require get_template_directory() . '/inc/affiliate-tools/affiliate-tools.php';

require get_template_directory() . '/inc/github-updater/github-updater.php';

