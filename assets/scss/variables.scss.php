<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage SCSS 変数定義 ファイル
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

global $dsblog_mod;
?>

// variables

// ======================== //
// color
// ======================== //

$gray-darker:               #222; // #222
$gray-dark:                 #333;  // #333
$gray:                      #555; // #555
$gray-light:                #CCC;   // #999
$gray-lighter:              #e8e8e8; // #eee
$gray-lightist:             #f5f5f5; // #eee

// ======================== //
// brand
// ======================== //

$brand-primary:             #428bca;
$brand-success:             #5cb85c;
$brand-info:                #5bc0de;
$brand-warning:             #f0ad4e;
$brand-danger:              #d9534f;

// ======================== //
// wrapper
// ======================== //

$wrapper-width:             <?php echo $dsblog_mod['design_width']; ?>;
$wrapper-bg:                #FFF;
$wrapper-color:             <?php echo $dsblog_mod['font_text_color']; ?>;
$wrapper-padding-top:       1.3em;
$wrapper-padding-bottom:    1.3em;

// ======================== //
// border-width
// ======================== //

$border-width-sm:           1px;
$border-width-md:           ceil(($border-width-sm * 4));
$border-width-lg:           ceil(($border-width-sm * 6));

// ======================== //
// radius
// ======================== //

$border-radius-sm:          <?php echo $dsblog_mod['design_radius']; ?>;
$border-radius-md:          ceil(($border-radius-sm * 2));
$border-radius-lg:          ceil(($border-radius-sm * 6));

// ======================== //
// border-color
// ======================== //

$border-color:              #FFF;

// ======================== //
// box-shadow
// ======================== //

$base-box-shadow:           <?php echo $dsblog_mod['design_shadow']; ?>;
$header-box-shadow:         <?php echo $dsblog_mod['design_shadow']; ?>;
$main-box-shadow:           ceil((<?php echo $dsblog_mod['design_shadow']; ?>*1.8));

// ======================== //
// margin top
// ======================== //

$global-mt-sm :             1em;
$global-mt-md :             1em;
$global-mt-lg :             1em;

// ======================== //
// margin bottom
// ======================== //

$global-mb-sm :             1.0em;
$global-mb-md :             1.4em;
$global-mb-lg :             2.0em;

// ======================== //
// margin right
// ======================== //

$global-mr-sm :             1.0em;
$global-mr-md :             1.4em;
$global-mr-lg :             2.0em;

// ======================== //
// margin left
// ======================== //

$global-ml-sm :             1.0em;
$global-ml-md :             1.4em;
$global-ml-lg :             2.0em;

// ======================== //
// padding
// ======================== //

// padding top
$padding-top-sm :           1.0em;
$padding-top-md :           1.4em;
$padding-top-lg :           2.0em;

// padding bottom
$padding-bottom-sm :        1.0em;
$padding-bottom-md :        1.4em;
$padding-bottom-lg :        2.0em;

// padding right
$padding-right-sm :         1.0em;
$padding-right-md :         1.4em;
$padding-right-lg :         2.0em;

// padding left
$padding-left-sm :          1.0em;
$padding-left-md :          1.4em;
$padding-left-lg :          2.0em;

// ======================== //
// header
// ======================== //

$header-min-height:         110px;
$header-bg-color:           #FFF;

// ======================== //
// widget title
// ======================== //

$widget-title-border-top:   $border-width-md;
$widget-title-border-bottom:$border-width-sm;

// ======================== //
// entry title
// ======================== //

$entry-title-border-top:    $border-width-md;
$entry-title-border-bottom: $border-width-sm;

// ======================== //
// breadcrumbs
// ======================== //

$breadcrumbs-bg:            $gray-lighter;
$breadcrumbs-radius:        $border-radius-md;

// ======================== //
// logo
// ======================== //

$logo-margin-top:           $global-mt-sm;
$logo-margin-bottom:        $global-mb-sm;
$logo-sm-font-size:         1.3em;
$logo-md-font-size:         ceil(($logo-sm-font-size * 1.6));
$logo-lg-font-size:         ceil(($logo-sm-font-size * 3));

// ======================== //
// navbar
// ======================== //

$navbar-mt:                 0;
$navbar-mb:                 0;

// ======================== //
// responsive width
// ======================== //

$responsive-device-width:   320px;

// ======================== //
// Typography
// ======================== //

$font-size-base:            <?php echo $dsblog_mod['font_base_size'] ;?>;
$font-size-large:           ceil(($font-size-base * 1.25)) ; // ~18px
$font-size-small:           ceil(($font-size-base * 0.85)) ; // ~12px

$line-height-base:          1.73 ; // 20/14

$line-height-computed:      floor(($font-size-base * $line-height-base)) ; // ~20px

$text-muted:                $gray-light;
$abbr-border-color:         $gray-light;
$headings-small-color:      $gray-light;

$blockquote-small-color:    $gray-light;
$blockquote-font-size:      ($font-size-base * 1.25);
$blockquote-border-color:   $gray-lighter;
$page-header-border-color:  $gray-lighter;

// ======================== //
// Headings
// ======================== //

$headings-font-family:      inherit;
$headings-font-weight:      500;
$headings-line-height:      1.1;
$headings-color:            inherit;

$headings-small-color:      $gray-light ;

$font-size-h1:              floor(( $font-size-base * 2.2)) ; // ~36px
$font-size-h2:              floor(( $font-size-base * 1.85)) ; // ~30px
$font-size-h3:              ceil(( $font-size-base * 1.5)) ; // ~24px
$font-size-h4:              ceil(( $font-size-base * 1.25)) ; // ~18px
$font-size-h5:              $font-size-base ;
$font-size-h6:              ceil(($font-size-base * 0.85)) ; // ~12px
