<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマ設定CSS 出力テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */
global $dsblog_mod;
?>
body{
	background-color: <?php echo esc_attr( $dsblog_mod['background_color'] )?>;
	<?php if ( '' !== $dsblog_mod['background_image_upload'] ) : ?>
		background-image: url("<?php echo esc_url( $dsblog_mod['background_image_upload'] )?>");
	<?php endif; ?>
	font-size: <?php echo $dsblog_mod['font_base_size'];?>;
	line-height: <?php echo $dsblog_mod['font_line_height'];?>;
	letter-spacing: <?php echo $dsblog_mod['font_letter_space'];?>;
}
<?php if ( '' !==  get_theme_mod( 'header_background_image_upload', '' ) ) : ?>
header#masthead{
	background: url("<?php echo  get_theme_mod( 'header_background_image_upload', '' ) ;?>");
}
<?php endif ; ?>

header#masthead .logo {
	color: <?php echo get_theme_mod( 'logo_text_color', false );?>
}

//
// ナビバー
//

.navbar.navbar-relative {
	-webkit-box-shadow: inset 0px 0px 1px #fff;
	box-shadow: inset 0px 0px 1px #fff;
	padding-top: 8px;
	background-image: url(../img/navbar-bg.png) ;
	padding-bottom: 8px;
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
	.container {
		#header-navbar-collapse {
			> ul {
				border-right: 1px solid lighten(<?php echo get_theme_mod('themecolor_themecolor','#e8e8e8')?>,5);
				> li{
					&.active{
						<?php the_dsblog_color('themecolor', 'bg' , 'darken', 10 ); ?>
					}
					> a{
						<?php the_dsblog_color('themecolor', 'bg' ) ?>
						background-color: transparent !important;
						border-left: 1px solid lighten(<?php echo get_theme_mod('themecolor_themecolor','#e8e8e8')?>,5);
						&:hover{
							<?php the_dsblog_color('themecolor', 'bg' , 'lighten', 10 ); ?>
						}
					}
				}
			}
		}
		.navbar-toggle {
			border-color: #FFF;
			&:hover,
			&:focus {
				background-color: <?php the_dsblog_color('themecolor', 'bg' , 'lighten', 10 ); ?>;
			}
			.icon-bar {
				background-color: #FFF;
			}
		}
	}
}

//
// リンク
//

a{
	<?php the_dsblog_color('linkcolor', 'text' ) ?>
}
a:hover{
	<?php the_dsblog_color('linkcolor_hover', 'text' ) ?>
}

//
// カラー
//

.themecolor-bg{
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
}
.themecolor-bg a{
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
}
.themecolor-color{
	<?php the_dsblog_color('themecolor', 'text' ) ?>
}
.themecolor-border{
	<?php the_dsblog_color('themecolor', 'border' ) ?>
}
.primary-bg{
	<?php the_dsblog_color('primary', 'bg' ) ?>
}
.primary-color{
	<?php the_dsblog_color('primary', 'text' ) ?>
}
.primary-border{
	<?php the_dsblog_color('primary', 'border' ) ?>
}
.secondary-bg{
	<?php the_dsblog_color('secondary', 'bg' ) ?>
}
.pagination>li>a,
.pagination>li>span,
.pagination>li>a:hover,
.pagination>li>a:focus,
.pagination>li>span:hover,
.pagination>li>span:focus,
.secondary-color{
	<?php the_dsblog_color('secondary', 'text' ) ?>
}

.secondary-border-bottom{
	<?php the_dsblog_color('secondary', 'border-bottom', 'lighten' , '30' )  ?>
}

.secondary-border-top{
	<?php the_dsblog_color('secondary', 'border-top' )  ?>
}


//
// ウィジェット
//

.widget.widget_recent_entries  ul li a:hover,
.widget.widget_nav_menu  ul li a:hover,
.widget.widget_categories  ul li a:hover,
.widget.widget_meta  ul li a:hover,
.widget.widget_archive  ul li a:hover {
	background-color: <?php echo get_theme_mod('themecolor_secondary', '#FFF');?>;
	border-bottom: 3px solid darken( <?php echo get_theme_mod('themecolor_secondary', '#FFF');?>, 10 );
	color: #fff;
}
.widget.widget_recent_entries.widget ul li:hover a,
.widget.widget_nav_menu  ul li a:hover,
.widget.widget_categories  ul li a:hover,
.widget.widget_meta  ul li a:hover,
.widget.widget_archive  ul li a:hover{
	color: #fff;
}
.widget.widget_recent_entries.widget ul li a:active,
.widget.widget_nav_menu  ul li a:active,
.widget.widget_categories  ul li a:active,
.widget.widget_meta  ul li a:active,
.widget.widget_archive  ul li a:active{
	<?php the_dsblog_color('secondary', 'bg', 'darken', 20 )  ?>
	color: #fff;
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus {
	<?php the_dsblog_color('primary', 'bg' ) ?>
}

//
// 投稿日
//

.entry-meta .posted-on .entry-date{
	<?php the_dsblog_color('secondary', 'bg' ) ?>
	color: #FFFFFF;
}
<?php
// フッター
 if ( is_footer_widget() ) : ?>
footer#colophon{
	border-top: 3px solid transparent;
	<?php the_dsblog_color('themecolor', 'border' , 'lighten', 0 ) ?>
	background-repeat: repeat-x;
	background-position: top ;
	padding-top: 0.8em;
	padding-bottom: 1em;
}
<?php

endif;
?>

//
// 投稿フッター
//

.single{
	.site-main{
		.entry-footer{
			<?php the_dsblog_color('secondary', 'bg', 'lighten', 40 ) ?>
			<?php the_dsblog_color('secondary', 'border', 'lighten', 35 ) ?>
		}
	}
}

<?php

//
// 見出し出力
//

the_headding_css( array( '.entry_header' ) , 'header' );
the_headding_css( array( '.widget-title' ) , 'widget_title' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h1' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h2' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h3' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h4' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h5' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h6' );

//
// ボタン出力
//

echo get_button_generator( '.btn-dimensional', 'primary' , 1  );


/**
 * 見出しの設定
 * @param array $dom
 * @param string $color
 * @return echo css
 */

function the_headding_css( array $dom , $heading_tag , $color = 'secondary' ){
	global $dsblog_mod;
	if ( empty( $dom ) )  return ;
	$selecter = '';
	foreach ( $dom as $key => $value ) {
		if ( $value === end( $dom ) ) {
			$selecter .= $value ;
		} else {
			$selecter .= $value . ',';
		}
	}
	switch ( $dsblog_mod['heading_type_'. $heading_tag] ) {
		case 'type01':
echo get_dom_selecter( $dom , $heading_tag ) ;?> .entry-meta {
	margin-top: 0.7em;
}

	<?php
break;
// type02
case 'type02':
echo get_dom_selecter( $dom , $heading_tag ) ;?>{
	/*type02 */
	<?php the_dsblog_color( $color , 'border' , 'darken' , 20);  ?>
	position: relative;
	<?php the_dsblog_color( $color , 'bg' , 'lighten' , 0)  ?>
	margin: 0em -0.6em 1.5em -0.6em;
	padding: 0.3em 0.6em;
	box-shadow: 0 1px 3px #777;
	-moz-box-shadow: 0 1px 3px #777;
	-webkit-box-shadow: 0 1px 3px #777;
	-o-box-shadow: 0 1px 3px #777;
	-ms-box-shadow: 0 1px 3px #777;
	display: inline-block;
	border: none;
	width: 102.5%;
}
<?php echo get_dom_selecter( $dom , $heading_tag  , ' a') ;?>{
	color: #FFF;

}
<?php echo get_dom_selecter( $dom , $heading_tag  , ':after') ;?>{
	content: "";
	position: absolute;
	top: 100%;
	height: 0;
	width: 0;
	border: $entry-title-border-top solid transparent;
	border-top: $entry-title-border-top solid #333;

}
<?php echo get_dom_selecter( $dom , $heading_tag  , ':after') ;?>{
	left: 0;
	border-right: $entry-title-border-top solid #333;

}
<?php
break;
// type03
case 'type03': ?>
<?php echo get_dom_selecter( $dom , $heading_tag  ) ;?>{
	/*type03 */
	position:relative;
	padding-left: 0em;
	overflow: hidden;
	font-weight: bold;
	margin-top: 0px;
	line-height: 3em;
	border: none !important;
}
<?php echo get_dom_selecter( $dom , $heading_tag  , ':before') ;?>{
	content:'';
	height:3px;
	position: absolute;
	width:100px;
	left: 0px;
	bottom: 0;
	display:block;
	<?php the_dsblog_color( $color , 'bg' , 'lighten' , 0)  ?>
}
<?php echo get_dom_selecter( $dom , $heading_tag  , ':after') ;?>{
	content:'';
	height: 1px;
	position: absolute;
	width: 100%;
	left: 0px;
	bottom: 0;
	display: block;
	<?php the_dsblog_color( $color , 'bg' , 'lighten' , 0)  ?>
}
<?php
break;
// type04
case 'type04': ?>
<?php echo get_dom_selecter( $dom , $heading_tag  ) ;?>{
	/*type04 */
	position: relative;
	padding-left: 1.6em;
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAA+CAIAAAByeXZvAAAAP0lEQVRIx2N6/+YNfsTEJyiIHzExEAKDRsV/QmAI+WU0PEZDbKT45TshMIR8Sw13DJ2YG1m+HU6pkI2ZGT8CALVhqxkMFv8hAAAAAElFTkSuQmCC");
	font-weight: bold;
	border-left: 1px solid #e8e8e8;
	border-right: 1px solid #e8e8e8;
	background-size: auto 100%;
	border-top: none !important;
	margin-top: 0px;
	line-height: 3em;
}
<?php echo get_dom_selecter( $dom , $heading_tag  , ':before') ;?>{
	content: "";
	position: absolute;
	<?php the_dsblog_color( $color , 'bg' , 'lighten' , 0)  ?>
	top: 50%;
	left: 0.4em;
	margin-top :-15px;
	height: 30px;
	width: 8px;
	border-radius: 2px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
}
<?php
break;
// type 05
case 'type05': ?>

<?php echo get_dom_selecter( $dom , $heading_tag  ) ;?>{
	/*type05 */
	position: relative;
	padding-left: 1em;
	<?php the_dsblog_color( $color , 'border' , 'lighten' , 0)  ?>
	padding-top: 2px;
	border: 2px solid;
	font-weight: bold;
	line-height: 2.3em;
}

<?php echo get_dom_selecter( $dom , $heading_tag , ':before' ) ;?>{
	content: "";
	position: absolute;
	<?php the_dsblog_color( $color , 'bg' , 'lighten' , 0)  ?>
	top: 50%;
	left: 0.4em;
	margin-top :-15px;
	height: 30px;
	width: 8px;
	border-radius: 2px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
}
			<?php
break;

// type06
case 'type06': ?>
<?php echo get_dom_selecter( $dom , $heading_tag  ) ;?>{
	/*type06 */
	position: relative;
	font-weight: bold;
	margin-top: 0px;
	margin-bottom: 1.5em;
	padding: 0.5em 0.5em 0.5em 2.4em;
	border-bottom: 2px solid transparent;
	<?php the_dsblog_color( $color , 'border' , 'lighten' , 0)  ?>
	clear:both;
	overflow:hidden;
	line-height: 2.3em;
	border-top: none;
}
<?php echo get_dom_selecter( $dom , $heading_tag , ':before' ) ;?>{
	content: "□";
	font-size: 130%;
	position: absolute;
	<?php the_dsblog_color( $color , 'text' , 'darken' , 30)  ?>
	top: 0.8em;
	left: 0.3em;
	height: 12px;
	width: 12px;
}
<?php echo get_dom_selecter( $dom , $heading_tag , ':after' ) ;?>{
	content: "□";
	font-size: 130%;
	position: absolute;
	<?php the_dsblog_color( $color , 'text' , 'lighten' , 0)  ?>
	top: 0.4em;
	left: 0;
	height: 12px;
	width: 12px;
}
			<?php
break;

default:
break;
		}
}
function get_dom_selecter( $dom , $heading_tag ,  $repeat='' ){
	$selecter = '';
	foreach ( $dom as $key => $value ) {
		if ( $value === end( $dom ) ) {
			switch ( $heading_tag ) {
				case 'widget_title':
					$selecter .= $value . $repeat;
					break;
				case 'h1':
				case 'h2':
				case 'h3':
				case 'h4':
				case 'h5':
					$selecter .= $value . $heading_tag . $repeat;
					break;
				default:
					$selecter .=  $heading_tag . $value. $repeat;
					break;
			}
		} else {
			switch ( $heading_tag ) {
				case 'widget_title':
					$selecter .= $value . $repeat .',';
					break;
				case 'h1':
				case 'h2':
				case 'h3':
				case 'h4':
				case 'h5':
					$selecter .= $value . $heading_tag . $repeat .',';
					break;
				default:
					$selecter .= $heading_tag . $value . $repeat .',';
					break;
			}
		}
	}
	return $selecter;
}

/**
 * button generator
 *
 * @param $dom : html selecter
 * @param $themecolor : themecolor,primary,secondary
 * @param $effect : gradient effent
 * @return css code
 */
function get_button_generator( $dom='a.btn' , $themecolor='themecolor' , $effect ){
	global $dsblog_mod;
	$css = '';
	$background = '';
	$color = $dsblog_mod['themecolor_' . $themecolor ];
	$color_obj = new Color( $color );
	$background .= $color_obj->getCssGradientCustom( $effect , TRUE );
	$css .=
$dom . '{
	background-color: ' . $color . ';
	position: relative;
	color: ' . ( $color_obj->isDark() ? '#FFF' : '#252525' ) . ';
	text-shadow: 1px 1px 1px ' . ( $color_obj->isDark() ? 'rgba(255,255,255,0.2)' : 'rgba(0,0,0,0.2)' ) . ';
	' . $background . '
	-webkit-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', 0px 4px 0px #' . $color_obj->darken( $effect+20 ) . ';
	-moz-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', 0px 4px 0px #' . $color_obj->darken( $effect+20 ) . ';
	-o-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', 0px 4px 0px #' . $color_obj->darken( $effect+20 ) . ';
	box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', 0px 4px 0px #' . $color_obj->darken( $effect+20 ) . ';
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-o-border-radius: 4px;
	border-radius: 4px;
	border: none;
}
' . $dom . '::before {
background-color: #' . $color_obj->darken( $effect ) . ';
content:"";
display: block;
position: absolute;
width: 100%;
height: 100%;
padding-left: 2px;
padding-right: 2px;
padding-bottom: 3px;
left: -2px;
top: 5px;
z-index: -1;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
-o-border-radius: 4px;
border-radius: 4px;
-webkit-box-shadow: 0px 1px 0px #fff;
-moz-box-shadow: 0px 1px 0px #fff;
-o-box-shadow: 0px 1px 0px #fff;
box-shadow: 0px 1px 0px #fff;
}
' . $dom . ':active {
color: ' . ( $color_obj->isDark() ? '#252525' : '#FFF' ) . ' ;
text-shadow: 0px 1px 1px rgba( 255, 255, 255 , 0.3);
background: #' . $color_obj->lighten( $effect+10 ) . ';
-webkit-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', inset 0px -1px 0px #' . $color_obj->darken( $effect+20 ) . ';
-moz-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', inset 0px -1px 0px #' . $color_obj->darken( $effect+20 ) . ';
-o-box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', inset 0px -1px 0px #' . $color_obj->darken( $effect+20 ) . ';
box-shadow: inset 0px 1px 0px #' . $color_obj->darken( $effect ) . ', inset 0px -1px 0px #' . $color_obj->darken( $effect+20 ) . ';
margin-top:7px;
}
' . $dom . ':active::before {
	margin-top:-2px;
}';
		return $css;
}
