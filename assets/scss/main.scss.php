<?php
/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage SCSS 出力 ファイル
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

// require variables
require get_template_directory() . '/assets/scss/variables.scss.php'; ?>

// import theme scss
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/typo";

@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/header";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/animation";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/article";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/forms";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/headings";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/main";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/media";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/aside";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/footer";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/wordpress";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/single";
@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/comment";

.edit-link{
	border-bottom-width: 2px;
	.dashicons{
		vertical-align: top;
		font-size: 17px;
	}
}

@media  (max-width: $responsive-device-width ) {
	@import "<?php echo get_theme_mod( 'theme_name', 'themes' );?>/responsive";
}

<?php
require get_template_directory() . '/assets/scss/customizer.scss.php'; ?>
