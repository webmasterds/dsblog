<?php
/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 検索ページ
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<section id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php if ( have_posts() ) : ?>

		<header class="page-header">
			<h1 class="page-title"><?php printf( __( '%s の検索結果 : ', 'dsblog' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		</header><!-- .page-header -->

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'templates/content', 'search' ); ?>

		<?php endwhile; ?>

		<?php dsblog_paging_nav(); ?>

	<?php else : ?>

		<?php get_template_part( 'templates/content', 'none' ); ?>

	<?php endif; ?>

	</main><!-- #main -->
</section><!-- #primary -->
