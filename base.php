<?php

/**
 * =====================================================
 * ベース テンプレート
 * @package   DS BLOG THEME
 * @author    夢リタ
 * @license   http://creativecommons.org/licenses/by/2.1/jp/
 * @link      http://yumerita.jp/blog
 * @copyright 2014 夢リタ
 * =====================================================
 */

get_template_part( 'modules/head' );
get_template_part( 'modules/header' );

do_action( 'get_header' ); ?>

<!--[if lt IE 8]>
	<div class="alert alert-warning">
		<?php _e( 'あなたが使っているのは <strong>古いブラウザです。</strong><a href="http://browsehappy.com/">ブラウザをアップグレード</a> してください。 ', 'dsblog' ); ?>
	</div>
<![endif]-->

<?php
dynamic_sidebar( 'main-visual-primary' ); ?>

<div class="wrap container" role="document">
	<div class="content clearfix">
		<!-- メインカラム -->
		<main class="div main <?php echo dsblog_main_class(); ?>" role="main">
			<?php
			// コンテンツプライマリーウィジェットエリア
			dynamic_sidebar( 'content-primary' ); ?>
			<?php
			// テンプレートファイルをインクルード
			include dsblog_template_path(); ?>
			<?php
			// コンテンツセカンダリーウィジェットエリア
			dynamic_sidebar( 'content-secondary' ); ?>
		</main>
		<?php
		if ( dsblog_display_sidebar() ) : ?>
			<aside class="div sidebar <?php echo dsblog_sidebar_class(); ?>" role="complementary">
				<?php
				// sidebar.php を呼出
				include dsblog_sidebar_path(); ?>
			</aside>
		<?php
		endif; ?>
	</div>
</div>

<?php
do_action( 'get_footer', $arg = '' );

get_template_part( 'modules/footer' );
