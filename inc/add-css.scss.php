<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマ設定CSS 出力テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

// use phpColors\Color;

global $dsblog_mod;
?>
body{
	background-color: <?php echo esc_attr( $dsblog_mod['background_color'] )?>;
	<?php if ( '' !== $dsblog_mod['background_image_upload'] ) : ?>
	background-image: url("<?php echo esc_url( $dsblog_mod['background_image_upload'] )?>");
	<?php endif; ?>
	font-size: <?php echo $dsblog_mod['font_base_size'];?>;
	line-height: <?php echo $dsblog_mod['font_line_height'];?>;
	letter-spacing: <?php echo $dsblog_mod['font_letter_space'];?>;
}
<?php if ( '' !==  get_theme_mod( 'header_background_image_upload', '' ) ) : ?>
header#masthead{
	background: url("<?php echo  get_theme_mod( 'header_background_image_upload', '' ) ;?>");
}
<?php endif ; ?>

header#masthead .logo {
	color: <?php echo get_theme_mod( 'logo_text_color', false );?>
}

//
// ナビバー
//

.navbar.navbar-relative {
	-webkit-box-shadow: inset 0px 0px 1px #fff;
	box-shadow: inset 0px 0px 1px #fff;
	padding-top: 8px;
	background-image: url(../img/navbar-bg.png) ;
	padding-bottom: 8px;
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
	.container {
		#header-navbar-collapse {
			> ul {
				border-right: 1px solid lighten(<?php echo get_theme_mod('themecolor_themecolor','#e8e8e8')?>,5);
				> li{
					&.active{
						<?php the_dsblog_color('themecolor', 'bg' , 'darken', 10 ); ?>
					}
					> a{
						<?php the_dsblog_color('themecolor', 'bg' ) ?>
						background-color: transparent !important;
						border-left: 1px solid lighten(<?php echo get_theme_mod('themecolor_themecolor','#e8e8e8')?>,5);
						&:hover{
							<?php the_dsblog_color('themecolor', 'bg' , 'lighten', 10 ); ?>
						}
					}
				}
			}
		}
		.navbar-toggle {
			border-color: #FFF;
			&:hover,
			&:focus {
				background-color: <?php the_dsblog_color('themecolor', 'bg' , 'lighten', 10 ); ?>;
			}
			.icon-bar {
				background-color: #FFF;
			}
		}
	}
}

//
// リンク
//

a{
	<?php the_dsblog_color('linkcolor', 'text' ) ?>
}
a:hover{
	<?php the_dsblog_color('linkcolor_hover', 'text' ) ?>
}

//
// カラー
//

.themecolor-bg{
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
}
.themecolor-bg a{
	<?php the_dsblog_color('themecolor', 'bg' ) ?>
}
.themecolor-color{
	<?php the_dsblog_color('themecolor', 'text' ) ?>
}
.themecolor-border{
	<?php the_dsblog_color('themecolor', 'border' ) ?>
}
.primary-bg{
	<?php the_dsblog_color('primary', 'bg' ) ?>
}
.primary-color{
	<?php the_dsblog_color('primary', 'text' ) ?>
}
.primary-border{
	<?php the_dsblog_color('primary', 'border' ) ?>
}
.secondary-bg{
	<?php the_dsblog_color('secondary', 'bg' ) ?>
}
.pagination>li>a,
.pagination>li>span,
.pagination>li>a:hover,
.pagination>li>a:focus,
.pagination>li>span:hover,
.pagination>li>span:focus,
.secondary-color{
	<?php the_dsblog_color('secondary', 'text' ) ?>
}

.secondary-border-bottom{
	<?php the_dsblog_color('secondary', 'border-bottom', 'lighten' , '30' )  ?>
}

.secondary-border-top{
	<?php the_dsblog_color('secondary', 'border-top' )  ?>
}


//
// ウィジェット
//

.widget.widget_recent_entries  ul li a:hover,
.widget.widget_nav_menu  ul li a:hover,
.widget.widget_categories  ul li a:hover,
.widget.widget_meta  ul li a:hover,
.widget.widget_archive  ul li a:hover {
	background-color: <?php echo get_theme_mod('themecolor_secondary', '#FFF');?>;
	border-bottom: 3px solid darken( <?php echo get_theme_mod('themecolor_secondary', '#FFF');?>, 10 );
	color: #fff;
}
.widget.widget_recent_entries.widget ul li:hover a,
.widget.widget_nav_menu  ul li a:hover,
.widget.widget_categories  ul li a:hover,
.widget.widget_meta  ul li a:hover,
.widget.widget_archive  ul li a:hover{
	color: #fff;
}
.widget.widget_recent_entries.widget ul li a:active,
.widget.widget_nav_menu  ul li a:active,
.widget.widget_categories  ul li a:active,
.widget.widget_meta  ul li a:active,
.widget.widget_archive  ul li a:active{
	<?php the_dsblog_color('secondary', 'bg', 'darken', 20 )  ?>
	color: #fff;
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus {
	<?php the_dsblog_color('primary', 'bg' ) ?>
}

//
// 投稿日
//

.entry-meta .posted-on .entry-date{
	<?php the_dsblog_color('secondary', 'bg' ) ?>
	color: #FFFFFF;
}
<?php
// フッター
 if ( is_footer_widget() ) : ?>
footer#colophon{
	border-top: 3px solid transparent;
	<?php the_dsblog_color('themecolor', 'border' , 'lighten', 0 ) ?>
	background-repeat: repeat-x;
	background-position: top ;
	padding-top: 0.8em;
	padding-bottom: 1em;
}
<?php

endif;
?>

//
// 投稿フッター
//

.single{
	.site-main{
		.entry-footer{
			<?php the_dsblog_color('secondary', 'bg', 'lighten', 40 ) ?>
			<?php the_dsblog_color('secondary', 'border', 'lighten', 35 ) ?>
		}
	}
}

<?php

//
// 見出し出力
//

the_headding_css( array( '.entry_header' ) , 'header' );
the_headding_css( array( '.widget-title' ) , 'widget_title' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h1' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h2' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h3' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h4' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h5' );
the_headding_css( array( '.entry-content ' , '.post > ' ) , 'h6' );

//
// ボタン出力
//

echo get_button_generator( '.btn-dimensional', 'primary' , 1  );
