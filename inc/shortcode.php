<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ショートコード設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

$ds_prefix = 'ds_';

/**
 * リストショートコード : ds_list
 *
 * 例 : [ds_list type="circle" color="black" list="list,list,list"]
 *
 * @param $atts['type']  : circle , dotted ,checked ,box_checked ,arrow, box_arrow , circle_arrow, circle
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['list'] : string
 */

add_shortcode( 'ds_list', 'ds_blog_shortcode_list' );

function ds_blog_shortcode_list( $atts, $content = null ) {

	global $ds_prefix;
	$html    = '';
	extract( shortcode_atts( array(
		'type'      => 'circle',
		'color'      => 'black',
		'list'      => 'ここにリストを,区切りで記入してください。',
	) , $atts ) );
	$list = explode( ',', $atts['list'] );
	foreach ( $list as $key => $value ) {
		$html.= '<li>' . $value . '</li>';
	}

	return '<ul class="ds_list ' . $atts['type'] . ' ' . esc_attr( $ds_prefix . $color ) . '">' . $html . '</ul>';
}

/**
 * ノーマルボックスショートコード : ds_normal_box
 *
 * 例 : [ds_normal_box border="1" color="black"] aaaaaa [/ds_normal_box]
 *
 * @param $atts['border']  : border width (px)
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['radius'] : radius width (px)
 * @param $content : string
 */

add_shortcode( 'ds_normal_box', 'ds_blog_shortcode_normal_box' );

function ds_blog_shortcode_normal_box( $atts, $content = null ) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'border' => '1',
		'color' => 'black',
		'radius' => '0',
	) , $atts));
	$style.= 'border-width: ' . $border . 'px !important;';
	if ($radius) {
		$style.= 'border-radius: ' . $radius . 'px;';
	}
	return '<div class="ds_normal_box ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
}

/**
 * Q & A ボックス : qanda_box
 *
 * 例 : [ds_qa_box color="black" q="hogehoge" a="hogehoge"]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['q'] : string
 * @param $atts['a'] : string
 * @param $content : string
 */

add_shortcode('ds_qa_box', 'ds_blog_shortcode_qa_box');

function ds_blog_shortcode_qa_box($atts, $content = null) {
	global $ds_prefix;

	$html    = '';
	extract(shortcode_atts(array(
		'color' => 'black',
		'q' => 'Question',
		'a' => 'Answer',
	) , $atts));

	$html.= '
<dl class="ds_qa_box ' . esc_attr($ds_prefix . $color) . '">
<dt class="question">
' . $atts['q'] . '
</dt>
<dd class="answer">
' . $atts['a'] . '
</dd>
</dl>';
	return $html;
}

/**
 * チェックボックス : check_box
 *
 * 例 : [ds_check_box color="black" radius="3"]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['a'] : string
 * @param $content : string
 */

add_shortcode('ds_check_box', 'ds_blog_shortcode_check_box');

function ds_blog_shortcode_check_box($atts, $content = null) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'color' => 'black',
		'radius' => '0',
		'border' => '1',
	) , $atts));
	if ($radius || $border) {
		$style.= 'border-radius: ' . $radius . 'px;';
		$style.= 'border-width: ' . $border . 'px !important';
	}
	$html.= '<div class="ds_check_box ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * ビックリマーク ボックス : exclamation_box
 *
 * 例 : [ds_exclamation_box type="1" color="black" radius="3"]hogehoge[/ds_exclamation_box]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['type'] : 1 ,2
 * @param $atts['radius'] : int (px)
 * @param $content : string
 */

add_shortcode('ds_exclamation_box', 'ds_blog_shortcode_exclamation_box');

function ds_blog_shortcode_exclamation_box($atts, $content = '') {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	$type    = '';
	extract(shortcode_atts(array(
		'type' => '1',
		'color' => 'black',
		'radius' => '0',
		'border' => '0',
	) , $atts));
	if ($radius || $border) {
		$style.= 'border-radius: ' . $radius . 'px;';
		$style.= 'border-width: ' . $border . 'px !important';
	}
	if ($type == '1') {
		$type = 'square';
	} else {
		$type = 'triangle';
	}
	$html.= '<div class="ds_exclamation_box ' . esc_attr($type) . ' ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * メモ ボックス : memo_box
 *
 * 例 : [ds_memo_box color="black" radius="3"]hogehoge[/ds_memo_box]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['radius'] : int (px)
 * @param $content : string
 */

add_shortcode('ds_memo_box', 'ds_blog_shortcode_memo_box');

function ds_blog_shortcode_memo_box($atts, $content = null) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'color'       => 'black',
		'radius'       => '0',
	) , $atts));
	$style = ($radius ? 'border-radius: ' . $radius . 'px' : '');
	$html.= '<div class="ds_memo_box ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * 指 ボックス : pencil_box
 *
 * 例 : [ds_pencil_box color="black" radius="3"]hogehoge[/ds_pencil_box]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['radius'] : int (px)
 * @param $content : string
 */

add_shortcode('ds_pencil_box', 'ds_blog_shortcode_pencil_box');

function ds_blog_shortcode_pencil_box($atts, $content = null) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'color'       => 'black',
		'radius'       => '0',
	) , $atts));
	$style = ($radius ? 'border-radius: ' . $radius . 'px' : '');
	$html.= '<div class="ds_pencil_box ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * 星 ボックス : star_box
 *
 * 例 : [ds_star_box color="black" radius="3"]hogehoge[/ds_star_box]
 *
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['radius'] : int (px)
 * @param $content : string
 */

add_shortcode('ds_star_box', 'ds_blog_shortcode_star_box');

function ds_blog_shortcode_star_box($atts, $content = null) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'color'       => $prefix . 'black',
		'radius'       => '0',
	) , $atts));
	$style = ($radius ? 'border-radius: ' . $radius . 'px' : '');
	$html.= '<div class="ds_star_box ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * ランキングボックス : ds_ranking_box
 *
 * 例 : [ds_ranking_box rank="1" radius="3"]テストテスト[/ds_ranking_box]
 *
 * @param $atts['rank] (int) 1 ~ 10
 * @param $atts['radius'] (int) px
 * @param $content : string
 */

add_shortcode('ds_ranking_box', 'ds_blog_shortcode_ranking_box');

function ds_blog_shortcode_ranking_box( $atts, $content = null ) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'rank'       => '1',
		'radius'     => '0',
	) , $atts));
	$style = ($radius ? 'border-radius: ' . $radius . 'px' : '');
	$html.= '<div class="ds_ranking_box ' . esc_attr( 'ranking_' . $rank) . '" style="' . $style . '">' . $content . '</div>';
	return $html;
}

/**
 * 装飾見出し : ds_heading
 *
 * 例 : [ds_heading type="crown" color="black" radius="3"]hogehoge[/ds_heading]
 *
 * @param $atts['type'] : crown2, memo2, beginner2, present2, start2,crown2, memo2, beginner2, present2, start2
 * @param $atts['color'] : black, red, blue, pink, green, orange
 * @param $atts['radius'] : int (px)
 * @param $content : string
 */

add_shortcode('ds_heading', 'ds_blog_shortcode_heading');

function ds_blog_shortcode_heading($atts, $content = null) {
	global $ds_prefix;
	$html    = '';
	$style   = '';
	extract(shortcode_atts(array(
		'color'       => 'black',
		'radius'       => '0',
		'type'       => 'crown',
	) , $atts));
	$style = ($radius ? 'border-radius: ' . $radius . 'px' : '');
	$html.= '<h3 class="ds_heading ' . esc_attr($type) . ' ' . esc_attr($type) . ' ' . esc_attr($ds_prefix . $color) . '" style="' . $style . '">' . $content . '</h3>';
	return $html;
}

/**
 *
 */
$dsblog_shortcodes = array(

	// リスト
	'ds_list' => array(
		'title' => 'リスト',
		'desc' => '様々な形式のリストを表示できます。',
		'example' => '[ds_list type="circle" color="red" list="サンプル1,サンプル2,サンプル3"]',
		'param' => array(
			'type' => array(
				'desc' => '使用可能なリストのパラメーター',
				'param' => array(
					'circle',
					'dotted',
					'checked',
					'box_checked',
					'arrow',
					'box_arrow',
					'circle_arrow',
					'circle',
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'list' => array(
				'desc' => 'テキストをカンマ区切りで入力してください。',
				'param' => array(
					'テキスト,テキスト'
				) ,
			)
		)
	) ,

	// ノーマルボックスショートコード
	'ds_normal_box' => array(
		'title' => 'ノーマルボックス',
		'desc' => '枠線に囲われたボックスを表示します。',
		'example' => '[ds_normal_box border="1" color="red"] サンプルサンプルサンプルサンプル [/ds_normal_box]',
		'param' => array(
			'border' => array(
				'desc' => '枠線の太さ',
				'param' => array(
					'5',
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// Q & A ボックス
	'ds_qa_box' => array(
		'title' => 'Q&Aボックス',
		'desc' => 'Q&Aボックスを表示します。',
		'example' => '[ds_qa_box color="red" q="サンプル" a="サンプル"]',
		'param' => array(
			'type' => array(
				'desc' => '使用可能なタイプ',
				'param' => array(
					'1',
					'2',
				)
			) ,
			'q' => array(
				'desc' => '質問内容',
				'param' => array(
					'テキスト'
				) ,
			) ,
			'a' => array(
				'desc' => '解答',
				'param' => array(
					'テキスト'
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			) ,
		)
	) ,

	// チェックボックス
	'ds_check_box' => array(
		'title' => 'チェックボックス',
		'desc' => 'チェックボックスアイコンを左に表示します。',
		'example' => '[ds_check_box color="orange" border="5" radius="3"]サンプルサンプルサンプル[/ds_check_box]',
		'param' => array(
			'type' => array(
				'desc' => 'アイコンのタイプ',
				'param' => array(
					'1',
					'2',
				) ,
			) ,
			'border' => array(
				'desc' => '枠線の太さ',
				'param' => array(
					'5',
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// エクストラメーション
	'ds_exclamation_box' => array(
		'title' => 'エクストラメーション',
		'desc' => 'エクストラメーションアイコンを左に表示するボックスです。',
		'example' => '[ds_exclamation_box type="1" color="orange" border="3" radius="3"]example[/ds_exclamation_box]',
		'param' => array(
			'type' => array(
				'desc' => 'アイコンのタイプ',
				'param' => array(
					'1',
					'2',
				) ,
			) ,
			'border' => array(
				'desc' => '枠線の太さ',
				'param' => array(
					'5',
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// メモボックス
	'ds_memo_box' => array(
		'title' => 'メモボックス',
		'desc' => 'メモアイコンを左に表示するボックスです。',
		'example' => '[ds_memo_box color="pink" radius="3"]exampleexample[/ds_memo_box]',
		'param' => array(
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// ペンシルボックス
	'ds_pencil_box' => array(
		'title' => 'ペンシルボックス',
		'desc' => 'ペンシルアイコンを左に表示するボックスです。',
		'example' => '[ds_pencil_box color="green" type="2" radius="3"]サンプルサンプルサンプルサンプル[/ds_pencil_box]',
		'param' => array(
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// スターボックス
	'ds_star_box' => array(
		'title' => 'スターボックス',
		'desc' => 'スターアイコンを左に表示するボックスです。',
		'example' => '[ds_star_box color="blue" border="3" radius="3"]サンプルサンプルサンプル[/ds_star_box]',
		'param' => array(
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,
	 // ランキングボックス
	'ds_ranking_box' => array(
		'title' => 'ランキングボックス',
		'desc' => 'ランキングアイコンを左に表示するボックスです。',
		'example' => '[ds_ranking_box rank="1" radius="3"]サンプルサンプルサンプル[/ds_ranking_box]<br />[ds_ranking_box rank="2" radius="3"]サンプルサンプルサンプル[/ds_ranking_box]<br />[ds_ranking_box rank="3" radius="3"]サンプルサンプルサンプル[/ds_ranking_box]',
		'param' => array(
			'rank' => array(
				'desc' => 'ランキングの順位のアイコンを設定',
				'param' => array(
					'1',
					'2',
					'3',
					'4',
					'5',
					'6',
					'7',
					'8',
					'9',
					'10',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,

	// 装飾見出し
	'ds_heading' => array(
		'title' => '装飾見出し',
		'desc' => 'アイコンを備えた見出しを表示します。',
		'example' => '[ds_heading type="crown" color="red" radius="3"]サンプルサンプルサンプル[/ds_heading]<br>[ds_heading type="crown2" color="blue" radius="3"]サンプルサンプルサンプル[/ds_heading]',
		'param' => array(
			'type' => array(
				'desc' => '使用可能なアイコン',
				'param' => array(
					'crown',
					'memo',
					'beginner',
					'star2',
					'crown2',
					'memo2',
					'beginner2',
					'star2',
				) ,
			) ,
			'color' => array(
				'desc' => '使用可能なカラー',
				'param' => array(
					'black',
					'red',
					'blue',
					'pink',
					'green',
					'orange',
				) ,
			) ,
			'radius' => array(
				'desc' => '枠線の丸み',
				'param' => array(
					'5',
				) ,
			)
		)
	) ,
);
