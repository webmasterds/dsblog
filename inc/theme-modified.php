<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマ設定出力用関数群
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

use phpColors\Color;

$dsblog_mod = array(

	// 基本的な設定
	'general_keywords' => get_theme_mod( 'general_keywords'        , ''     ),
	'google_analytics_code' => get_theme_mod( 'google_analytics_code'   , ''     ),

	// スライダーの設定
	'slider_disp'                => get_theme_mod( 'slider_disp'               , 'true'  ),
	'slider_image_uoload_one'    => get_theme_mod( 'slider_image_uoload_one'   , ''),
	'slider_image_uoload_two'    => get_theme_mod( 'slider_image_uoload_two'   , ''),
	'slider_image_uoload_three'  => get_theme_mod( 'slider_image_uoload_three' , 'left'  ),

	// ロゴの設定
	'logo_choice'                => get_theme_mod( 'logo_choice'            , 'logo'  ),
	'logo_image_uoload'          => get_theme_mod( 'logo_image_uoload'      , get_template_directory_uri() . '/assets/img/logo-default.png'      ),
	'logo_size'                  => get_theme_mod( 'logo_size'              , 'normal'),
	'logo_align'                 => get_theme_mod( 'logo_align'             , 'left'  ),
	'logo_text_color'            => get_theme_mod( 'logo_text_color'        , '#252525'  ),

	// テーマカラーの設定
	'themecolor_themecolor'         => get_theme_mod( 'themecolor_themecolor'  , '#081C61'),
	'themecolor_primary'            => get_theme_mod( 'themecolor_primary'     , '#F27432'),
	'themecolor_secondary'          => get_theme_mod( 'themecolor_secondary'   , '#84909E'),
	'themecolor_linkcolor'          => get_theme_mod( 'themecolor_linkcolor'   , '#84909E'),
	'themecolor_linkcolor_hover'    => get_theme_mod( 'themecolor_linkcolor_hover'   , '#84909E'),

	// 背景の設定
	'background_color'               => get_theme_mod( 'background_color'       , '#FFFFFF'),
	'background_image_upload'        => get_theme_mod( 'background_image_upload', ''       ),
	'header_background_image_upload' => get_theme_mod( 'header_background_image_upload', ''       ),

	// 見出しの設定
	'heading_type_widget_title'  => get_theme_mod( 'heading_type_widget_title'  , 'type01'),
	'heading_type_header'        => get_theme_mod( 'heading_type_entry_header'  , 'type01'),
	'heading_type_h1'            => get_theme_mod( 'heading_type_h1'            , 'type01'),
	'heading_type_h2'            => get_theme_mod( 'heading_type_h2'            , 'type01'),
	'heading_type_h3'            => get_theme_mod( 'heading_type_h3'            , 'type01'),
	'heading_type_h4'            => get_theme_mod( 'heading_type_h4'            , 'type01'),
	'heading_type_h5'            => get_theme_mod( 'heading_type_h5'            , 'type01'),
	'heading_type_h6'            => get_theme_mod( 'heading_type_h6'            , 'type01'),

	// レイアウト設定
	'layout_top'              => get_theme_mod( 'layout_top'             , 'right'  ),
	'layout_layer'            => get_theme_mod( 'layout_layer'           , 'right'  ),
	'layout_navigation'       => get_theme_mod( 'layout_navigation'      , 'right'  ),

	// フォントの設定
	'font_base_size'          => get_theme_mod( 'font_base_size'         , '14px'   ),
	'font_line_height'        => get_theme_mod( 'font_line_height'       , '1.4em'  ),
	'font_letter_space'       => get_theme_mod( 'font_letter_space'      , '0px'    ),
	'font_text_color'         => get_theme_mod( 'font_text_color'      , '#252525'  ),

	// コメントの設定
	'comment_disp'            => get_theme_mod( 'comment_disp'           , 'true'   ),
	'comment_facebook'        => get_theme_mod( 'comment_facebook'       , 'true'   ),

	// ナビゲーション
	'navigation_color'        => get_theme_mod( 'navigation_color'       , 'true'   ),

	// デザイン詳細
	'design_radius'           => get_theme_mod( 'design_radius'       , '3px' ),
	'design_shadow'           => get_theme_mod( 'design_shadow'       , '3px' ),
	'design_width'            => get_theme_mod( 'design_width'        , '1000px' ),

	// フッターの設定
	'footer_copyright'        => get_theme_mod( 'footer_copyright'       , '© Copyright' . get_the_date( 'Y' ) . 'All Rights Reserved.'       ),
	'footer_pagetop_disp'     => get_theme_mod( 'footer_pagetop_disp'    , ''       ),
);

/**
 * dsblog_logo
 * @param $dsblog_mod : テーマ設定のグローバル変数
 * @return HTML
 */
function the_dsblog_logo(){
	global $dsblog_mod ;
	$html = '';
	$class = '';
	$class .= ' ' . $dsblog_mod['logo_size'] . ' ';
	$class .= ' text-' . $dsblog_mod['logo_align'];
	$color = $dsblog_mod['logo_text_color'];

	if ( 'image' == $dsblog_mod['logo_choice'] ) {
		$html .= '<div class="logo ' . esc_attr( $class ). '">';
		$html .= '<img src="' . esc_url( $dsblog_mod['logo_image_uoload'] ) . '" alt="' . esc_html( get_bloginfo( 'title' ) ) . '">';
		$html .= '</div>';
	} else {
		$html .= '<div class="logo ' . esc_attr( $class ) . '" style="' . $color . '">';
		$html .= esc_html( get_bloginfo( 'title' ) ) ;
		$html .= '</div>';
	}
	echo $html;
}

/**
 * ヘッダーカラムのクラス
 * @return string
 */
function the_dsblog_header_column( $name = 'logo', $echo=true ){

	global $dsblog_mod;

	$html  = '';
	$class = '';

	$class = $dsblog_mod['logo_size'] ;
	switch ( $class ) {
		case 'small':
			$class = array(
								 'logo' => 'col-lg-8' ,
								 'widget' => 'col-lg-16',
							 );
			break;

		case 'normal':
			$class = array(
								 'logo' => 'col-lg-12' ,
								 'widget' => 'col-lg-12',
							 );
			break;
		case 'full':
			$class = array(
								 'logo' => 'col-lg-24' ,
								 'widget' => 'col-lg-24',
							 );
			break;
		default:
			$class = array(
								 'logo' => 'col-lg-12' ,
								 'widget' => 'col-lg-12',
							 );
			break;
	}
	if ( $echo === true ) {
		echo  $class[ $name ];
	} else{
		return $class[ $name ];
	}
}

/**
 * meta keywords
 *
 */

add_action( 'wp_head', 'dsblog_meta_keywords');

function dsblog_meta_keywords( $src, $handle = null ){
	global $dsblog_mod;
	$html  = '';
	$meta_keywords = $dsblog_mod['general_keywords'];
	if ( '' !== $meta_keywords ) {
		$html .= '<meta name="keywords" content="' . $meta_keywords . '" >
';
	}
	echo $html;
}

/**
 * add google analytics
 * フッターにGoogle Analytics 追加
 * @return $html : google analytics script
 */

add_action('wp_footer', 'dsblog_google_analytics');

function dsblog_google_analytics() {
	global $dsblog_mod;
	$google_code = isset( $dsblog_mod['general_analytics_code'] ) ? $dsblog_mod['general_analytics_code'] : '';
	$html = '';
	if ( '' !== $google_code ) {
$html .= "
<script>
(function(b,o,i,l,e,r){b.GotogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='//www.google-analytics.com/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','" .  esc_html( $google_code ) . "');ga('send','pageview');
</script>
";
	}
	echo $html;
}

/**
 * themecolor
 *
 * @param $color  : themecolor , primary , secondary
 * @param $place  : bg , text , border
 * @param $action : 'darken' , 'lighten'
 * @param $value  : parsentage
 *
 */
function the_dsblog_color( $color='themecolor', $place='bg' , $action='' , $value='' ){
	// theme modifield global
	global $dsblog_mod;

	$theme_color = $dsblog_mod['themecolor_' . $color];
	$theme_color_obj = new Color( $dsblog_mod['themecolor_' . $color] );
	$html = '';
	switch ( $place ) {
		case 'bg':
			$html = 'background: ' . get_theme_color_by_action( $theme_color , $action , $value ) . ';';
			$html .= 'color: ' . ( $theme_color_obj->isDark() ? "#FFF" : "#252525") . ' ;';
			break;
		case 'text':
			$html = 'color: ' . get_theme_color_by_action( $theme_color , $action , $value )  . ';';
			break;
		case 'border':
			$html = 'border-color: ' . get_theme_color_by_action( $theme_color , $action , $value )  . ' !important;
			';
			break;
		case 'border-top':
			$html = 'border-top-color: ' . get_theme_color_by_action( $theme_color , $action , $value )  . ' !important;
			';
			break;
		case 'border-bottom':
			$html = 'border-bottom-color: ' . get_theme_color_by_action( $theme_color , $action , $value )  . ' !important;
			';
			break;
		default:
			$html = 'background: ' . $theme_color  . ' !important;';
			break;
	}
	echo $html;
}


/**
 * color uttility
 *
 * @param $theme_color
 * @param $action
 * @param $value
 * @return color code
 */

function get_theme_color_by_action( $theme_color, $action = '', $value = '' ){

	if ( empty( $action ) ){
		return $theme_color;
	}
	if ( $value == '' || $value == '') return $theme_color;

	$attr = '';
	$color_obj = new Color( $theme_color );

	switch ( $action ) {
		case 'darken':
			$attr = '#' . $color_obj->darken( $value );
			break;

		case 'lighten':
			$attr = '#' . $color_obj->lighten( $value );
			break;

		default:
			$attr = '#' . $theme_color;
			break;
	}
	return $attr;
}

/**
 * layout setting
 *
 */
add_filter( 'dsblog_display_sidebar', 'get_dsblog_layout' , 10 , 1 );

function get_dsblog_layout(){
	global $post,$dsblog_mod,$pagenow;
	$class = '';
	if ( is_front_page() || is_home() ) {
			$layout_top = $dsblog_mod['layout_top'];
			$class = get_layout_class( $layout_top );
	} else {
			$layout_layer = $dsblog_mod['layout_layer'];
			$class = get_layout_class( $layout_layer );

	}
	return $class;

}

/**
 * layout class helper
 * @param $layout
 * @return pull-left or pull-right or false
 */

function get_layout_class( $layout ){
	$class = '';
	switch ( $layout ) {
		case 'left':
			$class['main'] = 'pull-right';
			$class['side'] = 'pull-left';
			break;

		case 'right':
			$class['main'] = 'pull-left';
			$class['side'] = 'pull-right';
			break;
		case 'one_column':
			$class = false;
			break;
		default:
			break;
	}
	return $class;
}



/**
 * facebook js sdk をwp_footerへ埋め込み
 *
 * @package dsblog
 * @return javascript
 */
// if ( function_exists('affiliate_tools_get_option') ) {


add_filter( 'wp_footer', 'facebook_jssdk_init' ,10, 1 );
function facebook_jssdk_init(){
	global $dsblog_mod;
	$dsblog = TitanFramework::getInstance( 'dsblog' );
	$api_key = $dsblog->getOption( 'facebook_api_key' );
	if ( $dsblog_mod['comment_facebook'] && $api_key ) :
	?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1&appId=<?php echo esc_attr( $api_key ) ;?>";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
		<?php
	endif;
}


/**
 * facebook タグをコメントへ埋め込み
 *
 * @package dsblog
 * @return javascript
 */

add_filter( 'get_comments_template', 'facebook_comments_init', 10, 1 );

function facebook_comments_init(){
	global $dsblog_mod;
	$dsblog = TitanFramework::getInstance( 'dsblog' );
	$api_key = $dsblog->getOption( 'facebook_api_key' );
	$num = $dsblog->getOption( 'facebook_comment_num' );
	$width = $dsblog->getOption( 'facebook_comment_width' );
	$color = $dsblog->getOption( 'facebook_comment_color' );
	if ( $dsblog_mod['comment_facebook'] ) :  ?>
		<div class="facebook-comments-block">
			<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="<?php echo esc_attr( $width ) ;?>" data-numposts="<?php echo esc_attr( $num ) ;?>" data-colorscheme="<?php echo esc_attr( $color ); ?>"></div>
		</div>
	<?php
	endif;
}


/**
 * scroll top
 */
add_action( 'get_footer', 'add_footer_scrolltop_init', 10 ,1 );
function add_footer_scrolltop_init(){
	global $dsblog_mod;
	if ( 'true' == $dsblog_mod['footer_pagetop_disp'] ) {
		echo '<a href="#body" class="scroll-to-top primary-bg"><i class="dashicons dashicons-arrow-up-alt2"></i> ページトップへ</a>';
	}
}


/**
 * シェアボタンの表示
 *
 * @return string shortcode
 */

function the_sharebtn_disp(){
	$dsblog = TitanFramework::getInstance( 'dsblog' );
	$disp = $dsblog->getOption( 'post_group_sherebtn_button_disp' );
	$service = $dsblog->getOption( 'sherebtn_disp' );
	if ( ! $service ) return ;
	$service = implode(',',$service );
	if ( 'true' ==  $disp ) {
		echo do_shortcode('[sharebtn service=' . $service . ']');
	}
}

/**
 * SEOメタタグ設定
 *
 * @return string shortcode
 */

add_filter( 'wp_head', 'ds_override_seo_meta', 0,  1 );

function ds_override_seo_meta(){
	global $dsblog_mod;
	global $post;
	$dsblog       = TitanFramework::getInstance( 'dsblog' );

	if ( $dsblog->getOption( 'ogp_post_meta_disp' ) === false) {
		return;
	}
	$post_title   = $dsblog->getOption( 'ds_blog_post_title' )       ? $dsblog->getOption( 'ds_blog_post_title' ) : ds_seo_title( '|', false, 'right') ;
	$url          = get_permalink( $post->ID );
	$meta_desc    = $dsblog->getOption( 'ds_blog_post_description' ) ? $dsblog->getOption( 'ds_blog_post_description' ) : get_bloginfo( 'description' ) ;
	$meta_keyword = $dsblog->getOption( 'ds_blog_post_title' ) ? $dsblog->getOption( 'ds_blog_post_keyword' ) : $dsblog_mod['general_keywords'];
	$ogp_disp     = $dsblog->getOption( 'ds_blog_post_ogp_disp' );

	if ( is_single() || is_page() ) {

		$ogp_title    = $dsblog->getOption( 'ds_blog_post_ogp_title' ) ? $dsblog->getOption( 'ds_blog_post_ogp_title' ) : ds_seo_title( '|', false, 'right') ;
		$ogp_desc     = $dsblog->getOption( 'ds_blog_post_ogp_description' ) ? $dsblog->getOption( 'ds_blog_post_ogp_description' ) : get_bloginfo( 'description' );
		$ogp_image    = $dsblog->getOption( 'ds_blog_post_ogp_image' ) ? wp_get_attachment_url( $dsblog->getOption( 'ds_blog_post_ogp_image' ) ) : wp_get_attachment_url( $dsblog->getOption( 'ds_blog_default_ogp_image' ) ) ;

	} else if ( is_front_page() || is_home() ) {

		$ogp_title    = $dsblog->getOption( 'ds_blog_default_title' ) ? $dsblog->getOption( 'ds_blog_default_title' ) : ds_seo_title( '|', false, 'right') ;
		$ogp_desc     = $dsblog->getOption( 'ds_blog_default_desctiption' ) ? $dsblog->getOption( 'ds_blog_default_desctiption' ) : get_bloginfo( 'description' ) ;
		$ogp_image    = $dsblog->getOption( 'ds_blog_default_ogp_image' ) ? wp_get_attachment_url( $dsblog->getOption( 'ds_blog_default_ogp_image' ) ) : '';

	} else {

		$ogp_title    = $dsblog->getOption( 'ds_blog_default_title' )       ? $dsblog->getOption( 'ds_blog_default_title' )       : ds_seo_title( '|', false, 'right') ;
		$ogp_desc     = $dsblog->getOption( 'ds_blog_default_desctiption' ) ? $dsblog->getOption( 'ds_blog_default_desctiption' ) : get_bloginfo( 'description' ) ;
		$ogp_image    = $dsblog->getOption( 'ds_blog_default_ogp_image' )   ? wp_get_attachment_url( $dsblog->getOption( 'ds_blog_default_ogp_image' ) ) : '';

	}
	$blog_name    = get_bloginfo( 'name' );
	$meta_tag     = '';
	// if ( is_front_page() || is_archive() || is_home() ) rseturn ;
$meta_tag .= '
<title>' . ds_seo_title('|',false,'right' ) . '</title>
';
	// description
$meta_tag .= '
<meta name="description" content="' . $meta_desc . '">
';
	// keyword
$meta_tag .= '
<meta name="keywords" content="' . $meta_keyword . '">
';
	if ( 'true' == $ogp_disp  || ( !is_single() && !is_page() ) )  {
$meta_tag .= '
<meta property="og:locale" content="ja_JP">
<meta property="og:type" content="blog">
<meta property="og:description" content="'. $ogp_desc . '">
<meta property="og:title" content="' . $ogp_title . '">
<meta property="og:url" content="' . $url . '">
<meta property="og:site_name" content="' . $blog_name . '">
<meta property="og:image" content="' . $ogp_image . '">
';
	}
	echo $meta_tag;
}

/**
 * タイトルを置換
 *
 */

function ds_seo_title($sep = '&raquo;', $display = true, $seplocation = '') {
	global $wpdb, $wp_locale;
	$dsblog = TitanFramework::getInstance( 'dsblog' );
	$post_title = $dsblog->getOption( 'ds_blog_post_title' );
	$m = get_query_var('m');
	$year = get_query_var('year');
	$monthnum = get_query_var('monthnum');
	$day = get_query_var('day');
	$search = get_query_var('s');
	$title = '';

	$t_sep = '%WP_TITILE_SEP%';

	if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
		if ( $post_title ) {
			$title = $post_title;
		} else {
			$title = single_post_title( '', false );
		}
	}

	// If there's a post type archive
	if ( is_post_type_archive() ) {
		$post_type = get_query_var( 'post_type' );
		if ( is_array( $post_type ) )
			$post_type = reset( $post_type );
		$post_type_object = get_post_type_object( $post_type );
		if ( ! $post_type_object->has_archive )
			$title = post_type_archive_title( '', false );
	}

	// If there's a category or tag
	if ( is_category() || is_tag() ) {
		$title = single_term_title( '', false );
	}

	// If there's a taxonomy
	if ( is_tax() ) {
		$term = get_queried_object();
		if ( $term ) {
			$tax = get_taxonomy( $term->taxonomy );
			$title = single_term_title( $tax->labels->name . $t_sep, false );
		}
	}

	// If there's an author
	if ( is_author() ) {
		$author = get_queried_object();
		if ( $author )
			$title = $author->display_name;
	}

	// Post type archives with has_archive should override terms.
	if ( is_post_type_archive() && $post_type_object->has_archive )
		$title = post_type_archive_title( '', false );

	// If there's a month
	if ( is_archive() && !empty($m) ) {
		$my_year = substr($m, 0, 4);
		$my_month = $wp_locale->get_month(substr($m, 4, 2));
		$my_day = intval(substr($m, 6, 2));
		$title = $my_year . ( $my_month ? $t_sep . $my_month : '' ) . ( $my_day ? $t_sep . $my_day : '' );
	}

	// If there's a year
	if ( is_archive() && !empty($year) ) {
		$title = $year;
		if ( !empty($monthnum) )
			$title .= $t_sep . $wp_locale->get_month($monthnum);
		if ( !empty($day) )
			$title .= $t_sep . zeroise($day, 2);
	}

	// If it's a search
	if ( is_search() ) {
		/* translators: 1: separator, 2: search phrase */
		$title = sprintf(__('Search Results %1$s %2$s'), $t_sep, strip_tags($search));
	}

	// If it's a 404 page
	if ( is_404() ) {
		$title = __('Page not found');
	}

	$prefix = '';
	if ( !empty($title) )
		$prefix = " $sep ";

	// Determines position of the separator and direction of the breadcrumb
	if ( 'right' == $seplocation ) { // sep on right, so reverse the order
		$title_array = explode( $t_sep, $title );
		$title_array = array_reverse( $title_array );
		$title = implode( " $sep ", $title_array ) . $prefix;
	} else {
		$title_array = explode( $t_sep, $title );
		$title = $prefix . implode( " $sep ", $title_array );
	}

	/**
	 * Filter the text of the page title.
	 *
	 * @since 2.0.0
	 *
	 * @param string $title       Page title.
	 * @param string $sep         Title separator.
	 * @param string $seplocation Location of the separator (left or right).
	 */
	$title = apply_filters( 'wp_title', $title, $sep, $seplocation );
	// Send it out
	if ( $display )
		echo $title;
	else
		return $title;

}
