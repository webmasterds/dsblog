<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ツアー
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

/**
 * Theme Activation Tour
 *
 * This class handles the pointers used in the introduction tour.
 * @package Popup Demo
 *
 */
class WordImpress_Theme_Tour {

	private $pointer_close_id = 'wordimpress_tour'; //value can be cleared to retake tour

	/**
	 * Class constructor.
	 *
	 * If user is on a pre pointer version bounce out.
	 */
	public function __construct()
	{
		global $wp_version;

				//pre 3.3 has no pointers
		if ( version_compare( $wp_version, '3.4', '<' ) ){
			return false;
		}

				//version is updated ::claps:: proceed
		add_action( 'admin_enqueue_scripts', array( $this, 'tour_enqueue' ) );
	}

		/**
		 * Enqueue styles and scripts needed for the pointers.
		 */
	public function tour_enqueue()
	{

		if ( ! current_user_can( 'manage_options' ) ){
			return;
		}
		// Assume pointer shouldn't be shown
		$enqueue_pointer_script_style = false;

		// Get array list of dismissed pointers for current user and convert it to array
		$dismissed_pointers = explode( ',', get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );

				// Check if our pointer is not among dismissed ones
		if ( ! in_array( $this->pointer_close_id, $dismissed_pointers ) ) {

			$enqueue_pointer_script_style = true;

			// Add footer scripts using callback function
			add_action( 'admin_print_footer_scripts', array( $this, 'intro_tour') );
			add_action( 'wp_footer', array( $this, 'intro_tour' ) );
		}

		// Enqueue pointer CSS and JS files, if needed
		if ( $enqueue_pointer_script_style ) {
			wp_enqueue_style( 'wp-pointer' );
			wp_enqueue_script( 'wp-pointer' );
		}

	}


	/**
	 * Load the introduction tour
	 */
	public function intro_tour()
	{
		$adminpages = array(
				//array name is the unique ID of the screen @see: http://codex.wordpress.org/Function_Reference/get_current_screen
			'themes' => array(
				array(
						'content' => "<h3>" . __("0. ようこそ！DS BLOG テーマへ。", 'dsblog') . "</h3>"
								. "<p>" . __( "このテーマの使い方をツアー形式で体験していただけます。<br> <small> ※ このツアーは、「テーマ設定」からいつでも始めることができます。</small>", 'dsblog') . "</p>", //Content for this pointer
						'id' => '.theme-actions .customize.load-customize', //ID of element where the pointer will point
						'position' => array(
							'edge' => 'top', //Arrow position; change depending on where the element is located
							'align' => 'center' //Alignment of Pointer
						),
						'button2' => __('次へ', 'dsblog'), //text for the next button
						'function' => 'window.location="' . admin_url('customize.php') . '";' //where to take the user
					),
				),
				'customize' => array(
					array(
						'content' => '<h3>' . __('1.1 テーマデザインのカスタマイズ', 'dsblog') . '</h3>
						<p>' . __('デザインのカスタマイズはテーマカスタマイザーの左メニューから行えます。右側が実際のサイトページとなっています。 設定変更後は必ず保存ボタンをクリックしてください。', 'dsblog') . '</p>',
						'id' => '#accordion-section-title_tagline',
						'position' => array(
								'edge' => 'left', //Arrow position; change depending on where the element is located
								'align' => 'left' //Alignment of Pointer
						),
						// 'button2' => __('次へ', 'dsblog'),
						// 'function' => 'window.location="' . admin_url('customize.php#accordion-section-nav?welcome_tour=2') . '";'
					 ),
					 array(
						'content' => '<h3>' . __( '1.2 ウィジェットの設置', 'dsblog' ) . '</h3>
						<p>' . __( '「ウィジェットを追加」ボタンからウィジェットを選択し、設置することができます。
							編集後は必ず画面上部の保存ボタンをクリックしてください。
							<img width="200" src="' .  get_template_directory_uri() . '/assets/img/theme-customizer-widgets.gif' . ' " />', 'dsblog' ) . '</p>',
						'id' => '#accordion-section-gg_color_settings',
						'position' => array(
								'edge' => 'left', //Arrow position; change depending on where the element is located
								'align' => 'left', // ポインターのいち
						),
						'button2' => __( '次へ', 'dsblog' ),
						'function' => 'window.location="' . admin_url( 'widgets.php?welcome_tour=2' ) . '";',
					),
				),
				'widgets' => array(
					array(
						'content' => '<h3>' . __( '2.1 ウィジェットのカスタマイズ', 'dsblog' ) . '</h3>
						<p>' . __( 'ウィジェットは、サイトのパーツを簡単に設置するためのWordPressの機能です。<br>
						当テーマでは<strong>6つ</strong>のウィジェットエリアを用意しています。<br>
						また、通常のウィジェットに加え、以下の機能を拡張として用意しています。<br>
						<h2 style="margin-left: 20px">表示ページの設定</h2>
						<p>表示するユーザー・ページをウィジェットごとに詳細に設定可能です。</p>
						<h2 style="margin-left: 20px">自動追尾機能</h2>
						<p>スクロールすると自動的に画面内についてくるウィジェットを実装します。
						ただし、サイドバーエリアのみにしか適用されず、サイドバーより、メインコンテンツの高さの方が低い場合には適用されません。
						モバイルからのアクセス時も制限されます。
						</p>
						<h2 style="margin-left: 20px">スマートフォン表示設定</h2>
						<p>ウィジェットごとに、スマートフォンからのアクセスの場合の表示を設定することができます。
						</p>
						上記の機能は、各ウィジェットを設置後、クリックし開くことで設定項目が表示されます。
						', 'dsblog' ) . '</p>',
						'id' => '#sidebar-primary',
						'position' => array(
							'edge' => 'top',
							'align' => 'left',
						),
						'button2' => __( '次へ', 'dsblog' ),
						'function' => 'page_scroller("#wp-pointer-1");',
					),
					array(
						'content' => '<h3>' . __( '2.2 ウィジェットのカスタマイズ', 'dsblog') . '</h3>
						<p>' . __('ウィジェットは、サイトのパーツを簡単に設置するためのWordPressの機能です。<br> 当テーマでは<strong>6つ</strong>のウィジェットエリアを用意しています。また、独自のウィジェットを組み込んでおりますのでご活用下さい。
						<ul>
							<li>　・スライダー表示ウィジェット</li>
							<li>　・メールマガジン登録フォーム</li>
							<li>　・おすすめ投稿ウィジェット</li>
							<li>　・ランキングウィジェット</li>
							<li>　・動画ウィジェット</li>
							<li>　・プロフィールウィジェット</li>
							<li>　・ビジュアルウィジェット</li>
						</ul>', 'dsblog') . '</p>',
						'id' => '#available-widgets',
						'position' => array(
							'edge' => 'top',
							'align' => 'left',
						),
						'button2' => __('次へ', 'dsblog'),
						'function' => 'window.location="' . admin_url('nav-menus.php?welcome_tour=3') . '";'
					),
				),
				'nav-menus' => array(
					array(
						'content' => '<h3>' . __( '3. ナビゲーションのカスタマイズ', 'dsblog' ) . '</h3>
						<p>' . __( 'ヘッダー部のグローバルナビゲーションには、このページから編集します。
						左のメニュー一覧からページを選択し、「メニューに追加」ボタンをクリックすることでナビゲーションに追加できます。
						また、各メニューをクリック後、「アイコンを選択」からメニューに表示するアイコンを指定することができます。
						', 'dsblog' ) . '</p>',
						'id' => '#side-sortables',
						'position' => array(
								'edge' => 'left',
								'align' => 'left',
						),
						'button2' => __( '次へ', 'dsblog' ),
						'function' => 'window.location="' . admin_url('post-new.php?post_type=atslider&welcome_tour=3') . '";',
					),
				),
				'atslider' => array(
					array(
						'content' => "<h3>" . __( "4.1 スライダーの作成", 'dsblog' ) . "</h3>"
								. "<p>" . __( "先程紹介した「スライダー表示ウィジェット」では、予めスライダーを制作しておく必要があります。<br>
								「画像を追加」ボタンから画像を設定し、スライダークリック時のリンク先や、スライダー画像上に表示するコンテンツ・カラーの設定をしてください。", 'dsblog') . "</p>",
						'id' => '#submitdiv',
						'position' => array(
							'edge' => 'right',
							'align' => 'right',
						),
						'button2' => __( '次へ', 'dsblog' ),
						'function' => 'page_scroller("#_atslider_slider_repeat_group_repeat .add-group-row");',
					),
					array(
						'content' => "<h3>" . __( "4.2 スライダー画像の追加", 'dsblog' ) . "</h3>"
								. "<p>" . __( "スライダー画像は何枚でも指定することができます。
														 「画像を追加」ボタンから画像を設定し、「スライダーセットを追加」ボタンをクリックすることで複数枚のスライダーとして作成します。<br>
														 スライダーの編集終了後は、「公開ボタン」をクリックしてスライダーを保存してください。", 'dsblog' ) . "</p>",
						'id' => '#_atslider_slider_repeat_group_repeat .add-group-row',
						'position' => array(
							'edge' => 'bottom',
							'align' => 'left',
						),
						'button2' => __( '次へ', 'dsblog' ),
						'function' => 'window.location="' . admin_url('admin.php?page=affiliate_tools') . '";',
					),
				),
				'affiliate_tools' => array(
					array(
						'content' => "<h3>" . __("5. テーマ設定", 'dsblog') . "</h3>"
								. "<p>" . __("
									Facebook コメントやソーシャルシェアボタン、OGPの設定をこのページから編集することができます。<br>
									他のSEOプラグインを導入する際や、記事一覧ページにシェアボタンを導入したい際は訪れてみてください。<br>
									また、このツアーも改めて最初から開始することができます。", 'dsblog') . "</p>",
						'id' => '#wpbody-content',
						'position' => array(
								'edge' => 'top',
								'align' => 'left',
						),
						'button2' => __('次へ', 'dsblog'),
						'function' => 'window.location="' . admin_url('post-new.php') . '";',
					),
				),
				'post' => array(
					array(
						'content' => "<h3>" . __( "6. 投稿編集ページ", 'dsblog' ) . "</h3>"
												. "<p>" . __( '
													 <h2 style="margin-left: 20px">ページビルダー機能</h2>
													 <p>当テーマでは「ページビルダー」を使用しての記事投稿が可能です。<br>
													 右タブの「ページビルダー」をクリックすると、少し違う画面になります。<br>
													 左上のボタングループの左から2番目をクリックすると、カラムを入力するポップアップが出現します。<img src="' . get_template_directory_uri() . '/assets/img/page-builder.gif" style="width: 460px">
													 <small>カラムとは、横一列に並ぶ縦列のことです。</small><br>
													 次に一番左ボタンをクリックすることで、ウィジェットを設置することが可能です。<br>
													 「ビジュアルエディタ」を選択すると、通常と同じエディターで編集することができます。<br>
													 また、カラム間の濃い青色部分をドラッグすることで、各カラムの幅を変更できます。<br></p>
													 <h2 style="margin-left: 20px">SEO拡張機能</h2>
													 <p>各記事にSEOの詳細設定を施すことが可能です。<br>
													 空の場合にはデフォルトの設定が優先されます。
													 <h2 style="margin-left: 20px">OGP設定機能</h2>
													 <p>Facebook でシェアされる際の画像・タイトル・説明文を設定します。
													 </p>
													 ', 'dsblog' ) . "</p>",
							'id' => '#categorydiv',
							'position' => array(
								'edge' => 'right',
								'align' => 'left',
							),
							'button2' => __( '次へ', 'dsblog' ),
								// 'function' => 'window.location="' . admin_url('plugins.php?welcome_tour=7') . '";'
						),
					),
						// 'plugins' => array(
						// 						array(
						// 								'content' => "<h3>" . __("7. プラグインについて", 'dsblog') . "</h3>"
						// 										. "<p>" . __('
						// 																 このテーマはプラグインインストーラーを搭載しています。<br>
						// 																 上記プラグインのインストールを始めるからインストールすることで、
						// 																 便利なプラグインを一気にインストールすることが可能です。
						// 																 ', 'dsblog') . "</p>",
						// 								'id' => '#setting-error-tgmpa',
						// 								'position' => array(
						// 										'edge' => 'top',
						// 										'align' => 'left'
						// 								),
						// 								// 'button2' => __('次へ', 'dsblog'),
						// 								'function' => 'window.location="' . admin_url('admin.php?page=affiliate_tools') . '";'
						// 						),
						// 				),
				);


				$page = '';
				$screen = get_current_screen();

				//Check which page the user is on
				if ( isset( $_GET[ 'page' ] ) ) {
					$page = $_GET[ 'page' ];
				}

				if ( empty( $page ) ) {
					$page = $screen->id;
				}

				$function = '';
				$button2 = '';
				$opt_arr = array();
				if ( !$adminpages[$page] ) return ;
				foreach ( $adminpages[$page] as $key => $option) {
					//Location the pointer points
					if (!empty($adminpages[$page][$key]['id'])) {
							$id[$key] = $adminpages[$page][$key]['id'];
					} else {
							$id[$key] = $screen->id;
					}
					//Options array for pointer used to send to JS
					if ('' != $page && in_array($page, array_keys($adminpages[$page]))) {

							$align = ( is_rtl() ) ? 'right' : 'left';
							$opt_arr[$key] = array(
																	'content' => $adminpages[$page][$key]['content'],
																	'position' => array(
																			'edge' => (!empty($adminpages[$page][$key]['position']['edge'])) ? $adminpages[$page][$key]['position']['edge'] : 'left',
																			'align' => (!empty($adminpages[$page][$key]['position']['align'])) ? $adminpages[$page][$key]['position']['align'] : $align
																	),
																	'pointerWidth' => 500
													 );
							if (isset($adminpages[$page][$key]['button2'])) {
									$button2[$key] = (!empty($adminpages[$page][$key]['button2'])) ? $adminpages[$page][$key]['button2'] : '';
							}
							if (isset($adminpages[$page][$key]['function'])) {
									$function[$key] = $adminpages[$page][$key]['function'];
							}
					}
				}
				$this->print_scripts( $id , $opt_arr, __( "ツアーを終了" , 'dsblog'), $button2, $function);
		}


		/**
		 * Prints the pointer script
		 *
		 * @param string $selector The CSS selector the pointer is attached to.
		 * @param array $options The options for the pointer.
		 * @param string $button1 Text for button 1
		 * @param string|bool $button2 Text for button 2 (or false to not show it, defaults to false)
		 * @param string $button2_function The JavaScript function to attach to button 2
		 * @param string $button1_function The JavaScript function to attach to button 1
		 */
		public function print_scripts( $selectors, $options, $button1, $button2 = array(), $button2_function = '', $button1_function = '') {
			foreach ( $selectors as $key => $selector) { ?>
				<script type="text/javascript">
						//<![CDATA[
						(function ($) {

								var wordimpress_pointer_options_<?php echo $key ?> = <?php echo json_encode( $options[$key] ); ?>, setup_<?php echo $key ?>;
								//Userful info here
								wordimpress_pointer_options_<?php echo $key ?> = $.extend(wordimpress_pointer_options_<?php echo $key ?>, {
										buttons: function (event, t) {
												button = jQuery('<a id="pointer-close_<?php echo $key;?>" style="margin-left:5px" class="button-secondary">' + '<?php echo $button1; ?>' + '</a>');
												button.bind('click.pointer', function () {
														t.element.pointer('close');
												});
												return button;
										}
								});
								page_scroller = function( element ){
									var target = $(element).offset().top;
									$("html, body").animate({scrollTop:target}, 500);
									event.preventDefault();
									return false;
								}

								setup_<?php echo $key ?> = function () {
										$('<?php echo $selector; ?>').pointer(wordimpress_pointer_options_<?php echo $key ?>).pointer('open');
										<?php
										if ( $button2[$key] ) { ?>
										jQuery('#pointer-close_<?php echo $key;?>').after('<a id="pointer-primary_<?php echo $key;?>" class="button-primary">' + '<?php echo $button2[$key]; ?>' + '</a>');
										<?php } ?>
										jQuery('#pointer-primary_<?php echo $key;?>').click(function () {
												<?php echo $button2_function[$key]; ?>
										});
										jQuery('#pointer-close_<?php echo $key;?>').click(function () {
												<?php if ( $button1_function[$key] == '' ) { ?>
												$.post(ajaxurl, {
														pointer: '<?php echo $this->pointer_close_id; ?>', // pointer ID
														action: 'dismiss-wp-pointer'
												});
												<?php } else { ?>
												<?php echo $button1_function[$key]; ?>
												<?php } ?>
										});
								};
								if (wordimpress_pointer_options_<?php echo $key ?>.position && wordimpress_pointer_options_<?php echo $key ?>.position.defer_loading) {
										$(window).bind('load.wp-pointers', setup_<?php echo $key ?>);
								} else {
										$(document).ready(setup_<?php echo $key ?>);
								}

						})(jQuery);
						//]]>
				</script>
		<?php
			}
		}
}

$wordimpress_theme_tour = new WordImpress_Theme_Tour();

add_filter( 'admin_init', 'default_tour_start', 10, 1 );
function default_tour_start(){
	$flg = $_GET['default_tour_start'];
	if ( 'on' !== $flg && is_admin() ) return ;
	$meta = get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true );
	$dismissed_wp_pointers =  isset( $meta ) ? explode( ',', $meta ) : array();
	if ( in_array( 'wordimpress_tour', $dismissed_wp_pointers ) ) {

		foreach ( $dismissed_wp_pointers as $key => $value ) {
			if ( $value !== 'wordimpress_tour' ) {
				$new_dismissed_wp_pointers[] = $value;
			}
		}
		update_usermeta(  get_current_user_id(), 'dismissed_wp_pointers', implode( ',', $new_dismissed_wp_pointers ) );
	}
}
