<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テンプレートタグの設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

if ( ! function_exists( 'dsblog_paging_nav' ) ) :

	/**
	 * post & page navigation meta data
	 *
	 * @return void
	 */
	function dsblog_paging_nav() {

		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}
		if ( ! function_exists( 'wp_pagenavi' ) ) { ?>
		<nav class="navigation paging-navigation row" role="navigation">
			<div class="nav-links">
				<?php if ( get_next_posts_link() ) : ?>
				<div class="nav-previous btn btn-default pull-left"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> 前の記事一覧へ', 'dsblog' ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_previous_posts_link() ) : ?>
				<div class="nav-next btn btn-default pull-right"><?php previous_posts_link( __( '次の記事一覧へ <span class="meta-nav">&rarr;</span>', 'dsblog' ) ); ?></div>
				<?php endif; ?>
			</div>
		</nav>
		<?php
		} else { ?>
		<div class="navigation paging-navigation clearfix text-center" role="navigation">
			<?php wp_pagenavi(); ?>
		</div>
		<?php
		}
	}
endif;

if ( ! function_exists( 'dsblog_post_nav' ) ) :
	/**
	 * Display navigation to next/previous post when applicable.
	 *
	 * @return void
	 */
	function dsblog_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="navigation post-navigation" role="navigation">
			<div class="nav-links clearfix">
				<?php
					previous_post_link( '<div class="nav-previous btn btn-default pull-left">%link</div>', _x( '<span class="meta-nav"><i class="dashicons dashicons-arrow-left"></i> </span> %title', '前の記事一覧へ', 'dsblog' ) );
					next_post_link(     '<div class="nav-next btn btn-default pull-right">%link</div>',     _x( '%title <span class="meta-nav"><i class="dashicons dashicons-arrow-right"></i> </span>', '次の記事一覧へ',     'dsblog' ) );
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
endif;

if ( ! function_exists( 'dsblog_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function dsblog_posted_on() {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		// if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		// 	$time_string = '<time class="updated" datetime="%3$s">%4$s</time>';
		// }

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		printf( __( '<span class="posted-on">%1$s</span>', 'dsblog' ),
			$time_string
		);
	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 */
function dsblog_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Flush out the transients used in dsblog_categorized_blog.
 */
function dsblog_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'dsblog_category_transient_flusher' );
add_action( 'save_post',     'dsblog_category_transient_flusher' );
