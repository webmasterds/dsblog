<?php
/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマカスタマイザープレビュー js のロード
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 Drema Style
 * =====================================================
 */

function dsblog_customize_preview_js() {
	wp_enqueue_script( 'dsblog_customizer', THEME_CUSTOMIZER_URI . 'js/customizer.js', array( 'customize-preview' ), '20130508', false );
}
add_action( 'customize_preview_init', 'dsblog_customize_preview_js' );
