<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマカスタマイザー 設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 Drema Style
 * =====================================================
 */

/**
 * @name 拡張ファイルを読み込み
 */

define( 'THEME_CUSTOMIZER_DIR', get_stylesheet_directory() . '/inc/theme-customizer/' );
define( 'THEME_CUSTOMIZER_URI', get_stylesheet_directory_uri() . '/inc/theme-customizer/' );

require ( THEME_CUSTOMIZER_DIR . 'customizer.php' );
require ( THEME_CUSTOMIZER_DIR . 'class-media-uploader.php' );

add_action( 'customize_register', 'theme_mods_dsblog' );

function theme_mods_dsblog( $wp_customize ) {

	/************************************************************
	 *
	 * @name 初期設定削除
	 *
	 *************************************************************/

	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_section( 'colors' );

	/************************************************************
	 *
	 * @name 基本的な設定
	 *
	 * @section gg_general_settings : 見出しエリアを追加
	 * @param hedding_grad_color_top : 背景グラデーションカラー トップ
	 * @param hedding_grad_color_bottom : 背景グラデーションカラー ボトム
	 * @param hedding_font_size : フォントサイズ指定
	 *
	 *
	 *************************************************************/

	$wp_customize->
		add_section(
			'gg_general_settings',
			array(
				'title' => '基本的な設定',
				'priority' => 29,
			)
		);

	$wp_customize->
		add_setting(
			'theme_mods_dsblog[general_keywords]',
			array(
				'default' => '',
				'capability' => 'edit_theme_options',
				'type' => 'option',
				'transport' => 'postMessage',
			)
		);

	$wp_customize->
		add_control(
			'general_keywords',
			array(
				'label' => 'メタキーワードをカンマ区切りで入力してください。',
				'section' => 'gg_general_settings',
				'settings' => 'theme_mods_dsblog[general_keywords]',
			)
		);

	$wp_customize->add_setting(
		'theme_mods_dsblog[general_analytics_code]',
		array(
			'default' => '',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'general_analytics_code',
		array(
			'label' => 'Google analytics コードを入力してください',
			'section' => 'gg_general_settings',
			'settings' => 'theme_mods_dsblog[general_analytics_code]',
		)
	);


	/**
	 *
	 * @name ロゴの設定
	 *
	 * gg_header_customize : ヘッダー設定エリアを追加
	 * @param image_upload_logo : ロゴ画像アップロード
	 * @param logo_size : ロゴの大きさを設定
	 * @param logo_pattern : ロゴテキスト時のパターン
	 * @param logo_text : ロゴにテキストを使用する時に設定
	 * @param image_upload_logo : ロゴ画像がある場合に設定
	 *
	 *
	 ************************************************************
	 */

	$wp_customize->add_section(
		'gg_logo_settings',
		array(
			'title'    => 'ロゴの設定',
			'priority' => 30,
		)
	);

	//ロゴ表示
	$wp_customize->add_setting(
		'theme_mods_dsblog[logo_choice]',
		array(
			'default'    => 'image',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
			'transport'  => 'refresh',
		)
	);

	$wp_customize->add_control(
		'logo_choice',
		array(
			'label'    => __( 'ロゴ表示方法の選択してください', 'dsblog' ),
			'section'  => 'gg_logo_settings',
			'settings' => 'theme_mods_dsblog[logo_choice]',
			'type'     => 'radio',
			'choices'  => array(
				'text'   => 'テキスト',
				'image'  => '画像',
			) ,
		)
	);

	// 画像アップロード
	$wp_customize->add_setting(
		'theme_mods_dsblog[logo_image_uoload]',
		array(
			'capability' => 'edit_theme_options',
			'type'       => 'option',
			'transport'  => 'postMessage',
			'default'    => get_template_directory_uri() . '/assets/img/logo-default.png',
		)
	);

	$wp_customize->add_control(
		new MultiImageControl(
			$wp_customize,
			'logo_image_uoload',
			array(
				'label'    => __( 'ロゴ画像をアップロードしてください', 'dsblog' ),
				'section'  => 'gg_logo_settings',
				'settings' => 'theme_mods_dsblog[logo_image_uoload]',
			)
		)
	);

	//大きさをセレクト
	$wp_customize->add_setting(
		'theme_mods_dsblog[logo_size]',
		array(
			'default'    => 'normal',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
			'transport'  => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'logo_size',
		array(
			'label'    => __( 'ロゴの大きさを選択して下さい', 'dsblog' ),
			'section'  => 'gg_logo_settings',
			'settings' => 'theme_mods_dsblog[logo_size]',
			'type'     => 'radio',
			'choices'  => array(
				'small'  => '小',
				'normal' => '中',
				'full'   => 'フル',
			),
		)
	);

	//ロゴの位置
	$wp_customize->add_setting(
		'theme_mods_dsblog[logo_align]',
		array(
			'default'    => 'left',
			'capability' => 'edit_theme_options',
			'type'       => 'option',
			'transport'  => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'logo_align',
		array(
			'label'    => __( 'ロゴの位置を選択してください', 'dsblog' ),
			'section'  => 'gg_logo_settings',
			'settings' => 'theme_mods_dsblog[logo_align]',
			'type'     => 'radio',
			'choices'  => array(
					'left'   => '左',
					'center' => 'センター',
					'right'  => '右',
			) ,
		)
	);

	// ロゴテキストのカラー
	$wp_customize->add_setting(
		'theme_mods_dsblog[logo_text_color]',
		array(
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'default'           => '#252525',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'logo_text_color',
			array(
				'label' => __( 'テキスト時のロゴカラーを選択して下さい', 'dsblog' ),
				'section' => 'gg_logo_settings',
				'settings' => 'theme_mods_dsblog[logo_text_color]',
			)
		)
	);

	/************************************************************
	 *
	 * @name カラーの設定
	 *
	 *************************************************************/

	$wp_customize->add_section(
		'gg_color_settings',
		array(
			'title' => 'カラー設定',
			'priority' => 34,
			'description' => __( '', 'dsblog' ),
		)
	);

	// テーマカラー
	$wp_customize->add_setting(
		'theme_mods_dsblog[themecolor_themecolor]',
		array(
			'default' => '#081C61',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'themecolor_themecolor',
			array(
				'label'    => __( 'テーマカラーを設定してください。', 'dsblog' ) ,
				'section'  => 'gg_color_settings',
				'settings' => 'theme_mods_dsblog[themecolor_themecolor]',
			)
		)
	);

	// プライマリーカラー
	$wp_customize->add_setting(
		'theme_mods_dsblog[themecolor_primary]',
		array(
			'default'           => '#FB760D',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'postMessage',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'themecolor_primary',
			array(
				'label'    => __( 'プライマリーカラー ( ボタン ) を選択してください', 'dsblog' ),
				'section'  => 'gg_color_settings',
				'settings' => 'theme_mods_dsblog[themecolor_primary]',
			)
		)
	);

	// 補助色の設定
	$wp_customize->add_setting(
		'theme_mods_dsblog[themecolor_secondary]',
		array(
			'default'           => '#84909E',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'refresh',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'themecolor_secondary',
			array(
				'label'    => __( '補助色( 見出しカラー )を選択してください ※ 保存の画面を更新すると反映されます', 'dsblog' ),
				'section'  => 'gg_color_settings',
				'settings' => 'theme_mods_dsblog[themecolor_secondary]',
			)
		)
	);

	// リンク文字色
	$wp_customize->add_setting(
		'theme_mods_dsblog[themecolor_linkcolor]',
		array(
			'default'           => '#0066CC',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'themecolor_linkcolor',
			array(
				'label'    => __( 'リンクテキストカラーを指定してください', 'dsblog' ) ,
				'section'  => 'gg_color_settings',
				'settings' => 'theme_mods_dsblog[themecolor_linkcolor]',
			)
		)
	);

	// リンクホバー文字色
	$wp_customize->add_setting(
		'theme_mods_dsblog[themecolor_linkcolor_hover]',
		array(
			'default'           => '#FD7606',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'themecolor_linkcolor_hover', array(
			'label'    => __( 'リンクマウスホバー時のカラーを指定してください', 'dsblog' ) ,
			'section'  => 'gg_color_settings',
			'settings' => 'theme_mods_dsblog[themecolor_linkcolor_hover]',
	)));

	/**
	 *
	 * @name 背景の設定
	 *
	 * gg_background_settings : 背景設定エリアを追加
	 *
	 *
	 ************************************************************
	 */

	$wp_customize->add_section('gg_background_settings', array(
			'title'    => '背景の設定',
			'priority' => 30,
	));

	// 背景色の設定
	$wp_customize->add_setting('theme_mods_dsblog[background_color]', array(
			'default'           => '#FFFFFF',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability'        => 'edit_theme_options',
			'type'              => 'option',
			'transport'         => 'postMessage',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'background_color', array(
			'label'    => __('背景色の選択してください', 'dsblog') ,
			'section'  => 'gg_background_settings',
			'settings' => 'theme_mods_dsblog[background_color]',
	)));

	// 画像アップロード
	$wp_customize->add_setting('theme_mods_dsblog[background_image_upload]', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control(new MultiImageControl($wp_customize, 'background_image_upload', array(
			'label' => '背景画像をアップロードしてください',
			'section' => 'gg_background_settings',
			'settings' => 'theme_mods_dsblog[background_image_upload]',
	)));

	// 画像アップロード
	$wp_customize->add_setting('theme_mods_dsblog[header_background_image_upload]', array(
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control(new MultiImageControl($wp_customize, 'header_background_image_upload', array(
			'label' => 'ヘッダーの背景画像をアップロードしてください',
			'section' => 'gg_background_settings',
			'settings' => 'theme_mods_dsblog[header_background_image_upload]',
	)));

	/************************************************************
	 *
	 * @name フォントの設定
	 *
	 *************************************************************/

	$wp_customize->add_section('gg_font_settings', array(
			'title' => 'フォントの設定',
			'priority' => 36,
	));

	$wp_customize->add_setting('theme_mods_dsblog[font_base_size]', array(
			'default' => '14px',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('font_base_size', array(
			'label' => 'ベースとなるフォントのサイズを選択してください。',
			'section' => 'gg_font_settings',
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[font_base_size]',
			'choices' => array(
					'10px' => '10px',
					'11px' => '11px',
					'12px' => '12px',
					'13px' => '13px',
					'14px' => '14px',
					'15px' => '15px',
					'16px' => '16px',
					'17px' => '17px',
					'18px' => '18px',
					'19px' => '19px',
					'20px' => '20px',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[font_line_height]', array(
			'default' => '1.6em',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('font_line_height', array(
			'label' => '行間を設定してください。',
			'section' => 'gg_font_settings',
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[font_line_height]',
			'choices' => array(
					'1.0em' => '0',
					'1.1em' => '1',
					'1.2em' => '2',
					'1.3em' => '3',
					'1.4em' => '4',
					'1.5em' => '5',
					'1.6em' => '6',
					'1.7em' => '7',
					'1.8em' => '8',
					'1.9em' => '9',
					'2.0em' => '10',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[font_letter_space]', array(
			'default' => '0px',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('font_letter_space', array(
			'label' => '字間を設定してください。',
			'section' => 'gg_font_settings',
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[font_letter_space]',
			'choices' => array(
					'0px' => '0px',
					'1px' => '1px',
					'2px' => '2px',
					'3px' => '3px',
					'4px' => '4px',
					'5px' => '5px',
					'6px' => '6px',
					'7px' => '7px',
					'8px' => '8px',
					'9px' => '9px',
					'10px' => '10px',
			)
	));

	// 背景色の設定
	$wp_customize->add_setting(
		'theme_mods_dsblog[font_text_color]',
		array(
			'default' => '#252525',
			'sanitize_callback' => 'sanitize_hex_color',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'font_text_color',
			array(
				'label' => __( 'テキストカラーを選択してください', 'dsblog' ),
				'section' => 'gg_font_settings',
				'settings' => 'theme_mods_dsblog[font_text_color]',
			)
		)
	);

	/**
	 *
	 * @name レイアウトの設定
	 *
	 ************************************************************
	 */

	// トップページ
	$wp_customize->add_section(
		'gg_layout_settings',
		array(
			'title' => 'レイアウトの設定',
			'priority' => 32,
			'description' => 'トップページ・下層ページでそれぞれサイドバーの位置を設定することができます。',
		)
	);

	$wp_customize->add_setting(
		'theme_mods_dsblog[layout_top]',
		array(
			'default' => 'right',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
		)
	);

	$wp_customize->add_control(
		'layout_top',
		array(
			'label' => 'トップページ : サイドバーの位置をしてください',
			'section' => 'gg_layout_settings',
			'settings' => 'theme_mods_dsblog[layout_top]',
			'type' => 'radio',
			'choices' => array(
					'right' => '右',
					'left' => '左',
					'one_column' => 'ワンカラム',
			) ,
		)
	);

	// 下層
	$wp_customize->add_setting(
		'theme_mods_dsblog[layout_layer]',
		array(
			'default' => 'right',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
		)
	);

	$wp_customize->add_control(
		'layout_layer',
		array(
			'label' => '下層ページ : サイドバーの位置をしてください',
			'section' => 'gg_layout_settings',
			'settings' => 'theme_mods_dsblog[layout_layer]',
			'type' => 'radio',
			'choices' => array(
					'right' => '右',
					'left' => '左',
					'one_column' => 'ワンカラム',
			) ,
		)
	);

	/************************************************************
	 *
	 * @name 見出し
	 *
	 *************************************************************/

	$wp_customize->add_section('gg_heading_settings', array(
			'title' => '見出しの設定',
			'description' => '見出しに予め決められたスタイルを適用させることができます。※ 保存の画面を更新すると反映されます',
			'priority' => 37,
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_widget_title]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_widget_title', array(
			'label' => 'ウィジェットタイトル ',
			'section' => 'gg_heading_settings',
			'priority' => 1,
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_widget_title]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_entry_header]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_entry_header', array(
			'label' => '記事タイトル ',
			'section' => 'gg_heading_settings',
			'type' => 'select',
			'priority' => 2,
			'settings' => 'theme_mods_dsblog[heading_type_entry_header]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h1]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h1', array(
			'label' => 'h1(記事内) ',
			'section' => 'gg_heading_settings',
			'priority' => 3,
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h1]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h2]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h2', array(
			'label' => 'h2(記事内) ',
			'section' => 'gg_heading_settings',
			'priority' => 4,
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h2]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h3]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h3', array(
			'label' => 'h3(記事内) ',
			'priority' => 5,
			'section' => 'gg_heading_settings',
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h3]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h4]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h4', array(
			'label' => 'h4(記事内) ',
			'section' => 'gg_heading_settings',
			'priority' => 6,
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h4]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h5]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h5', array(
			'label' => 'h5(記事内) ',
			'priority' => 7,
			'section' => 'gg_heading_settings',
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h5]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[heading_type_h6]', array(
			'default' => 'type01',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('heading_type_h6', array(
			'label' => 'h6(記事内) ',
			'section' => 'gg_heading_settings',
			'priority' => 8,
			'type' => 'select',
			'settings' => 'theme_mods_dsblog[heading_type_h6]',
			'choices' => array(
					'type01' => 'タイプ01',
					'type02' => 'タイプ02',
					'type03' => 'タイプ03',
					'type04' => 'タイプ04',
					'type05' => 'タイプ05',
					'type06' => 'タイプ06',
			)
	));
	/************************************************************
	 *
	 * @name コメントの設定
	 *
	 * @section gg_ga_customize_settings : ga設定エリアの設定
	 * @param general_analytics_code : コードを入力し、設定。
	 *
	 *
	 *************************************************************/

	$wp_customize->add_section('gg_comment_settings', array(
			'title' => 'コメントの設定',
			'priority' => 36,
	));

	$wp_customize->add_setting('theme_mods_dsblog[comment_disp]', array(
			'default' => 'true',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('comment_disp', array(
			'label' => '固定ページでのコメント表示',
			'section' => 'gg_comment_settings',
			'type' => 'radio',
			'settings' => 'theme_mods_dsblog[comment_disp]',
			'choices' => array(
					'true' => '表示する',
					'false' => '表示しない',
			)
	));

	$wp_customize->add_setting('theme_mods_dsblog[comment_facebook]', array(
			'default' => 'true',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('comment_facebook', array(
			'label' => 'Facebookコメントを表示しますか？',
			'section' => 'gg_comment_settings',
			'type' => 'radio',
			'settings' => 'theme_mods_dsblog[comment_facebook]',
			'choices' => array(
					'true' => '表示する',
					'false' => '表示しない',
			)
	));

	/************************************************************
	 *
	 * @name フッターの設定
	 *
	 * @section gg_footer_customize : フッター設定エリアを追加
	 * @param footer_copyright : コピーライトテキストの追加
	 *
	 *************************************************************/

	$wp_customize->add_section('gg_footer_settings', array(
			'title' => 'フッター設定',
			'priority' => 37,
	));

	$wp_customize->add_setting('theme_mods_dsblog[footer_copyright]', array(
			'default' => 'copyright © ' . the_date('Y') . get_bloginfo('name') . ' all right reserved.',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('footer_copyright', array(
			'label' => 'コピーライトテキスト',
			'section' => 'gg_footer_settings',
			'settings' => 'theme_mods_dsblog[footer_copyright]',
	));

	//ページトップ
	$wp_customize->add_setting('theme_mods_dsblog[footer_pagetop_disp]', array(
			'default' => 'true',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('footer_pagetop_disp', array(
			'settings' => 'theme_mods_dsblog[footer_pagetop_disp]',
			'label' => 'このページの先頭へボタン表示しますか？',
			'section' => 'gg_footer_settings',
			'type' => 'radio',
			'choices' => array(
					'true' => '表示する',
					'false' => '表示しない',
			) ,
	));
	/**
	 * ナビゲーション
	 */
	$wp_customize->add_setting('theme_mods_dsblog[navigation_color]', array(
			'default' => 'default',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('theme_mods_dsblog[navigation_color]', array(
			'label' => 'ナビゲーションカラー',
			'section' => 'nav',
			'settings' => 'theme_mods_dsblog[navigation_color]',
			'type' => 'radio',
			'choices' => array(
					'inverse' => '黒',
					'default' => '白',
					'relative' => 'テーマカラー',
			),
	));


	$wp_customize->add_setting('theme_mods_dsblog[layout_navigation]', array(
			'default' => 'true',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('layout_navigation', array(
			'label' => 'ナビゲーションの表示',
			'section' => 'nav',
			'settings' => 'theme_mods_dsblog[layout_navigation]',
			'type' => 'radio',
			'choices' => array(
					'false' => '表示しない',
					'true' => '表示する',
			) ,
	));

	/**
	 *
	 * @name デザイン詳細設定
	 *
	 ************************************************************
	 */

	// トップページ
	$wp_customize->add_section('gg_design_settings', array(
			'title' => 'デザイン詳細設定',
			'priority' => 45,
			'description' => 'デザイン詳細部の設定をすることが可能です。'
	));

	$wp_customize->add_setting('theme_mods_dsblog[design_radius]', array(
			'default' => '3px',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('design_radius', array(
			'label' => '角丸 (px)',
			'section' => 'gg_design_settings',
			'type' => 'text',
			'settings' => 'theme_mods_dsblog[design_radius]',
	));

	$wp_customize->add_setting('theme_mods_dsblog[design_shadow]', array(
			'default' => '3px',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'refresh',
	));

	$wp_customize->add_control('design_shadow', array(
			'label' => 'ボックスシャドウ (px)',
			'section' => 'gg_design_settings',
			'type' => 'text',
			'settings' => 'theme_mods_dsblog[design_shadow]',
	));

	$wp_customize->add_setting('theme_mods_dsblog[design_width]', array(
			'default' => '1000px',
			'capability' => 'edit_theme_options',
			'type' => 'option',
			'transport' => 'postMessage',
	));

	$wp_customize->add_control('design_width', array(
			'label' => 'サイトの幅',
			'section' => 'gg_design_settings',
			'type' => 'text',
			'settings' => 'theme_mods_dsblog[design_width]',
	));



}
