<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage TinyMCE ポップアップテンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 Drema Style
 * =====================================================
 */

ob_start();

$file = dirname( __FILE__ );
$file_name = substr( $file, 0, stripos( $file, "dsblog" ) );
require( $file_name . '../../wp-load.php' );
global $dsblog_shortcodes;

?>
<head>
<title>ショートコードの挿入</title>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ; ?>/inc/shortcodes/tinymce-popup.js"></script>
<style>
	body{
		padding-left: 10px;
		padding-right: 10px;
		font-size: 12px;
	}
	table{
		font-size: 12px;
	}
</style>
<script>
var ShortcodesDialog = {
	insert : function( file , title ) {
		var ed = tinyMCEPopup.editor, dom = ed.dom;
		tinyMCEPopup.execCommand('mceInsertContent', false, '');
		tinyMCEPopup.close();
	}
};
tinyMCEPopup.onInit.add(ShortcodesDialog.init, ShortcodesDialog);
</script>
<?php wp_head();?>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ; ?>/assets/js/scripts.min.js"></script>
</head>

<body>
<div class="yinstr">
	<p></p>
	<p>下記から挿入するショートコードを選択してください。</p>
</div>

<form onSubmit="ShortcodesDialog.insert();return false;" action="#" method="post">
<div class="mceActionPanel">
<script type="text/javascript" language="javascript">
var dsblog_sel_content = tinyMCE.activeEditor.selection.getContent();
</script>
<?php
echo "<div><table id='shortcodes_table' class='table table-bordered'>";
foreach( $dsblog_shortcodes as $tagname => $tag ) {
	$attr_param = '';
	foreach ($tag['param'] as $paramename => $param ) {
		$attr_param .= strval( $paramename ) . '='. $param['param'][0] . ' ';
	}
?>
		<tr>
			<td style="width: 300px">
				<h4><code><a href="javascript:;" onClick="tinyMCEPopup.close();" onmousedown="tinyMCE.execCommand('mceInsertContent',false,'[<?php echo $tagname ;?> <?php echo $attr_param; ?>]' + dsblog_sel_content + '[/<?php echo $tagname;?>]');"><?php echo $tag['title'] ;?></a></code></h4><br>
				<a href="javascript:;" onClick="tinyMCEPopup.close();" onmousedown="tinyMCE.execCommand('mceInsertContent',false,'[<?php echo $tagname ;?> <?php echo $attr_param; ?>]' + dsblog_sel_content + '[/<?php echo $tagname;?>]');" class="btn btn-default btn-sm">挿入</a>
			</td>
			<td>
			<table class="table table-bodered">
				<tr>
					<td><?php echo $tag['title'] ?> - <?php echo $tag['desc'] ?></td>
				</tr>
				<tr>
					<td><code><?php echo $tag['example'] ?></code></td>
				</tr>
				<tr>
					<td>
					<!-- Button trigger modal -->
					<a href="javascript:;" class="dsmodal btn btn-primary btn-sm" data-toggle="modal" data-target="#Modal<?php echo $tagname ;?>">
						詳細を見る
					</a>
					<!-- Modal -->
					<div class="modal fade" id="Modal<?php echo $tagname ;?>" tabindex="-1" role="dialog" aria-labelledby="ModalLable<?php echo $tagname ;?>" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="ModalLable<?php echo $tagname ;?>">詳細</h4>
								</div>
								<div class="modal-body">
									<table class="table table-bordered">
									<thead>
										<th>キー</th>
										<th>説明</th>
										<th>値</th>
									</thead>
									<?php foreach ($tag['param'] as $key => $value) { ?>
										<tr>
											<td><?php echo $key ?></td>
											<td><?php echo $value['desc']; ?><br>
											サンプル : <code>[<?php echo $tagname ;?> <?php echo $key ?>="<?php echo $value['param'][0] ?>"]</code>
											</td>
											<td>
											<?php foreach ( $value['param'] as $nest_param_key => $nest_param ) {
												echo $nest_param . '</br>';
												} ?>
											</td>
										</tr>
								 <?php } ?>
									</table>
									<?php echo do_shortcode( $tag['example'] ) ?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
								</div>
							</div>
						</div>
					</div>
					</td>
				</tr>
			</table>

			</td>
		</tr>
<?php }
echo "</table></div>";
?>
</div>

<div class="mceActionPanel">
	<div style="float:left;padding-top:5px">
	</div>
</div>
</form>
</body>
