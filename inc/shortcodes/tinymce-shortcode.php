<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage TinyMCE テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 Drema Style
 * =====================================================
 */

add_action( 'init', 'dsblog_buttons' );

// button add filter
function dsblog_buttons() {
		add_filter( "mce_external_plugins", "dsblog_add_buttons" );
		add_filter( 'mce_buttons', 'dsblog_register_buttons' );
}

// add button
function dsblog_add_buttons( $plugin_array ) {
		$plugin_array['dsblog'] = get_template_directory_uri() . '/inc/shortcodes/shortcode-plugin.js';
		return $plugin_array;
}

// register button
function dsblog_register_buttons( $buttons ) {
		array_push( $buttons, 'ds_shortcodes' );
		return $buttons;
}

