( function () {
	tinymce.create( 'tinymce.plugins.dsblog', {
		init: function ( ed, url ) {

			ed.addButton( 'ds_shortcodes', {
				title: 'ショートコードの挿入',
				cmd: 'ds_shortcodes',
				image: url + '/shortcode-icon.png'
			} );

			ed.addCommand( 'ds_shortcodes', function () {
				ed.windowManager.open( {
					title: 'ショートコードの挿入',
					file: url + '/shortcodes-dialog.php',
					width: 920 + ed.getLang( 'shortcodes.delta_width', 0 ),
					height: 600 + ed.getLang( 'shortcodes.delta_height', 0 ),
					inline: 1
				}, {
					plugin_url: url,
					some_custom_arg: 'custom arg'
				} );
			} );
		},
		createControl: function ( n, cm ) {
			return null;
		},

		getInfo: function () {
			return {
				longname: 'ShortCode Buttons',
				author: 'GrowGroup',
				authorurl: 'http://grow-group.jp',
				infourl: 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
				version: "0.1"
			};
		}
	} );

	tinymce.PluginManager.add( 'dsblog', tinymce.plugins.dsblog );
} )();
