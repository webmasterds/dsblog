<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ウィジェット設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

function dsblog_custom_sidebar() {
		$type = get_theme_mod( 'heading_type_widget_title', 'type01' );
		register_sidebar(array(
				'name' => __('サイドバー', 'dsblog') ,
				'id' => 'sidebar-primary',
				'before_widget' => '<section class="widget widget-sidebar %1$s %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));

		register_sidebar(array(
				'name' => __('ヘッダー広告エリア', 'dsblog') ,
				'id' => 'header-primary',
				'before_widget' => '<section class="widget widget-header %1$s %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));

		register_sidebar(array(
				'name' => __('メインヴィジュアルエリア', 'dsblog') ,
				'id' => 'main-visual-primary',
				'before_widget' => '<section class="widget widget-content %1$s %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));

		register_sidebar(array(
				'name' => __('コンテンツ上部', 'dsblog') ,
				'id' => 'content-primary',
				'before_widget' => '<section class="widget widget-content %1$s %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));

		register_sidebar(array(
				'name' => __('コンテンツ下部', 'dsblog') ,
				'id' => 'content-secondary',
				'before_widget' => '<section class="widget widget-content %1$s %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));

		register_sidebar(array(
				'name' => __('フッター', 'dsblog') ,
				'id' => 'footer-primary',
				'before_widget' => '<section class="widget widget-footer %1$s %2$s col-xs-24 col-lg-6 col-md-6">',
				'after_widget' => '</section>',
				'before_title' => '<h3 class="widget-title ' . $type . ' secondary-border-top">',
				'after_title' => '</h3>',
		));
}

// Hook into the 'widgets_init' action
add_action('widgets_init', 'dsblog_custom_sidebar');

/**
 * フッターにウィジェットがセットされているか確認
 * @return bool
 */
function is_footer_widget(){
	$sidebar_setting = get_option( 'sidebars_widgets', false );
	if ( $sidebar_setting['footer-primary'] ) {
		return true ;
	}
	return false;
}
