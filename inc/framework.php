<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 管理画面パネル 設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

require_once get_template_directory() . '/inc/framework/titan-framework.php';

$dsblog = TitanFramework::getInstance( 'dsblog' );


/**
 * 管理パネル作成
 */
$admin_panel = $dsblog->createAdminPanel( array(
		'name' => 'テーマ設定',
		'id'   => 'affiliate_tools'
) );

	$default_tab = $admin_panel->createTab( array(
			'name' => 'テーマ設定',
			'id'   => 'default_settings'
	) );

		$default_tab->createOption( array(
				'name' => '画像素材の読み込み',
				'id' => 'default_free_images',
				'type' => 'note',
				'desc' => '予め用意されているフリー素材を WordPress にインポートします。<a href="' . admin_url('admin.php?page=affiliate_tools&free_image_import=on') . '" class="button-primary"> 画像をインポート </a>',
				'default' => '',
		) );

		$default_tab->createOption( array(
				'name' => 'ツアーの開始',
				'id' => 'default_tour_start',
				'type' => 'note',
				'desc' => 'テーマの使い方ツアーをもう一度開始します。
				<a href="' . admin_url('themes.php?default_tour_start=on') . '" class="button-primary"> ツアーを開始する </a>',
				'default' => '',
		) );

	// 基本設定タブ
	$general_tab = $admin_panel->createTab( array(
			'name' => 'Facebook コメント設定',
			'id'   => 'general_settings'
	) );

		$general_tab->createOption( array(
				'name' => 'Facebook API キー',
				'id' => 'facebook_api_key',
				'type' => 'text',
				'desc' => 'Facebookコメントを表示するためにFacebookアプリとの連携をする必要があります。Facebookアプリはこちらから作成し、APIキーを下記のフォームに入力してください。',
				'default' => '',
		) );

		$general_tab->createOption( array(
				'name' => 'Facebook コメント : 表示数',
				'id' => 'facebook_comment_num',
				'type' => 'text',
				'desc' => '実際に表示するコメント数を入力してください。',
				'default' => '10',
		) );

		$general_tab->createOption( array(
				'name' => 'Facebook コメント : 幅',
				'id' => 'facebook_comment_width',
				'type' => 'text',
				'desc' => 'コメント欄の幅を入力してください。(単位: px)',
				'default' => '720px',
		) );

		$general_tab->createOption( array(
				'name' => 'Facebook コメント : カラー',
				'id' => 'facebook_comment_color',
				'type' => 'select',
				'desc' => 'カラースキームを選択してください。',
				'options' => array(
						'light' => 'ライト',
						'dark' => 'ダーク',
				),
				'default' => 'light',
		) );

		$general_tab->createOption( array(
				'type' => 'save',
		) );

/**
 * ==========================
 *
 * ソーシャル設定
 *
 * ==========================
 */
	$social_tab = $admin_panel->createTab( array(
			'name' => 'SNS設定',
			'id'   => 'social_setting'
	) );

		$social_tab->createOption( array(
				'name' => 'Twitter 設定',
				'id' => 'twitter_acount_key',
				'type' => 'text',
				'desc' => 'TwitterのアカウントIDを入力してください。',
				'default' => '',
		) );

		$social_tab->createOption( array(
				'name' => 'シェアボタン 表示設定',
				'id' => 'sherebtn_disp',
				'type' => 'multicheck',
				'desc' => '表示するシェアボタンを設定してください。' ,
				'options' => array(
						'facebook' => 'Facebook',
						'twitter' => 'Twitter',
						'hatena' => 'はてなブックマーク',
						'line' => 'LINE',
						'google_plus' => 'Google +',
				),
				'default' => 'facebook',
		) );

		$social_tab->createOption( array(
				'name' => 'ソーシャルボタン : 投稿一覧ページ',
				'id' => 'post_group_sherebtn_button_disp',
				'type' => 'checkbox',
				'desc' => '投稿一覧へのボタン表示・非表示を設定してください。',
				'options' => array(
						'false' => '表示しない',
						'true' => '表示する',
				),
				'default' => 'true',
		) );

		$social_tab->createOption( array(
				'name' => 'ソーシャルボタン : 投稿ページ',
				'id' => 'single_sherebtn_button_disp',
				'type' => 'checkbox',
				'desc' => '投稿ページでのシェアボタン表示・非表示を設定してください。',
				'options' => array(
						'false' => '表示しない',
						'true' => '表示する',
				),
				'default' => 'true',
		) );

		$social_tab->createOption( array(
				'type' => 'save',
		) );

/**
 * ==========================
 *
 * OGP 設定
 *
 * ==========================
 */
	$ogp_tab = $admin_panel->createTab( array(
			'name' => 'OGP設定',
			'id'   => 'ogp_setting'
	) );

		$ogp_tab->createOption( array(
				'name' => 'OGP出力設定',
				'id' => 'ogp_post_meta_disp',
				'type' => 'radio',
				'desc' => '投稿画面でのOGP設定の表示・非表示を選択することができます。( 他のSEOプラグインを利用する際はオフにしてください。 )',
				'default' => 'true',
				'options' => array(
						'false' => '表示しない',
						'true' => '表示する',
				),
		) );

		$ogp_tab->createOption( array(
				'name' => 'OGP : トップページタイトル',
				'id' => 'ds_blog_default_title',
				'type' => 'text',
				'desc' => 'トップページのOGPタイトルを入力してください。( 空欄時にはサイト名が入ります。 )',
				'default' => get_bloginfo( 'name' ),
		) );

		$ogp_tab->createOption( array(
				'name' => 'OGP : トップページ説明文',
				'id' => 'ds_blog_default_desctiption',
				'type' => 'textarea',
				'desc' => 'トップページの説明文を入力してください。( 空欄時にはサイトのキャッチフレーズが入ります。 )',
				'default' => get_bloginfo( 'description' ),
		) );

		$ogp_tab->createOption( array(
				'name' => 'OGP : デフォルト画像',
				'id' => 'ds_blog_default_ogp_image',
				'type' => 'upload',
				'desc' => 'シェア時に表示されるサムネイル画像のデフォルト画像を設定することが可能です。',
				'default' => '',
		) );


		$ogp_tab->createOption( array(
				'type' => 'save',
		) );

/**
 * ==========================
 * おすすめ投稿記事
 *
 * ==========================
 */

	$recommend_panel = $admin_panel->createAdminPanel( array(
			'name' => 'おすすめの投稿',
			'id'   => 'affiliate_tools_recommend'
	) );

		$recommend_panel->createOption( array(
				'name' => '投稿記事を選択',
				'id' => 'affiliate_tools_recommend_post',
				'type' => 'multicheck-posts',
				'desc' => 'おすすめの投稿記事を選択してください。',
		) );

		$recommend_panel->createOption( array(
				'type' => 'save',
		) );


/**
	* ==================================
	* 投稿メタ
	*
	*/


	$seo_meta_box = $dsblog->createMetaBox( array(
			'name' => '<div class="dashicons dashicons-welcome-view-site"></div> SEO設定',
			'id'   => 'ds_blog_seo',
			'post_type' => array( 'page', 'post' ),
			'desc' => ''
	) );

		global $post;
		$seo_meta_box->createOption( array(
				'name' => '記事タイトル',
				'id' => 'ds_blog_post_title',
				'type' => 'text',
				'desc' => '記事タイトルを入力してください。',
				'default' => '',
		) );

		$seo_meta_box->createOption( array(
				'name' => '説明文',
				'id' => 'ds_blog_post_description',
				'type' => 'textarea',
				'desc' => '説明文を入力してください。',
				'default' => '',
		) );

		$seo_meta_box->createOption( array(
				'name' => 'キーワード',
				'id' => 'ds_blog_post_keyword',
				'type' => 'text',
				'desc' => 'キーワードをカンマ(,)区切りで入力してください。',
				'default' => '',
		) );

		$seo_meta_box->createOption( array(
				'name' => '<div class="dashicons dashicons-admin-post"></div> OGP 設定',
				'type' => 'heading',
		) );

		$seo_meta_box->createOption( array(
				'name' => 'OGP : 出力設定',
				'id' => 'ds_blog_post_ogp_disp',
				'type' => 'radio',
				'desc' => 'OGP タグを出力するかどうか設定してください。( Facebook , Twiiter の設定は<a href="'. admin_url('admin.php?page=affiliate_tools') .'">こちら</a> から行うことができます。)',
				'default' => 'true',
				'options' => array(
					'true' => '出力する',
					'false' => '出力しない',
				)
		) );

		$seo_meta_box->createOption( array(
				'name' => 'OGP : タイトル',
				'id' => 'ds_blog_post_ogp_title',
				'type' => 'text',
				'desc' => 'シェアされる時に表示するタイトルを入力してください。',
				'default' => '' ,
		) );

		$seo_meta_box->createOption( array(
				'name' => 'OGP : 説明文',
				'id' => 'ds_blog_post_ogp_description',
				'type' => 'textarea',
				'desc' => 'シェアされる時に表示すされる説明文を入力してください。',
				'default' => '',
		) );

		$seo_meta_box->createOption( array(
				'name' => 'OGP : 画像上書き',
				'id' => 'ds_blog_post_ogp_image',
				'type' => 'upload',
				'desc' => 'シェアされる時の画像を上書きすることが可能です。(デフォルトではアイキャッチ画像が使用されます。)',
				'default' => '',
		) );

