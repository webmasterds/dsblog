<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 基本的な設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

if ( ! function_exists( 'dsblog_setup' ) ) :
function dsblog_setup() {
	load_theme_textdomain( 'dsblog', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'menus' );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );

	register_nav_menus( array(
			'primary' => __( 'ヘッダーナビゲーション', 'dsblog' ),
	) );

	add_theme_support( 'nice-search' );
}
add_action( 'after_setup_theme', 'dsblog_setup' );
endif;

//attach our function to the wp_pagenavi filter
add_filter( 'wp_pagenavi', 'ik_pagination', 10, 2 );

//customize the PageNavi HTML before it is output
function ik_pagination($html) {
		$out = '';

		//wrap a's and span's in li's
		$out = str_replace("<div","",$html);
		$out = str_replace("class='wp-pagenavi'>","",$out);
		$out = str_replace("<a","<li><a",$out);
		$out = str_replace("</a>","</a></li>",$out);
		$out = str_replace("<span","<li><span",$out);
		$out = str_replace("</span>","</span></li>",$out);
		$out = str_replace("</div>","",$out);

		return '<div class=" pagination-centered">
						<ul class="pagination primary-color">'.$out.'</ul>
				</div>';
}

/**
 * ビジュアルエディターにスタイルを適用
 * @return add editor style
 */
function my_theme_add_editor_styles() {
		add_editor_style( 'assets/css/editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );
