<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage その他 関数
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @param array $args Configuration arguments.
 * @return array
 */
function dsblog_page_menu_args($args) {
	$args['show_home'] = true;
	return $args;
}
add_filter('wp_page_menu_args', 'dsblog_page_menu_args');

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function dsblog_body_classes($classes) {

	// Adds a class of group-blog to blogs with more than 1 published author.
	if (is_multi_author()) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter('body_class', 'dsblog_body_classes');

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function dsblog_wp_title($title, $sep) {
	if (is_feed()) {
		return $title;
	}

	global $page, $paged;

	$title.= get_bloginfo('name', 'display');

	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) {
		$title.= " $sep $site_description";
	}

	if ($paged >= 2 || $page >= 2) {
		$title.= " $sep " . sprintf(__('Page %s', 'dsblog') , max($paged, $page));
	}

	return $title;
}
add_filter('wp_title', 'dsblog_wp_title', 10, 2);

/**
 * Sets the authordata global when viewing an author archive.
 *
 * @global WP_Query $wp_query WordPress Query object.
 * @return void
 */
function dsblog_setup_author() {
	global $wp_query;

	if ($wp_query->is_author() && isset($wp_query->post)) {
		$GLOBALS['authordata'] = get_userdata($wp_query->post->post_author);
	}
}
add_action('wp', 'dsblog_setup_author');

/**
 * 画像のアップロード
 */

add_filter( 'attachment_thumbnail_args', 'custom_attachment_thumbnail_args', 10, 1 );
function custom_attachment_thumbnail_args(  ){

}
function ds_media_upload_theme_switch () {
	// media upload dir
	$wp_upload_dir  = wp_upload_dir();

	$root_template = get_template_directory() . '/assets/img/pattern/';

	// copy template dir to upload dir
	$filenames = ds_theme_activate_list_files( $root_template );
	foreach ( $filenames as $key => $filename ) {
		if ( !file_exists( $filename )  ) continue ;
		$copy_flg = copy( $filename, $wp_upload_dir['path'] . '/' . basename( $filename ) );
	}

	$root_upload_dir = $wp_upload_dir['path'] . '/';
	$upload_filenames = ds_theme_activate_list_files( $root_upload_dir );

	foreach ( $upload_filenames as $key => $filename ) {
		$parent_post_id = 0;
		$filetype       = wp_check_filetype( basename($filename) , null);
		$attachment     = array(
			'guid'                => $wp_upload_dir['url'] . '/' . basename( $filename ) ,
			'post_mime_type'      => $filetype['type'],
			'post_title'          => preg_replace('/\.[^.]+$/', '', basename($filename)) ,
			'post_content'        => '',
			'post_status'         => 'inherit'
		);
		$attach_id      = wp_insert_attachment( $attachment, $filename, $parent_post_id );
		require_once ( ABSPATH . 'wp-admin/includes/image.php');
		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id,  $filename  );
		$return = wp_update_attachment_metadata( $attach_id , $attach_data );
	}
	$return = add_option( 'ds_media_upload_theme_switch', 'true' );
	return $return;
}

/**
 * ファイル名を取得
 * @param $dir
 */

function ds_theme_activate_list_files( $dir , $fileonly=false  ) {
	$files = array();
	$list  = scandir( $dir );

	if ( empty( $list ) ) return ;
	foreach ( $list as $file ) {
		if ( $fileonly ) {
			if ( $file == '.' || $file == '..' ) {
				continue;
			} else if ( is_file( $dir . $file ) && ( strstr( $file , '.png' ) || strstr( $file , '.jpg' ) ) ) {
				$files[] = $file;
			} else if ( is_dir( $dir . $file )) {
				$files = array_merge( $files, ds_theme_activate_list_files(  $file . '/' ));
			}
		} else {
			if ( $file == '.' || $file == '..' ) {
				continue;
			} else if ( is_file( $dir . $file ) && ( strstr( $file , '.png' ) || strstr( $file , '.jpg' ) ) ) {
				$files[] = $dir . $file ;
			} else if ( is_dir( $dir . $file )) {
				$files = array_merge( $files, ds_theme_activate_list_files( $dir . $file . '/' ));
			}
		}
	}
	return $files;
}

add_filter( 'admin_init', 'ds_free_image_import', 10, 1 );
function ds_free_image_import(){
	if ( !is_admin() || !is_user_logged_in() ) return;
	$flg = $_GET['free_image_import'];
	if ( $flg === 'on' ) {
		ds_media_upload_theme_switch ();
		$return = update_option( 'ds_media_upload_theme_switch', 'true' );
	}
}
