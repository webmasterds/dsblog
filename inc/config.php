<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage テーマの内部的な設定
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

function dsblog_main_class() {
	$layout = get_dsblog_layout();
	if ( dsblog_display_sidebar() ) {
		// Classes on pages with the sidebar
		$class = 'col-sm-18 col-xs-24 col-lg-18 ' . $layout['main'] ;
	} else {
		// Classes on full width pages
		$class = 'col-sm-24  col-xs-24 col-lg-24 ';
	}
	return $class;
}

/**
 * .sidebar classes
 * @return string
 */
function dsblog_sidebar_class() {
	$layout = get_dsblog_layout();
	return 'col-sm-6 col-xs-24 col-lg-6 ' . $layout['side'] ;
}

/**
 * Define which pages shouldn't have the sidebar
 */

function dsblog_display_sidebar() {
	$sidebar_config = new DSblog_Sidebar(
		array(
			'is_404',
			// 'is_front_page'
		),
		array(
			// 'template-custom.php'
		)
	);
	return apply_filters('dsblog_display_sidebar', $sidebar_config->display);
}

/**
 * $content_width is a global variable used by WordPress for max image upload sizes
 */
if (!isset($content_width)) { $content_width = 1140; }

/**
 * Determines whether or not to display the sidebar based on an array of conditional tags or page templates.
 *
 * @link http://dsblog.io/the-dsblog-sidebar/
 * @param array list of conditional tags (http://codex.wordpress.org/Conditional_Tags)
 * @param array list of page templates. These will be checked via is_page_template()
 * @return boolean True will display the sidebar, False will not
 */
class DSblog_Sidebar {
	private $conditionals;
	private $templates;

	public $display = true;

	function __construct($conditionals = array(), $templates = array()) {
		$this->conditionals = $conditionals;
		$this->templates    = $templates;

		$conditionals = array_map(array($this, 'check_conditional_tag'), $this->conditionals);
		$templates    = array_map(array($this, 'check_page_template'), $this->templates);

		if (in_array(true, $conditionals) || in_array(true, $templates)) {
			$this->display = false;
		}
	}

	private function check_conditional_tag($conditional_tag) {
		if (is_array($conditional_tag)) {
			return $conditional_tag[0]($conditional_tag[1]);
		} else {
			return $conditional_tag();
		}
	}

	private function check_page_template($page_template) {
		return is_page_template($page_template);
	}
}
