<?php
/**
 * Affiliate Tools.
 *
 * @package   Affiliate_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * 基本クラス
 * @package Affiliate_Tools
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools {

	/**
	 * バージョン定義
	 *
	 * @since   1.0.0
	 * @var     string
	 */
	const VERSION = '1.0.0';

	/**
	 * スラッグ定義
	 *
	 * @since    1.0.0
	 * @var      string
	 */
	protected $plugin_slug = 'affiliate-tools';

	/**
	 * インスタンス
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// plugin, textdomain loaded
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// multisite load
		// add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// css & javascript load.
		// add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		// add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    // shortcode
    add_shortcode('sharebtn', array( $this , 'sharebtn_shortcode_create' ) );
	}

	/**
	 * plugin slug
	 *
	 * @since    1.0.0
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * create instance
	 *
	 * @since     1.0.0
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 */
	public static function activate( $network_wide ) {
		if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			if ( $network_wide  ) {
				$blog_ids = self::get_blog_ids();
				foreach ( $blog_ids as $blog_id ) {
					switch_to_blog( $blog_id );
					self::single_activate();
				}
				restore_current_blog();
			} else {
				self::single_activate();
			}
		} else {
			self::single_activate();
		}
	}

	/**
	 * 無効化時のアクション
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 *                                       "Network Deactivate" action, false if
	  *                                       WPMU is disabled or plugin is
	 *                                       deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_deactivate();

				}

				restore_current_blog();

			} else {
				self::single_deactivate();
			}

		} else {
			self::single_deactivate();
		}

	}

	/**
	 * Fired when a new site is activated with a WPMU environment.
	 *
	 * @since    1.0.0
	 *
	 * @param    int    $blog_id    ID of the new blog.
	 */
	public function activate_new_site( $blog_id ) {

		if ( 1 !== did_action( 'wpmu_new_blog' ) ) {
			return;
		}

		switch_to_blog( $blog_id );
		self::single_activate();
		restore_current_blog();

	}

	/**
	 * Get all blog ids of blogs in the current network that are:
	 * - not archived
	 * - not spam
	 * - not deleted
	 *
	 * @since    1.0.0
	 *
	 * @return   array|false    The blog ids, false if no matches.
	 */
	private static function get_blog_ids() {

		global $wpdb;

		// get an array of blog ids
		$sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";

		return $wpdb->get_col( $sql );

	}

	/**
	 * Fired for each blog when the plugin is activated.
	 *
	 * @since    1.0.0
	 */
	private static function single_activate() {
		// @TODO: Define activation functionality here
	}

	/**
	 * Fired for each blog when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	private static function single_deactivate() {
		// @TODO: Define deactivation functionality here
	}

	/**
	 * テキストドメインの呼出
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
		// load_plugin_textdomain( $domain, FALSE, basename( ( dirname( __FILE__ ) ) ) . '/languages/' );

	}

	/**
	 * CSSの登録
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', a_tools_url( 'public/assets/css/public.css' ), array(), self::VERSION );
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', a_tools_url( 'public/assets/js/public.js' ), array( 'jquery' ), self::VERSION );
	}


  /**
   * シェアボタン - ショートコード作成
   * [sharebtn service="twitter,facebook,hatena,googleplus,line"]
   */
  public static function sharebtn_shortcode_create( $atts , $content=null){
    global $post;
    // wrapper
    $output = '<div class="share-block">';
    // sharebtn setting
    $titan = TitanFramework::getInstance( 'dsblog' );
		$sharebtn_obj = $titan->getOption( 'sherebtn_disp' );
		$twitter_obj = $titan->getOption( 'twitter_acount_key' );
    $default = implode(',', $sharebtn_obj );
    // shortcode default
    extract( shortcode_atts( array(
          'service' => $default,
        ), $atts )
    );

    // foreach
    foreach ( explode(',', $service ) as $key => $s ) {
      $output .= '<span class="' . $s . '-share sharebtn">';
      switch ( $s ) {
        // facebook
        case 'facebook':
            $output .= '<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>';
          break;

        // Twitter
        case 'twitter':
            $output .= "<a href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-via=\"" . $twitter_obj . "\">ツイート</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
          break;

        // はてな
        case 'hatena':
            $output .= '<a href="' . get_permalink( $post->ID ) . '" class="hatena-bookmark-button" data-hatena-bookmark-title="' . get_the_title( $post->ID ) . '" data-hatena-bookmark-layout="standard-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>';
          break;

        // LINE
        case 'line':
            $output .= '<span><script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140127" ></script><script type="text/javascript">new media_line_me.LineButton({"pc":true,"lang":"ja","type":"a"});</script></span>';
          break;

        // Google +
        case 'google_plus':
            $output .= "<div class=\"g-plusone\" data-size=\"medium\"></div><script type=\"text/javascript\">window.___gcfg = {lang: 'ja'};(function() { var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/platform.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);})();</script>";
          break;

        default:
          break;
      }
      $output .= '</span>';
    }

    $output .= '</div>';

    return $output;

  }

}
