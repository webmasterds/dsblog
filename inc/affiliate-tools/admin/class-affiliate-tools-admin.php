<?php
/**
 * Affilicate Tools.
 *
 * @package   Affiliate_Tools_Admin
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup
 */

/*
 * 管理画面側の管理クラス
 *
 * @package Affiliate_Tools_Admin
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools_Admin_Page {

  /**
   * 基本的な設定 key
   * @var string
   */
  protected static $key_slider_widget = 'at_slider_widget_option';


  /**
   * スラッグ
   */
  public $plugin_slug = 'affiliate-tools';

  /**
   * インスタンス
   *
   * @since    1.0.0
   *
   * @var      object
   */
  protected static $instance = null;

  /**
   * Constructor
   * @since 0.1.0
   */
  public function __construct() {
    $this->title_slider_widget = __( 'スライダー設定', 'affiliate-tools' );

    add_action( 'admin_init', array( $this, 'affiliate_tools_init' ) );
    add_action( 'admin_menu', array( $this, 'add_page_general' ) , 0 , 1);
    add_action( 'admin_menu', array( $this, 'add_page_recommend' ) , 0 , 1);

    add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );
  }

  /**
    * インスタンス取得
    *
    * @since     1.0.0
    *
    * @return    object    A single instance of this class.
    */
  public static function get_instance() {

    if ( null == self::$instance ) {
      self::$instance = new self;
    }
    return self::$instance;

  }

  /**
   * init
   * @since  0.1.0
   */
  public function affiliate_tools_init() {
    register_setting( self::$key, self::$key );
    register_setting( self::$key_recommend, self::$key_recommend );
  }

  /**
   * Make public the protected $key variable.
   * @since  0.1.0
   * @return string  Option key
   */
  public static function key( $key ) {
      switch ( $key ) {
        case self::$key :
            return self::$key;
          break;
        case self::$key_slider_widget :
            return self::$key_slider_widget;
          break;
        default:
            return ;
          break;
      }
  }


  /**
   * CSSの登録
   */

  public function register_admin_styles() {
    wp_enqueue_style( $this->plugin_slug.'-admin-panel-styles', a_tools_url( 'admin/assets/css/admin.css' ) );
    // wp_enqueue_style( $this->plugin_slug.'-admin-panel-bootstrap', a_tools_url( 'admin/assets/css/bootstrap.css' ) );
  }
  /**
   * admin javascript wp_enqueue_script
   */
  public function register_admin_scripts() {
    wp_enqueue_script( $this->plugin_slug.'-admin-panel-slider', a_tools_url( 'admin/assets/js/admin.js' ), array( 'wp-color-picker' ), false, true );
    // wp_enqueue_script( $this->plugin_slug.'-admin-panel-bootstrap', a_tools_url( 'admin/assets/js/bootstrap.js' ), array( 'wp-color-picker' ), false, true );
  }

}
