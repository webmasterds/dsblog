<?php
/**
 * Plugin Name:       Affilicate Tools
 * Plugin URI:        http://grow-group.jp
 * Description:       アフィリエイトに特化した機能を提供するプラグイン
 * Version:           1.0.1
 * Author:            Ishihara Takashi
 * Author URI:        http://grow-group.jp
 * Text Domain:       affiliate-tools-locale
 * License:           MIT
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

// define
define('AFFILIATE_TOOLS_VERSION', '0.0.1');
define('AFFILIATE_TOOLS_BASE_URL', get_template_directory_uri() . '/inc/affiliate-tools/');
define('AFFILIATE_TOOLS_BASE_DIR', get_template_directory() . '/inc/affiliate-tools/');

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

/**
 * ファイルをロード
 *
 */

// メタボックス
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'includes/custom-field/affiliate-tools-post-meta.php');

// widget 表示分岐
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'includes/DisplayWidgets.php');

// 色の操作
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'includes/Color.php');

// メインクラス
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'public/class-affiliate-tools.php');

// モバイル
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'includes/MobileDetect.php');

// TinyMCE Custom
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'includes/CustomEditor.php');

// require_once ( AFFILIATE_TOOLS_BASE_DIR . 'github-updater/github-updater.php');

// widget
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-mailmagazine/class-at-mailmagazine.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-recommend-post/class-at-recommend-post.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-slider-widget/class-at-slider.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-slider-widget/class-at-slider-utility.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-video-widget/class-at-video-widget.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-ranking-widget/class-at-ranking-widget.php');
require_once ( AFFILIATE_TOOLS_BASE_DIR . 'widgets/at-image-widget/image-widget.php');

register_widget("Affiliate_Tools_Mail_Magazine_Widget");
register_widget("Affiliate_Tools_Recommend_Post_Widget");
register_widget("Affiliate_Tools_Slider_Widget");
register_widget("Affiliate_Tools_Video_Widget");
register_widget("Affiliate_Tools_Ranking_Widget");
register_widget("Tribe_Image_Widget");
// ウィジェットの登録

/**
 * プラグインロード
 *
 */
Affiliate_Tools::get_instance();
Affiliate_Tools_Slider_Widget_Utility::get_instance();


/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/**
 * 管理画面でのプラグインロード
 *
 */

// if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

require_once ( AFFILIATE_TOOLS_BASE_DIR . 'admin/class-affiliate-tools-admin.php');
add_action('admin_init', array(
    'Affiliate_Tools_Admin_Page',
    'get_instance'
));

// }


function a_tools_url( $path ){
	return get_template_directory_uri() . '/inc/affiliate-tools/' . $path ;
}

function a_tools_dir( $path ){
	return get_template_directory() . '/inc/affiliate-tools/' . $path ;
}
