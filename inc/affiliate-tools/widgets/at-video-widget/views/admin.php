<?php
/**
 * at video widget
 *  admin.php
 */
?>
<div class="<?php echo $this->get_field_id('at_video_title'); ?>">

  <p>Youtube, Vimeo , DailyMotion , blip.tv , viddler の動画を表示することができます。</p>

  <hr>

  <?php /* タイトル */ ?>
  <label for="<?php echo $this->get_field_id('at_video_title'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( '動画タイトル', $this->get_widget_slug() );?></label>
  <input class="widefat" value="<?php echo $at_video_title ; ?>" name="<?php echo $this->get_field_name('at_video_title');?>" id="<?php echo $this->get_field_id('at_video_title');?>" />

  <hr>

  <?php /* URL */ ?>
  <h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('at_video_url'); ?>"><?php echo __( '動画URL', $this->get_widget_slug() );?></label></h4>
  <p>動画のURLを入力してください。<br><small>( 例 : https://www.youtube.com/watch?v=XXXXXXXXXXXXXXXXXXXXX )</small></p>
  <input class="widefat" value="<?php echo $at_video_url ; ?>" name="<?php echo $this->get_field_name('at_video_url');?>" id="<?php echo $this->get_field_id('at_video_url');?>" />

  <hr>

  <?php /* 幅 */ ?>
  <h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('at_video_width'); ?>"><?php echo __( '幅を入力してください', $this->get_widget_slug() );?></label></h4>
  <p>幅を入力してください。(単位 : %)</p>
  <input class="widefat" value="<?php echo $at_video_width ; ?>" name="<?php echo $this->get_field_name('at_video_width');?>" id="<?php echo $this->get_field_id('at_video_width');?>" />

  <hr>

  <?php /* 高さ */ ?>
  <h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('at_video_height'); ?>"><?php echo __( '高さを入力してください', $this->get_widget_slug() );?></label></h4>
  <p>高さを入力してください。(単位 : px)</p>
  <input class="widefat" value="<?php echo $at_video_height ; ?>" name="<?php echo $this->get_field_name('at_video_height');?>" id="<?php echo $this->get_field_id('at_video_height');?>" />

  <hr>

</div>
