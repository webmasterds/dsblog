<?php
/**
 * at video widgets
 * フロント側
 */

?>

<?php if ( $instance['at_video_title']  ): ?>
	<?php echo $before_title ?>
	<?php echo esc_html( $instance['at_video_title'] ); ?>
	<?php echo $after_title ?>
<?php endif ?>

<div class="<?php echo $this->get_widget_slug(); ?> ">
<?php $args = array(
                  'width' => $instance['at_video_width'] ,
                  'height' => $instance['at_video_height'] ,

              ); ?>
  <?php echo wp_oembed_get( $instance['at_video_url'] , $args ); ?>
</div>


