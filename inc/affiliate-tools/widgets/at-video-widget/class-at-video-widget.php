<?php
/**
 * Affiliate Tools.
 *
 * @package   Affilicte_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * 動画ウィジェットの追加
 * @package Affiliate_Tools_Video_Widget
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools_Video_Widget extends WP_Widget {


	// widget slug
	 protected $widget_slug = 'at-video-widget';

	/**
	 * コンストラクタ
	 *
	 * @return add action
	 */

	public function __construct() {
		parent::__construct(
			$this->get_widget_slug(),
			__( '動画ウィジェット', $this->get_widget_slug() ),
			array(
				'class'  => $this->get_widget_slug().'-class',
				'description' => __( '動画を簡単に貼り付けるためのウィジェットです。', $this->get_widget_slug() )
			)
		);

		// ウィジェットスタイルを適用
		// add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );

		// Refreshing the widget's cached output with each new post
		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

	}

	/**
	 * Return the widget slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_widget_slug() {
			return $this->widget_slug;
	}

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * widget
	 *
	 * @param array args  The array of form elements
	 * @param array instance The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		// Check if there is a cached output
		$cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset ( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset ( $cache[ $args['widget_id'] ] ) )
			return print $cache[ $args['widget_id'] ];


		extract( $args, EXTR_SKIP );

		$widget_string = $before_widget;
		ob_start();

    // include widget.php
		include( a_tools_dir( 'widgets/at-video-widget/views/widget.php' )  );

		$widget_string .= ob_get_clean();
		$widget_string .= $after_widget;

		$cache[ $args['widget_id'] ] = $widget_string;

		wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );

		print $widget_string;

	}


	public function flush_widget_cache() {
			wp_cache_delete( $this->get_widget_slug(), 'widget' );
	}

	/**
	 * widget update.
	 *
	 * @param array new_instance The new instance of values to be generated via the update.
	 * @param array old_instance The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
    $instance['at_video_title']   = $new_instance['at_video_title']  ? $new_instance['at_video_title'] : '' ;
    $instance['at_video_width']   = $new_instance['at_video_width']  ? $new_instance['at_video_width'] : '100%' ;
    $instance['at_video_height']  = $new_instance['at_video_height']  ? $new_instance['at_video_height'] : '300px' ;
    $instance['at_video_url']     = $new_instance['at_video_url']  ? $new_instance['at_video_url'] : '' ;
		return $instance;
	}

	/**
	 * admin page generate
	 *
	 * @param array instance The array of keys and values for the widget.
	 */

	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance
		);
		$at_video_title   = isset( $instance['at_video_title'] ) ? $instance['at_video_title']   : '' ;
    $at_video_width   = isset( $instance['at_video_width'] ) ? $instance['at_video_width']   : '' ;
    $at_video_height  = isset( $instance['at_video_height'] ) ? $instance['at_video_height'] : '' ;
    $at_video_url     = isset( $instance['at_video_url'] ) ? $instance['at_video_url']       : '' ;
		include ( a_tools_dir( 'widgets/at-video-widget/views/admin.php' ) );
	}

	/**
	 * public css wp_enqueue_style
	 */
	public function register_widget_styles() {
		wp_enqueue_style( $this->get_widget_slug().'-widget-styles', a_tools_url( 'widgets/at-video-widget/css/public.css' ) );
	}

}

