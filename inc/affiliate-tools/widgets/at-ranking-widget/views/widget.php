<?php

/**
 * at video widgets
 * フロント側
 */
?>

<?php
if ($instance['at_ranking_title']): ?>
  <?php
  echo $before_title ?>
  <?php
  echo esc_html($instance['at_ranking_title']); ?>
  <?php
  echo $after_title ?>
<?php
endif ?>

<div class="<?php echo $this->get_widget_slug(); ?>">

<?php
for ($i = 1; $i < 11; $i++) { ?>

  <?php
  $rank_id = $instance['at_ranking_' . $i];
  if ('0' !== $rank_id): ?>

    <div class="ds-ranking-widget-box ranking<?php echo $i; ?> clearfix">
      <div class="clearfix">
        <div class="thumbnails">
          <a href="<?php echo get_permalink( $rank_id )?>">
            <img class="ranking-number" src="<?php echo get_template_directory_uri(); ?>/assets/img/shortcode/ds_ranking_box_ranking_<?php echo $i; ?>.png" alt="ランキング<?php echo $i; ?>">
            <?php
            if ( has_post_thumbnail( $rank_id )):
              echo get_the_post_thumbnail( $rank_id, 'thumbnail', array('class'=>'ranking-thumbnail'));
            else :
              echo '<img src="'. AFFILIATE_TOOLS_BASE_URL. 'assets/noimage.jpg" alt="画像がありません" class="ranking-thumbnail">';
            endif; ?>
          </a>
        </div>
        <div class="widget-ranking-content">
          <h3 class="ranking-title">
            <a href="<?php echo get_permalink( $rank_id )?>">
              <?php echo get_the_title($rank_id); ?>
            </a>
          </h3>
          <?php if ( $instance['at_ranking_catch_' . $i] ) { ?>
          <div class="ranking-catch-copy">
            <span class="text-underline"><?php echo $instance['at_ranking_catch_' . $i]; ?></span>
          </div>
          <?php } ?>
          <div class="ranking-post-content">
            <?php
            if ($instance['at_ranking_post_content_' . $i] !== 'true') {
              echo $instance['at_ranking_text_' . $i];
            } else {
              $post_object = get_post($rank_id);
              echo do_shortcode(mb_substr(strip_tags( $post_object->post_content ) , 0, 100));
            }
            ?>
            </div>
          </div>
        <?php if ( $instance['at_ranking_btn']  ) : ?>
        <a href="<?php echo get_permalink( $rank_id )?>" class="btn btn-sm btn-default more-link btn-dimensional ranking-more">
          続きを読む
        </a>
        <?php endif; ?>
      </div>
    </div>
  <?php
  endif ?>
<?php
} ?>
</div>
