<?php
/**
 * at video widget
 *  admin.php
 */
?>
<script>
var $ = jQuery;
$(function(){
  $(".<?php echo $this->get_field_id('at_ranking_title'); ?> .atMenu dt").on("click", function() {
    $(this).next().slideToggle('fast');
  });
});
</script>
<div class="<?php echo $this->get_field_id('at_ranking_title'); ?>">

  <p>記事を選択し、ランキングとして表示することができます。</p>

  <hr>

  <?php /* タイトル */ ?>
  <label for="<?php echo $this->get_field_id('at_ranking_title'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( 'ランキングタイトル', $this->get_widget_slug() );?></label>
  <input class="widefat" value="<?php echo $at_ranking_title ; ?>" name="<?php echo $this->get_field_name('at_ranking_title');?>" id="<?php echo $this->get_field_id('at_ranking_title');?>" />

  <hr>

  <?php /* ランキング */ ?>
  <?php for ($i = 1; $i < 11 ; $i++) { ?>
  <dl class="atMenu ">
    <dt><h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('at_ranking_'. $i); ?>"><?php echo __( 'ランキング' . $i . '位', $this->get_widget_slug() );?></label></h4></dt>
    <dd><p>ランキング<?php echo $i;  ?>位の記事を選択してください。<br></p>
    <select name="<?php echo $this->get_field_name( 'at_ranking_' . $i );?>" id="<?php echo $this->get_field_id( 'at_ranking_'. $i );?>">
          <option value="0" <?php selected( '0' , $ranking[$i] , true ) ?>>-- 選択してください --</option>
    <?php $posts = get_posts( array(
              'post_type'      =>  'post',
              'posts_per_page' => -1
            )
          );
          foreach ( $posts as $key => $value ) { ?>
            <option value="<?php echo $value->ID ;?>" <?php selected( $value->ID , $ranking[$i] , true ) ?>><?php echo mb_substr( $value->post_title , 0 , 40 ); ?></option>
         <?php } ?>
    </select>
    <hr>
    <label for="<?php echo $this->get_field_id('at_ranking_catch_' . $i  ); ?>"><div class="dashicons dashicons-admin-tools"></div> <?php echo __( 'キャッチコピー', $this->get_widget_slug() );?></label><br>
    <input type="text" value="<?php echo $at_ranking_catch[$i] ; ?>" class="widefat" name="<?php echo $this->get_field_name('at_ranking_catch_' . $i );?>" id="<?php echo $this->get_field_id('at_ranking_catch_' . $i);?>" style="margin-bottom: 10px" />
    <?php /* 説明文 */ ?>
    <label for="<?php echo $this->get_field_id('at_ranking_' . $i . 'text'); ?>"><div class="dashicons dashicons-admin-tools"></div> <?php echo __( '説明文', $this->get_widget_slug() );?></label>
    <label for="<?php echo $this->get_field_id( 'at_ranking_post_content_' . $i );?>" style="float: right;vertical-aligin:middle">
      <input type="checkbox" style="margin-top: -3px;" class="widefat" value="true" name="<?php echo $this->get_field_name('at_ranking_post_content_' . $i );?>" id="<?php echo $this->get_field_id('at_ranking_post_content_' . $i );?>" <?php checked( 'true', $at_ranking_post_content[$i], true ) ?>>
       投稿内容から引用
    </label>
    <textarea class="widefat" name="<?php echo $this->get_field_name('at_ranking_text_' . $i );?>" id="<?php echo $this->get_field_id('at_ranking_text' . $i);?>" cols="30" rows="10"><?php echo $at_ranking_text[$i] ; ?></textarea>
    </dd>
  </dl>
  <?php } ?>
  <hr>
  <label for="<?php echo $this->get_field_id('at_ranking_btn'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( 'ボタンの表示', $this->get_widget_slug() );?></label>
  <input type="checkbox" class="widefat" value="true" name="<?php echo $this->get_field_name('at_ranking_btn');?>" id="<?php echo $this->get_field_id('at_ranking_btn');?>" <?php checked( 'true' , $at_ranking_btn, $current = true, $echo = true ) ?> />

  <hr>

</div>
