<?php

/**
 * Affiliate Tools.
 *
 * @package   Affilicte_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * ランキングウィジェットの追加
 * @package Affiliate_Tools_Ranking_Widget
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools_Ranking_Widget extends WP_Widget
{

  // widget slug
  protected $widget_slug = 'at-ranking-widget';

  /**
   * コンストラクタ
   *
   * @return add action
   */

  public function __construct() {
    parent::__construct($this->get_widget_slug() , __('ランキングウィジェット', $this->get_widget_slug()) , array(
      'class' => $this->get_widget_slug() . '-class',
      'description' => __('ランキングを作成するためのウィジェットです。', $this->get_widget_slug())
    ));

    // ウィジェットスタイルを適用
    add_action('admin_enqueue_scripts', array(
      $this,
      'register_admin_styles'
    ));

    add_action('admin_enqueue_scripts', array(
      $this,
      'register_admin_scripts'
    ));

    // Register site styles and scripts
    // add_action('wp_enqueue_scripts', array(
    //   $this,
    //   'register_widget_styles'
    // ));

    // add_action('wp_enqueue_scripts', array(
    //   $this,
    //   'register_widget_scripts'
    // ));

    // Refreshing the widget's cached output with each new post
    add_action('save_post', array(
      $this,
      'flush_widget_cache'
    ));
    add_action('deleted_post', array(
      $this,
      'flush_widget_cache'
    ));
    add_action('switch_theme', array(
      $this,
      'flush_widget_cache'
    ));
  }

  /**
   * Return the widget slug.
   *
   * @since    1.0.0
   *
   * @return    Plugin slug variable.
   */
  public function get_widget_slug() {
    return $this->widget_slug;
  }

  /*--------------------------------------------------*/

  /* Widget API Functions
  /*--------------------------------------------------*/

  /**
   * widget
   *
   * @param array args  The array of form elements
   * @param array instance The current instance of the widget
   */
  public function widget($args, $instance) {

    // Check if there is a cached output
    $cache = wp_cache_get($this->get_widget_slug() , 'widget');

    if (!is_array($cache)) $cache = array();

    if (!isset($args['widget_id'])) $args['widget_id']       = $this->id;

    if (isset($cache[$args['widget_id']])) return print $cache[$args['widget_id']];

    extract($args, EXTR_SKIP);

    $widget_string = $before_widget;
    ob_start();

    // include widget.php
		include( a_tools_dir( 'widgets/at-ranking-widget/views/widget.php' )  );

    $widget_string.= ob_get_clean();
    $widget_string.= $after_widget;

    $cache[$args['widget_id']] = $widget_string;

    wp_cache_set($this->get_widget_slug() , $cache, 'widget');

    print $widget_string;
  }

  public function flush_widget_cache() {
    wp_cache_delete($this->get_widget_slug() , 'widget');
  }

  /**
   * widget update.
   *
   * @param array new_instance The new instance of values to be generated via the update.
   * @param array old_instance The previous instance of values before the update.
   */
  public function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['at_ranking_title']          = $new_instance['at_ranking_title'] ? $new_instance['at_ranking_title'] : '';
    for ($i = 0; $i < 11; $i++) {
      $instance['at_ranking_' . $i]              = $new_instance['at_ranking_' . $i] ? $new_instance['at_ranking_' . $i] : '0';
      $instance['at_ranking_catch_' . $i]         = $new_instance['at_ranking_catch_' . $i] ? $new_instance['at_ranking_catch_' . $i] : '';
      $instance['at_ranking_text_' . $i]         = $new_instance['at_ranking_text_' . $i] ? $new_instance['at_ranking_text_' . $i] : '';
      $instance['at_ranking_post_content_' . $i] = $new_instance['at_ranking_post_content_' . $i] ? $new_instance['at_ranking_post_content_' . $i] : '';
    }
    $instance['at_ranking_btn'] = $new_instance['at_ranking_btn'] ? $new_instance['at_ranking_btn'] : '';
    return $instance;
  }

  /**
   * admin page generate
   *
   * @param array instance The array of keys and values for the widget.
   */

  public function form($instance) {
    $instance         = wp_parse_args((array)$instance);
    $at_ranking_title = isset($instance['at_ranking_title']) ? $instance['at_ranking_title'] : '';
    for ($i = 0; $i < 11; $i++) {
      $ranking[$i]         = isset($instance['at_ranking_' . $i]) ? $instance['at_ranking_' . $i] : '0';
      $at_ranking_catch[$i] = isset($instance['at_ranking_catch_' . $i]) ? $instance['at_ranking_catch_' . $i] : '';
      $at_ranking_text[$i] = isset($instance['at_ranking_text_' . $i]) ? $instance['at_ranking_text_' . $i] : '';
      $at_ranking_post_content[$i] = isset( $instance['at_ranking_post_content_' . $i] ) ? $instance['at_ranking_post_content_' . $i] : '';
    }
    $at_ranking_btn    = isset($instance['at_ranking_btn']) ? $instance['at_ranking_btn'] : '';
		include( a_tools_dir( 'widgets/at-ranking-widget/views/admin.php' )  );
  }

  /**
   * public css wp_enqueue_style
   */
  public function register_widget_styles() {
    wp_enqueue_style($this->get_widget_slug() . '-widget-styles', a_tools_url( 'widgets/at-ranking-widget/css/public.css'));
  }

  /**
   * public javascript wp_enqueue_script
   */
  public function register_widget_scripts() {
    wp_enqueue_script( $this->get_widget_slug() . '-script', a_tools_url( 'widgets/at-ranking-widget/js/widget.js') , array(
      'jquery'
    ));
  }

  /**
   * admin enqueue_style
   */
  public function register_admin_styles() {
    wp_enqueue_style($this->get_widget_slug() . '-admin-styles', a_tools_url( 'widgets/at-ranking-widget/css/admin.css'));
  }

  /**
   * admin javascript wp_enqueue_script
   */
  public function register_admin_scripts() {
    wp_enqueue_script( $this->get_widget_slug() . '-admin-script', a_tools_url( 'widgets/at-ranking-widget/js/admin.js' ), array('jquery') );
  }

}

