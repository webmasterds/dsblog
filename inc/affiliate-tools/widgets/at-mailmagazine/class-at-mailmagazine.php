<?php

/**
 * Affiliate Tools.
 *
 * @package   Affiliate_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * ウィジェットの追加
 * @package Affiliate_Tools
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools_Mail_Magazine_Widget extends WP_Widget
{

  // widget slug
  protected $widget_slug = 'at-mailmagazine';

  /**
   * コンストラクタ
   *
   * @return add action
   */

  public function __construct() {
    parent::__construct($this->get_widget_slug() , __('メールマガジン登録フォーム', $this->get_widget_slug()) , array(
      'class' => $this->get_widget_slug() . '-class',
      'description' => __('メールマガジン登録フォームを表示するウィジェット', $this->get_widget_slug())
    ));

    // Register admin styles and scripts
    add_action('admin_print_styles', array(
      $this,
      'register_admin_styles'
    ));
    add_action('admin_enqueue_scripts', array(
      $this,
      'register_admin_scripts'
    ));

    // Register site styles and scripts
    // add_action('wp_enqueue_scripts', array(
    //   $this,
    //   'register_widget_styles'
    // ));
    // add_action('wp_enqueue_scripts', array(
    //   $this,
    //   'register_widget_scripts'
    // ));

    // Refreshing the widget's cached output with each new post
    add_action('save_post', array(
      $this,
      'flush_widget_cache'
    ));
    add_action('deleted_post', array(
      $this,
      'flush_widget_cache'
    ));
    add_action('switch_theme', array(
      $this,
      'flush_widget_cache'
    ));
  }

  /**
   * Return the widget slug.
   *
   * @since    1.0.0
   *
   * @return    Plugin slug variable.
   */
  public function get_widget_slug() {
    return $this->widget_slug;
  }

  /*--------------------------------------------------*/

  /* Widget API Functions
  /*--------------------------------------------------*/

  /**
   * widget
   *
   * @param array args  The array of form elements
   * @param array instance The current instance of the widget
   */
  public function widget($args, $instance) {

    // Check if there is a cached output
    $cache = wp_cache_get($this->get_widget_slug() , 'widget');

    if (!is_array($cache)) $cache = array();

    if (!isset($args['widget_id'])) $args['widget_id']       = $this->id;

    if (isset($cache[$args['widget_id']])) return print $cache[$args['widget_id']];

    extract($args, EXTR_SKIP);

    $widget_string = $before_widget;

    ob_start();
		include( a_tools_dir( 'widgets/at-mailmagazine/views/widget.php' )  );
    $widget_string.= ob_get_clean();
    $widget_string.= $after_widget;

    $cache[$args['widget_id']] = $widget_string;

    wp_cache_set($this->get_widget_slug() , $cache, 'widget');

    print $widget_string;
  }

  public function flush_widget_cache() {
    wp_cache_delete($this->get_widget_slug() , 'widget');
  }

  /**
   * widget update.
   *
   * @param array new_instance The new instance of values to be generated via the update.
   * @param array old_instance The previous instance of values before the update.
   */
  public function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['at_mailmagazine_optin_title']          = $new_instance['at_mailmagazine_optin_title'] ? $new_instance['at_mailmagazine_optin_title'] : '';
    $instance['at_mailmagazine_optin_type']          = $new_instance['at_mailmagazine_optin_type'] ? $new_instance['at_mailmagazine_optin_type'] : '0';
    $instance['at_mailmagazine_optin_code']          = $new_instance['at_mailmagazine_optin_code'] ? $new_instance['at_mailmagazine_optin_code'] : '';
    $instance['at_mailmagazine_optin_color']          = $new_instance['at_mailmagazine_optin_color'] ? $new_instance['at_mailmagazine_optin_color'] : '#e8e8e8';
    $instance['at_mailmagazine_optin_btn_color']          = $new_instance['at_mailmagazine_optin_btn_color'] ? $new_instance['at_mailmagazine_optin_btn_color'] : '#e8e8e8';
    $instance['at_mailmagazine_optin_btn_text']          = $new_instance['at_mailmagazine_optin_btn_text'] ? $new_instance['at_mailmagazine_optin_btn_text'] : '';
    $instance['at_mailmagazine_optin_layout']          = $new_instance['at_mailmagazine_optin_layout'] ? $new_instance['at_mailmagazine_optin_layout'] : 'small';
    return $instance;
  }

  /**
   * admin page generate
   *
   * @param array instance The array of keys and values for the widget.
   */

  public function form($instance) {
    $instance        = wp_parse_args((array)$instance);
    $optin_title     = isset($instance['at_mailmagazine_optin_title']) ? $instance['at_mailmagazine_optin_title'] : '';
    $optin_type      = isset($instance['at_mailmagazine_optin_type']) ? $instance['at_mailmagazine_optin_type'] : '';
    $optin_code      = isset($instance['at_mailmagazine_optin_code']) ? $instance['at_mailmagazine_optin_code'] : '';
    $optin_color     = isset($instance['at_mailmagazine_optin_color']) ? $instance['at_mailmagazine_optin_color'] : '';
    $optin_btn_color = isset($instance['at_mailmagazine_optin_btn_color']) ? $instance['at_mailmagazine_optin_btn_color'] : '';
    $optin_btn_text  = isset($instance['at_mailmagazine_optin_btn_text']) ? $instance['at_mailmagazine_optin_btn_text'] : '';
    $optin_layout    = isset($instance['at_mailmagazine_optin_layout']) ? $instance['at_mailmagazine_optin_layout'] : '';
		include( a_tools_dir( 'widgets/at-mailmagazine/views/admin.php' )  );
  }

  /**
   * admin enqueue_style
   */
  public function register_admin_styles() {
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_style($this->get_widget_slug() . '-admin-styles', a_tools_url('widgets/at-mailmagazine/css/admin.css', __FILE__));
  }

  /**
   * admin javascript wp_enqueue_script
   */
  public function register_admin_scripts() {
    wp_enqueue_script($this->get_widget_slug() . '-admin-color-picker', a_tools_url('widgets/at-mailmagazine/js/admin.js', __FILE__) , array(
      'wp-color-picker'
    ) , false, true);

    // wp_enqueue_script( $this->get_widget_slug().'-admin-script', a_tools_url( 'js/admin.js', __FILE__ ), array('jquery') );


  }

  /**
   * public css wp_enqueue_style
   */
  public function register_widget_styles() {

    wp_enqueue_style($this->get_widget_slug() . '-widget-styles', a_tools_url('widgets/at-mailmagazine/css/public.css', __FILE__));
  }

  /**
   * public javascript wp_enqueue_script
   */
  public function register_widget_scripts() {

    wp_enqueue_script($this->get_widget_slug() . '-script', a_tools_url('widgets/at-mailmagazine/js/widget.js', __FILE__) , array(
      'jquery'
    ));
  }

  /**
   * at_mailmagazine_parse_optin_form
   * @param
   */

  public function at_mailmagazine_parse_optin_form($optin_form_code) {
    global $plc;
    $pattern = '/<form\s[^>]*action[\s]*=[\s]*[\'|"](.*?)[\'|"][^>]*>/i';
    preg_match($pattern, $optin_form_code, $form_tag_matches);
    $form_action = isset($form_tag_matches[1]) ? $form_tag_matches[1] : '';
    $all_inputs  = $this->return_between_multiple($optin_form_code, '<input', '>', true);
    if ($all_inputs) {
      foreach ($all_inputs as $input) {
        if (strpos($input, 'hidden') !== false) {
          $input       = $input . '>';
          $name        = $this->return_between($input, 'name', ' ', false);
          if ($name == 'n') {
            $name        = return_between($input, 'name', '>', false);
          }
          $name        = trim($name);
          $name        = explode('=', $name, 2);
          $name        = str_replace('\'', '', $name[1]);
          $name        = str_replace('"', '', $name);
          $name        = str_replace('>', '', $name);
          $val         = $this->return_between($input, 'value', ' ', false);
          if ($val == 'v') {
            $val         = $this->return_between($input, 'value', '>', false);
          }
          $val         = trim($val);
          $val         = explode('=', $val, 2);
          $val         = str_replace('\'', '', $val[1]);
          $val         = str_replace('"', '', $val);
          $val         = str_replace('>', '', $val);
          $arr_name[]             = $name;
          $arr_value[]             = $val;
        }
      }
      if (is_array($arr_name) && is_array($arr_value)) {
        $post_params = array_combine($arr_name, $arr_value);
      }
    }

    $post_params['action']             = $form_action;
    return $post_params;
  }

  /**
   * at_mailmagazine_name_email_fields
   */

  public function at_mailmagazine_name_email_fields($optin_list_provider) {
    $providerNameFields  = array(
      'name',
      'c_name',
      'rdlastname',
      'sName',
      'touroku_name',
      'name1',
      'name',
      'name',
      'Name',
      'name',
      'full_name',
      'web_name',
      'Name',
      'category2',
      'fields_fname',
      'inf_field_FirstName',
      'name',
      'NAME',
      'fname',
      'name',
      's_first_name',
      'name',
      'FullName',
      'Contact0FirstName',
      'FirstName'
    );
    $providerEmailFields = array(
      'mail',
      'c_mailaddress',
      'rdemail',
      'sEmail',
      'touroku_mail',
      'email',
      'email',
      'address',
      'Email1',
      'email',
      'email',
      'web_email',
      'Email',
      'category3',
      'fields_email',
      'inf_field_Email',
      'from',
      'EMAIL',
      'email',
      'from',
      's_email',
      'from',
      'Email',
      'Contact0Email'
    );
    $fields['name']                     = $providerNameFields[$optin_list_provider];
    $fields['email']                     = $providerEmailFields[$optin_list_provider];
    return $fields;
  }

  /**
   * return_between
   */

  public function return_between($string, $start, $stop, $type) {
    if (strpos($string, $stop) === false) {
      $stop_err = str_replace('<', '&lt;', $stop);
      $stop_err = str_replace('>', '&gt;', $stop_err);
      if (is_admin()) {
        die('<span style="color:red;">エラー！divで問題が発生しました！プロセスを停止します"' . $stop_err . '" 投稿内容が見つかりません。</span>');
      } else {
        die('投稿プロセスエラー');
      }
    }
    $temp = $this->split_string($string, $start, 'AFTER', $type);
    if (empty($temp)) {
      return $temp;
    } else {
      return $this->split_string($temp, $stop, 'BEFORE', $type);
    }
  }

  public function return_between_multiple($string, $start, $stop, $type) {
    $return = array();
    while (stristr($string, $start)) {
      $temp   = $this->split_string($string, $start, 'AFTER', $type);
      $return[]        = $this->split_string($temp, $stop, 'BEFORE', $type);
      $string = $temp;
    }
    return $return;
  }

  /**
   *
   */
  public function split_string($string, $delineator, $desired, $type) {
    // Case insensitive parse, convert string and delineator to lower case
    $lc_str        = strtolower($string);
    $marker        = strtolower($delineator);

    if ($desired == 'BEFORE') {
      if ($type == 'EXCL')
       // Return text ESCL of the delineator
      $split_here    = strpos($lc_str, $marker);
      else

      // Return text INCL of the delineator
      $split_here    = strpos($lc_str, $marker) + strlen($marker);
      $parsed_string = substr($string, 0, $split_here);
    } else {
      if ($type == 'EXCL') {

        // Return text ESCL of the delineator
        $split_here    = strpos($lc_str, $marker) + strlen($marker);
      } else {

        // Return text INCL of the delineator
        $split_here    = strpos($lc_str, $marker);
      }
      $parsed_string = substr($string, $split_here, strlen($string));
    }
    return $parsed_string;
  }

  /**
   * button generator
   *
   * @param $dom : html selecter
   * @param $themecolor : themecolor,primary,secondary
   * @param $effect : gradient effent
   * @return css code
   */
  function get_button_generator($dom        = 'a.btn', $themecolor = '#e8e8e8', $effect) {
    $css        = '';
    $background = '';
    $color_obj  = new Color($themecolor);
    $background.= $color_obj->getCssGradientCustom($effect, TRUE);
    $css.= $dom . '{
              background-color: ' . $themecolor . ';
              position: relative;
              color: ' . ($color_obj->isDark() ? '#FFF' : '#252525') . ';
              ' . $background . '
              -webkit-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', 0px 6px 0px #' . $color_obj->darken($effect + 20) . ';
              -moz-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', 0px 6px 0px #' . $color_obj->darken($effect + 20) . ';
              -o-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', 0px 6px 0px #' . $color_obj->darken($effect + 20) . ';
              box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', 0px 6px 0px #' . $color_obj->darken($effect + 20) . ';
              -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
              -o-border-radius: 5px;
              border-radius: 5px;
              border: none;
            }
            ' . $dom . '::before {
              background-color: #' . $color_obj->darken($effect) . ';
              content:"";
              display: block;
              position: absolute;
              width: 100%;
              height: 100%;
              padding-left: 2px;
              padding-right: 2px;
              padding-bottom: 4px;
              left: -2px;
              top: 5px;
              z-index: -1;
              -webkit-border-radius: 6px;
              -moz-border-radius: 6px;
              -o-border-radius: 6px;
              border-radius: 6px;
              -webkit-box-shadow: 0px 1px 0px #fff;
              -moz-box-shadow: 0px 1px 0px #fff;
              -o-box-shadow: 0px 1px 0px #fff;
              box-shadow: 0px 1px 0px #fff;
            }
            ' . $dom . ':active {
              color: ' . ($color_obj->isDark() ? '#252525' : '#FFF') . ' ;
              text-shadow: 0px 1px 1px rgba( 255, 255, 255 , 0.3);
              background: #' . $color_obj->lighten($effect + 10) . ';
              -webkit-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', inset 0px -1px 0px #' . $color_obj->darken($effect + 20) . ';
              -moz-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', inset 0px -1px 0px #' . $color_obj->darken($effect + 20) . ';
              -o-box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', inset 0px -1px 0px #' . $color_obj->darken($effect + 20) . ';
              box-shadow: inset 0px 1px 0px #' . $color_obj->darken($effect) . ', inset 0px -1px 0px #' . $color_obj->darken($effect + 20) . ';
              top:7px;
            }
            ' . $dom . ':active::before {
              top:-2px;
            }';
    return $css;
  }
}

