<?php
/**
 *
 */




?>

<style>
	<?php echo $this->get_button_generator( '.btn-mailmagazine-dimensional',$instance['at_mailmagazine_optin_btn_color'], '10' ) ?>
	.optin.at-mailmagazine{
		background-color:<?php echo $instance['at_mailmagazine_optin_color'] ;?>;
	}
</style>

<?php if (  $instance['at_mailmagazine_optin_title']  ): ?>
	<?php echo $before_title ?>
	<?php echo esc_html( $instance['at_mailmagazine_optin_title'] ); ?>
	<?php echo $after_title ?>
<?php endif ?>

<div class="optin <?php echo $this->get_widget_slug(); ?> layout-<?php echo esc_attr( $instance['at_mailmagazine_optin_layout'] ) ;?>">
  <?php
    $optin_form_parsed = $this->at_mailmagazine_parse_optin_form( $instance['at_mailmagazine_optin_code']  );
    $optin_form_action = $optin_form_parsed['action'];
    unset( $optin_form_parsed['action']);
    $optin_form_hidden_fields = $optin_form_parsed;
    $optin_form_name_emails_fields = $this->at_mailmagazine_name_email_fields( $instance['at_mailmagazine_optin_type']  );
    $optin_form_name = $optin_form_name_emails_fields['name'];
    $optin_form_email = $optin_form_name_emails_fields['email'];
  ?>
  <form action="<?php echo esc_url( $optin_form_action ); ?> " method="post">
    <?php
    foreach($optin_form_hidden_fields as $key => $value) {
      echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
    }
    ?>
    <input class="text form-text" type="text" name="<?php echo esc_attr( $optin_form_name ); ?>" id="name" placeholder="お名前を入力してください。">
    <input class="text form-text" type="text" name="<?php echo esc_attr( $optin_form_email ); ?>" id="email" placeholder="メールアドレスを入力して下さい" />
    <p class="text-center" style="margin-bottom: 18px">
	    <input class="btn btn-default btn-mailmagazine-dimensional" type="submit" value="<?php echo esc_attr( $instance['at_mailmagazine_optin_btn_text'] );?>">
    </p>
  </form>
</div>


