<?php
//admin.php
?>



<div class="widget-at-mailmagazine">
	<h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_title'); ?>"><?php echo __( 'タイトル', $this->get_widget_slug() );?></label></h4>
	<input class="widefat" value="<?php echo $optin_title ; ?>" name="<?php echo $this->get_field_name('at_mailmagazine_optin_title');?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_title');?>" />

  <?php /* フォームタイプ */ ?>
	<h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_type'); ?>">フォームタイプ:</label></h4>
	<p><small>メールサービス名を選択してください。</small></p>
	<select name="<?php echo $this->get_field_name('at_mailmagazine_optin_type');?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_type');?>">
	    <option value="0" <?php selected( '0' , $optin_type , true ) ?>>楽メールPro</option>
	    <option value="1" <?php selected( '1' , $optin_type , true ) ?>>ネット商人Pro</option>
	    <option value="2" <?php selected( '2' , $optin_type , true ) ?>>メール商人</option>
	    <option value="3" <?php selected( '3' , $optin_type , true ) ?>>Jcity</option>
	    <option value="4" <?php selected( '4' , $optin_type , true ) ?>>アスメル</option>
	    <option value="5" <?php selected( '5' , $optin_type , true ) ?>>オートビズ</option>
	    <option value="6" <?php selected( '6' , $optin_type , true ) ?>>その他1(name属性がemail,nameになってるフォーム)</option>
	    <option value="7" <?php selected( '7' , $optin_type , true ) ?>>その他2(name属性がaddress,nameになってるフォーム)</option>
	    <option value="8" <?php selected( '8' , $optin_type , true ) ?>>1ShoppingCart</option>
	    <option value="9" <?php selected( '9' , $optin_type , true ) ?>>aWeber</option>
	    <option value="10" <?php selected( '10' , $optin_type , true ) ?>>AutoResponsePlus</option>
	    <option value="11" <?php selected( '11' , $optin_type , true ) ?>>Email Aces</option>
	    <option value="12" <?php selected( '12' , $optin_type , true ) ?>>FreeAutobot</option>
	    <option value="13" <?php selected( '13' , $optin_type , true ) ?>>GetResponse</option>
	    <option value="14" <?php selected( '14' , $optin_type , true ) ?>>iContact</option>
	    <option value="15" <?php selected( '15' , $optin_type , true ) ?>>InfusionSoft</option>
	    <option value="23" <?php selected( '23' , $optin_type , true ) ?>>InfusionSoft (Old)</option>
	    <option value="16" <?php selected( '16' , $optin_type , true ) ?>>ListPing</option>
	    <option value="17" <?php selected( '17' , $optin_type , true ) ?>>MailChimp</option>
	    <option value="18" <?php selected( '18' , $optin_type , true ) ?>>ParaBots</option>
	    <option value="19" <?php selected( '19' , $optin_type , true ) ?>>ProSender</option>
	    <option value="20" <?php selected( '20' , $optin_type , true ) ?>>QuickPayPro</option>
	    <option value="21" <?php selected( '21' , $optin_type , true ) ?>>Turbo Autoresponders</option>
	    <option value="22" <?php selected( '22' , $optin_type , true ) ?>>GVO</option>
	</select>

  <?php /* HTMLフォーム */ ?>

	<h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_type'); ?>"><?php echo __( 'HTMLフォーム', $this->get_widget_slug() );?></label></h4>
	<p>各メールサービスで配布しているHTMLフォームを入力してください。</p>
	<textarea class="widefat" name="<?php echo $this->get_field_name('at_mailmagazine_optin_code');?>" id="<?php echo $this->get_field_name('at_mailmagazine_optin_code');?>" cols="40" rows="10"><?php echo $optin_code ; ?></textarea>

	<?php /* カラーテーマ */ ?>
	<h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_color'); ?>"><?php echo __( 'カラー', $this->get_widget_slug() );?></label></h4>
	<p><?php echo __( 'カラーテーマを選択してください。', $this->get_widget_slug() ); ?></p>
	<input type="text" name="<?php echo $this->get_field_name('at_mailmagazine_optin_color'); ?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_color'); ?>" value="<?php echo $optin_color;?>" class="wp-color-picker-mailmagazine" data-default-color="#effeff" />

  <?php /* ボタンカラー */ ?>
	<p><?php echo __( 'ボタンカラーを選択してください。', $this->get_widget_slug() ); ?></p>
	<input type="text" name="<?php echo $this->get_field_name('at_mailmagazine_optin_btn_color'); ?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_btn_color'); ?>" value="<?php echo $optin_btn_color;?>" class="wp-color-picker-mailmagazine" data-default-color="#effeff" />

  <?php /* ボタンテキスト */ ?>
	<h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_btn_text'); ?>"><?php echo __( 'ボタンテキスト', $this->get_widget_slug() );?></label></h4>
	<input type="text" class="widefat" value="<?php echo $optin_btn_text ; ?>" name="<?php echo $this->get_field_name('at_mailmagazine_optin_btn_text');?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_btn_text');?>" />
	<br>

  <?php /* レイアウト */ ?>
  <h4><label for="<?php echo $this->get_field_id('at_mailmagazine_optin_layout'); ?>"><?php echo __( '大きさを選択してください', $this->get_widget_slug() );?></label></h4>
  <select name="<?php echo $this->get_field_name('at_mailmagazine_optin_layout');?>" id="<?php echo $this->get_field_id('at_mailmagazine_optin_layout');?>">
    <option value="small" <?php selected( 'small' , $optin_layout , true ) ?>>小</option>
    <option value="normal" <?php selected( 'normal' , $optin_layout , true ) ?>>中</option>
    <option value="large" <?php selected( 'large' , $optin_layout , true ) ?>>大</option>
  </select>
</div>
