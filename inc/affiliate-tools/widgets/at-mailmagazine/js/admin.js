// admin.js
var $ = jQuery;
jQuery(document).ready(function($){
  function updateColorPickers(){
      $('.wp-color-picker-mailmagazine').each(function(){
          $(this).wpColorPicker({
              defaultColor: true,
              change: function(event, ui){},
              clear: function() {},
              hide: true,
              palettes: true
          });
      });
  };
  updateColorPickers();
  $(document).ajaxSuccess(function(e, xhr, settings) {
      if(settings.data.search('action=save-widget') != -1 ) {
          $('.color-field .wp-picker-container').remove();
          updateColorPickers();
      }
  });
});

