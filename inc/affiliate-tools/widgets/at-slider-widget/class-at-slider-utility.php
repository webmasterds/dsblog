<?php
/**
 * Affiliate Tools.
 *
 * @package   Affilicte_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * スライダーユーティリティー
 *
 * @package Affiliate_Tools_Slider_Widget
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */

class Affiliate_Tools_Slider_Widget_Utility
{

  /**
   * インスタンス
   *
   * @since    1.0.0
   *
   * @var      object
   */
  protected static $instance    = null;

  protected $plugin_slug = 'affiliate-tools';

  private function __construct()
  {
    add_action( 'init' , array( $this, 'register_slider_post_type' ));
    add_action( 'save_post' , array( $this , 'slider_save_auto_title_rename' ));
    add_filter( 'cmb_meta_boxes' , array( $this, 'slider_widget_option_fields' ));
    // Register admin styles and scripts
    add_action('admin_print_styles', array( $this , 'register_admin_styles' ));
    add_action('admin_enqueue_scripts', array( $this , 'register_admin_scripts' ));

    // Register site styles and scripts
    add_action('wp_enqueue_scripts', array( $this , 'register_widget_styles' ));
    add_action('wp_enqueue_scripts', array( $this , 'register_widget_scripts' ));

    // Register admin panel
    add_filter('post_updated_messages', array( $this , 'slider_custom_postupdate_message' ) );
    add_filter('enter_title_here', array( $this, 'change_slider_title_placeholder' ));
    add_filter('get_sample_permalink_html', array( $this , 'change_post_pearmalink' ));
    add_filter('edit_form_top', array( $this , 'add_slider_post_top'));
  }

  /**
   * create instance
   *
   * @since     1.0.0
   * @return    object    A single instance of this class.
   */
  public static function get_instance()
  {

    if (null == self::$instance)
    {
      self::$instance = new self;
    }

    return self::$instance;
  }

  /**
   * admin enqueue_style
   */
  public function register_admin_styles()
  {
    wp_enqueue_style($this->plugin_slug . '-admin-styles', a_tools_url('widgets/at-slider-widget/css/admin.css'));
  }

  /**
   * admin javascript wp_enqueue_script
   */
  public function register_admin_scripts()
  {
    wp_enqueue_script($this->plugin_slug . '-admin-slider', a_tools_url('widgets/at-slider-widget/js/admin.js') , array(
      'wp-color-picker'
    ) , false, true);

    // wp_enqueue_script( $this->get_widget_slug().'-admin-script', a_tools_url( 'widgets/at-slider-widget/'js/admin.js' ), array('jquery') );


  }

  /**
   * public css wp_enqueue_style
   */
  public function register_widget_styles()
  {

    wp_enqueue_style($this->plugin_slug . '-widget-styles', a_tools_url('widgets/at-slider-widget/css/public.css'));
  }

  /**
   * public javascript wp_enqueue_script
   */
  public function register_widget_scripts()
  {

    wp_enqueue_script($this->plugin_slug . '-jcarousel', a_tools_url('widgets/at-slider-widget/js/jquery.jcarousel.min.js') , array(
      'jquery'
    ));
    wp_enqueue_script($this->plugin_slug . '-script', a_tools_url('widgets/at-slider-widget/js/widget.js') , array(
      'jquery'
    ));
  }

  /**
   * スライダー投稿タイプの追加
   *
   */

  public function register_slider_post_type()
  {
    $labels = array(
      'name'        => __('スライダー', 'affiate-tools') ,
      'singular_name'        => __('スライダー', 'affiate-tools') ,
      'add_new'        => __('新しいスライダーを作成', 'affiate-tools') ,
      'add_new_item'        => __('新しいスライダーを登録', 'affiate-tools') ,
      'edit_item'        => __('スライダーを編集', 'affiate-tools') ,
      'new_item'        => __('新しいスライダーを登録', 'affiate-tools') ,
      'all_items'        => __('スライダー設定', 'affiate-tools') ,
      'search_items'        => 'イベント情報を検索',
      'not_found'        => 'スライダーは見つかりませんでした。',
      'not_found_in_trash'        => 'ゴミ箱の中にはありませんでした',
      'parent_item_colon'        => '',
      'menu_name'        => 'スライダー'
    );

    $args   = array(
      'labels' => $labels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => false,
      'hierarchical' => false,
      'supports' => array(
        'title'
      ) ,
      'menu_position' => 100
    );
    register_post_type('atslider', $args);
  }

  /**
   * スライダーウィジェット - オプション
   *
   * @since  0.1.0
   * @return array
   */

  public function slider_widget_option_fields(array $meta_boxes)
  {
    $prefix = '_atslider_';
    $meta_boxes['atslider']        = array(
      'id' => 'at_slider_widget_options',
      'title' => __('スライダー登録', 'affiliate-tools') ,
      'pages' => array(
        'atslider'
      ) ,
      'show_names' => true,
      'context' => 'normal',
      'cmb_styles' => true,
       // Enqueue the affiliate-tools stylesheet on the frontend
      'priority' => 'high',
      'fields' => array(
        array(
          'id' => $prefix . 'slider_repeat_group',
          'type' => 'group',
          'options' => array(
            'add_button' => __('スライダーセットを追加', 'affiliate-tools') ,
            'remove_button' => __('スライダーセットを削除', 'affiliate-tools') ,
            'sortable' => true,
          ) ,
          'fields' => array(
            array(
              'name' => __('スライダー画像を登録', 'affiliate-tools') ,
              'desc' => __('画像をアップロードしてください。', 'affiliate-tools') ,
              'id' => 'slider_image',
              'type' => 'file',
              'allow' => array(
                'url',
                'attachment'
              ) ,
            ) ,
            array(
              'name' => __('リンク先URL', 'affiliate-tools') ,
              'desc' => __('スライダーをクリックした際のリンク先を入力してください。', 'affiliate-tools') ,
              'id' => 'slider_link',
              'type' => 'text',
            ) ,
            array(
              'name' => __('スライダーコンテンツ', 'affiliate-tools') ,
              'desc' => __('スライダーの画像上にコンテンツを挿入出来ます。', 'affiliate-tools') ,
              'id' => 'slider_desc',
              'type' => 'wysiwyg',
            ) ,
            array(
              'name' => __('スライダーカラー', 'affiliate-tools') ,
              'desc' => __('スライダーカラーを選択してください。( タイトル・説明文の背景色が変わります。 )', 'affiliate-tools') ,
              'id' => 'slider_color',
              'type' => 'colorpicker',
              'default' => '#ffffff'
            ) ,
          ) ,
        ) ,
      ) ,
    );
    return $meta_boxes;
  }

  /**
   * slider title save post
   */
  public function slider_save_auto_title_rename($post_id)
  {
    $post_data  = get_post($post_id);
    $post_title = $post_data->post_title;
    if ('atslider' !== $post_data->post_type) return;
    if ($post_title == '')
    {
      remove_action('save_post', array(
        $this,
        'slider_save_auto_title_rename'
      ));
      $save_post = array(
        'ID' => $post_id,
        'post_title' => 'スライダー ID : ' . $post_id
      );
      wp_update_post($save_post);
      add_action('save_post', array(
        $this,
        'slider_save_auto_title_rename'
      ));
    }
  }

  /**
   * custom post update message
   *
   * @param $message (string) 更新時のメッセージ
   * @return $message (string)
   */
  public function slider_custom_postupdate_message($message)
  {
    global $post, $typenow;
    if ( 'atslider' == $post->post_type)
    {
      $message['post'][1] = sprintf(__('スライダーを登録しました。<a href="%s">ウィジェットで設定する</a>') , esc_url(admin_url('/widgets.php')));
    }
    return $message;
  }

  /**
   * custom post update message
   *
   * @param $placeholder (string) プレースホルダー
   * @return $placeholder (string)
   */
  public function change_slider_title_placeholder($placeholder)
  {
    $placeholder = '';
    $screen      = get_current_screen();
    if ('atslider' == $screen->post_type)
    {
      $placeholder = __('スライダーのタイトルを入力してください。', 'affiliate-tools');
    }
    return $placeholder;
  }

  /**
   * 管理画面の表示変更 - パーマリンクを非表示に
   *
   * @return $return : 空
   *
   */
  public function change_post_pearmalink($return)
  {
    global $post;
    if ('atslider' === $post->post_type)
    {
      $return = '';
    }
    return $return;
  }

  /**
   * 管理画面の上部にテキストを表示
   *
   * @return notice
   */

  public function add_slider_post_top($post)
  {
    $html = '';
    if ($post->post_type == 'atslider')
    {
      $html.= '<hr /><div classs="updated below-h2"><p>スライダー作成画面です。ページ下部の「スライダーセットを追加」から、スライダー画像を追加することができます。 </p></div><hr />';
    }
    echo $html;
  }
}
