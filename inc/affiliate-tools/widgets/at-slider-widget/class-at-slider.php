<?php
/**
 * Affiliate Tools.
 *
 * @package   Affilicte_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * ウィジェットの追加
 *
 * @package Affiliate_Tools_Slider_Widget
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */

class Affiliate_Tools_Slider_Widget extends WP_Widget {

	// widget slug
	protected $widget_slug = 'at-slider-widget';

	/**
	 * コンストラクタ
	 *
	 * @return add action
	 */

	public function __construct() {
		parent::__construct(
			$this->get_widget_slug(),
			__( 'スライダー表示ウィジェット', $this->get_widget_slug() ),
			array(
				'class'  => $this->get_widget_slug().'-class',
				'description' => __( 'スライダーウィジェット設定で選択した画像を表示するウィジェットです。', $this->get_widget_slug() )
			)
		);

		// Refreshing the widget's cached output with each new post
		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );


	}
	/**
	 * Return the widget slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_widget_slug() {
		return $this->widget_slug;
	}
	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/
	/**
	 * widget
	 *
	 * @param array   args  The array of form elements
	 * @param array   instance The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		// Check if there is a cached output
		$cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset ( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset ( $cache[ $args['widget_id'] ] ) )
			return print $cache[ $args['widget_id'] ];


		extract( $args, EXTR_SKIP );

		$widget_string = $before_widget;
		ob_start();

		// 管理画面オプションの読み込み
		$slider_obj =  get_post_meta( $instance['slider_id'] , '_atslider_slider_repeat_group' , true );

		include( a_tools_dir( 'widgets/at-slider-widget/views/widget.php' )  );

		$widget_string .= ob_get_clean();
		$widget_string .= $after_widget;
		$cache[ $args['widget_id'] ] = $widget_string;
		wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );
		print $widget_string;

	}


	public function flush_widget_cache() {
		wp_cache_delete( $this->get_widget_slug(), 'widget' );
	}

	/**
	 * widget update.
	 *
	 * @param array   new_instance The new instance of values to be generated via the update.
	 * @param array   old_instance The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['slider_width']       = $new_instance['slider_width']       ? $new_instance['slider_width']       : '100%' ;
		$instance['slider_height']      = $new_instance['slider_height']      ? $new_instance['slider_height']      : '400px' ;
		$instance['slider_auto']        = $new_instance['slider_auto']        ? $new_instance['slider_auto']        : 'true' ;
		$instance['slider_control_nav'] = $new_instance['slider_control_nav'] ? $new_instance['slider_control_nav'] : 'true' ;
		$instance['slider_page_nav']    = $new_instance['slider_page_nav']    ? $new_instance['slider_page_nav']    : 'true' ;
		$instance['slider_id']          = $new_instance['slider_id']          ? $new_instance['slider_id']          : '' ;
		return $instance;
	}

	/**
	 * admin page generate
	 *
	 * @param array   instance The array of keys and values for the widget.
	 */

	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance
		);
		$slider_width        = isset( $instance['slider_width'] )       ? $instance['slider_width']       : '' ;
		$slider_height       = isset( $instance['slider_height'] )      ? $instance['slider_height']      : '' ;
		$slider_auto         = isset( $instance['slider_auto'] )        ? $instance['slider_auto']        : '' ;
		$slider_control_nav  = isset( $instance['slider_control_nav'] ) ? $instance['slider_control_nav'] : '' ;
		$slider_page_nav     = isset( $instance['slider_page_nav'] )    ? $instance['slider_page_nav']    : '' ;
		$slider_id           = isset( $instance['slider_id'] )       ? $instance['slider_id']          : '' ;

		include( a_tools_dir( 'widgets/at-slider-widget/views/admin.php' )  );
	}


}

