<?php
//admin.php
?>
<style>
  .widget-at-slider .dashicons{
    margin-top: 5px;
    width: 15px;
    height: 15px;
    margin-right: 10px;
  }
  .widget-at-slider label{
    margin-right: 10px;
  }
</style>
<div class="widget-at-slider">
<?php
$args = array( 'post_type' =>  'atslider', );
$slider_post = get_posts( $args );
?>
  <hr>
	<?php if ( $slider_post ) {

   /* スライダーを選択 */ ?>
  <label for="<?php echo $this->get_field_id('slider_id'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( 'スライダーIDを選択', $this->get_widget_slug() );?></label>
  <select name="<?php echo $this->get_field_name('slider_id');?>" id="<?php echo $this->get_field_id('slider_id');?>">
    <?php foreach ( $slider_post as $key => $post) { ?>
    <option value="<?php echo esc_attr( $post->ID ); ?>" <?php selected( $post->ID , $slider_id , true ) ?>><?php echo esc_attr( $post->post_title ); ?></option>
    <?php } ?>
  </select>

  <?php } else {
  	echo 'まだスライダーが登録されていないようです。<br> <a href="' . admin_url( "/edit.php?post_type=atslider" ) . '">こちら</a>からスライダー画像を登録してください。';
  } ?>

  <hr>

  <?php /* 幅 */ ?>
	<h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('slider_width'); ?>"><?php echo __( '幅を入力してください', $this->get_widget_slug() );?></label></h4>
  <p>幅を入力してください。(単位 : %)</p>
	<input class="widefat" value="<?php echo $slider_width ; ?>" name="<?php echo $this->get_field_name('slider_width');?>" id="<?php echo $this->get_field_id('slider_width');?>" placeholder="100%" />

  <hr>

  <?php /* 高さ */ ?>
  <h4><div class="dashicons dashicons-admin-tools"></div> <label for="<?php echo $this->get_field_id('slider_height'); ?>"><?php echo __( '高さを入力してください', $this->get_widget_slug() );?></label></h4>
  <p>高さを入力してください。(単位 : px)</p>
  <input class="widefat" value="<?php echo $slider_height ; ?>" name="<?php echo $this->get_field_name('slider_height');?>" id="<?php echo $this->get_field_id('slider_height');?>" placeholder="300px" />

  <hr>

  <?php /* 自動スライド */ ?>
  <label for="<?php echo $this->get_field_id('slider_auto'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( '自動でスライドさせる', $this->get_widget_slug() );?></label>
  <select name="<?php echo $this->get_field_name('slider_auto');?>" id="<?php echo $this->get_field_id('slider_auto');?>">
    <option value="true" <?php selected( 'true' , $slider_auto , true ) ?>>自動</option>
    <option value="false" <?php selected( 'false' , $slider_auto , true ) ?>>固定</option>
  </select>

  <hr>

  <?php /* ナビゲーションの表示 */ ?>
  <label for="<?php echo $this->get_field_id('slider_control_nav'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( 'コントロールナビゲーションの表示', $this->get_widget_slug() );?></label>
  <select name="<?php echo $this->get_field_name('slider_control_nav');?>" id="<?php echo $this->get_field_id('slider_control_nav');?>">
    <option value="true" <?php selected( 'true' , $slider_control_nav , true ) ?>>表示する</option>
    <option value="false" <?php selected( 'false' , $slider_control_nav , true ) ?>>表示しない</option>
  </select>

  <hr>

  <?php /* ページナビゲーションの設定 */ ?>
  <label for="<?php echo $this->get_field_id('slider_page_nav'); ?>"><div class="dashicons dashicons-admin-tools"></div><?php echo __( 'ページナビゲーションの表示', $this->get_widget_slug() );?></label>
  <select name="<?php echo $this->get_field_name('slider_page_nav');?>" id="<?php echo $this->get_field_id('slider_page_nav');?>">
    <option value="true" <?php selected( 'true' , $slider_page_nav , true ) ?>>表示する</option>
    <option value="false" <?php selected( 'false' , $slider_page_nav , true ) ?>>表示しない</option>
  </select>

  <hr>

</div>
