<?php
/**
 * フロント側
 */
?>

<div class="atslider-widget">

<script type="text/javascript">
var $ = jQuery;
(function($) {
  $(function() {
    var jcarousel = $('.jcarousel');
    jcarousel.on('jcarousel:reload jcarousel:create', function () {
	    var width = jcarousel.innerWidth();
      if (width >= 600) {
	      width = width / 3;
	    } else if (width >= 350) {
        width = width / 2;
      }
      jcarousel.jcarousel('items').css('width', width + 'px');
    })
    .jcarousel({
			wrap: 'circular',
	    center: true,
    })<?php if (  $instance['slider_auto'] ) : ?>
    .jcarouselAutoscroll({
	    interval: 3000,
      target: '+=1',
      autostart: true
    });<?php endif;
    // コントロールナビの分岐
    if ( $instance['slider_control_nav'] == 'true') : ?>
    var slide_width = $('.jcarousel ul li').width();
    var control_width = ( $(window).width() - slide_width ) / 2 ;
    $('.jcarousel-control-prev')
      .on('jcarouselcontrol:active', function() {
          $(this).removeClass('inactive');
      })
      .on('jcarouselcontrol:inactive', function() {
          $(this).addClass('inactive');
      })
      .jcarouselControl({
          target: '-=1'
      }).css('width', control_width );

    $('.jcarousel-control-next')
      .on('jcarouselcontrol:active', function() {
          $(this).removeClass('inactive');
      })
      .on('jcarouselcontrol:inactive', function() {
          $(this).addClass('inactive');
      })
      .jcarouselControl({
          target: '+=1'
      }).css('width', control_width );
    $(window).resize(function() {
      var slide_width = $('.jcarousel ul li').width();
      var control_width = ( $(window).width() - slide_width ) / 2 ;
      $('.jcarousel-control-prev').css('width', control_width );
      $('.jcarousel-control-next').css('width', control_width );
    });
    <?php endif ?>

    <?php
    // コントロールナビの分岐
    if ( $instance['slider_page_nav'] == 'true' ) : ?>

    $('.jcarousel-pagination')
      .on('jcarouselpagination:active', 'a', function() {
          $(this).addClass('active');
      })
      .on('jcarouselpagination:inactive', 'a', function() {
          $(this).removeClass('active');
      })
      .jcarouselPagination({
        'item': function( page, carouselItems ) {
	        return '<a href="#' + page + '" class="img-circle"></a>';
	      }
	    });
    <?php endif ?>
  });
})(jQuery);
</script>

<style>
.jcarousel{
  width:  <?php echo $instance['slider_width']  ?>;
  height: <?php echo $instance['slider_height'] ?>;
  background: <?php echo get_theme_mod( 'themecolor_themecolor', '#FFF' ) ?>;
}
.jcarousel ul li{
  height: <?php echo $instance['slider_height'] ?>;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 5px;
  padding-right: 5px;
  box-sizing: border-box;
}
.jcarousel ul li img{
	max-width: 97.8%;

}
</style>


<div class="jcarousel-wrapper">
  <div class="jcarousel">
    <ul>
<?php foreach ( $slider_obj as $key => $slide ) : ?>
      <li class="jcarousel-list">
      <style>
      .slider-contents<?php echo esc_attr( $key );?>{
        <?php $slide_conetnt_bg = ( $slide['slider_color'] ? $slide['slider_color'] : '#FFFFFF' ); ?>
        background-color: <?php $color_obj = new Color( $slide_conetnt_bg );
        $color_rgb_obj = $color_obj->getRgb();
        echo 'rgba(' . $color_rgb_obj['R'] . ', ' . $color_rgb_obj['G'] . ', ' . $color_rgb_obj['B'] . ', 0.8)';
        ?>;
      }
      </style>
      <?php if ( $slide['slider_link'] ): ?>
        <a href="<?php echo esc_url( $slide['slider_link'] );?>" target="_blank">
      <?php endif ?>
        <img src="<?php echo esc_url( $slide['slider_image'] );?>" alt="スライド画像 <?php  echo $key ;?>" />

      <?php if ( $slide['slider_desc'] ): ?>
        <div class="slider-contents slider-contents<?php echo esc_attr( $key );?>" >
          <?php echo  $slide['slider_desc'] ?>
        </div>
      <?php endif ?>

      <?php if (  isset( $slide['slider_link'] ) ): ?>
          </a>
      <?php endif ?>
      </li>
<?php endforeach; ?>
    </ul>

    <?php /** control nav setting **/ ?>
    <?php if ( $instance['slider_control_nav'] == 'true'): ?>
    <a href="#" class="jcarousel-control-prev"><span></span></a>
    <a href="#" class="jcarousel-control-next"><span></span></a>
    <?php endif ?>

    <?php /** page nav setting **/ ?>
    <?php if ( $instance['slider_page_nav'] == 'true'): ?>
    <p class="jcarousel-pagination"></p>
    <?php endif ?>
  </div>
</div>

<?php if ( is_user_logged_in() ) { ?>
<div class="user-edit-button">
  <p class="text-center">
    <a href="<?php echo admin_url( '/post.php?post=' . $instance['slider_id'] . '&action=edit' ) ?>" class="btn btn-sm btn-info"><span class="dashicons dashicons-admin-tools"></span> スライダーを編集</a>
  </p>
</div>
<?php } ?>
</div>
