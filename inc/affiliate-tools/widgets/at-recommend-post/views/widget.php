<?php
/**
 * フロント側
 */

?>

<?php if ( $instance['recommend_title']  ): ?>
	<?php echo $before_title ?>
	<?php echo esc_html( $instance['recommend_title'] ); ?>
	<?php echo $after_title ?>
<?php endif ?>

<div class="<?php echo $this->get_widget_slug(); ?> ">
<?php
  if ( $the_query->have_posts() ) : ?>
  <ul class="at-recommend-posts-ul">
    <?php
    while ( $the_query->have_posts() ) :
      $the_query->the_post(); ?>
    <li>
      <p class="title">
      <?php
      // サムネイル画像・表示・非表示
      $post = get_post();
      if ( $instance['recommend_thumbnail'] == 'true') {
        if ( has_post_thumbnail() ) {
          the_post_thumbnail( array('25','25') ,array( 'class' => 'eye-catch') );
        } else { ?>
          <img src="http://dummyimage.com/25x25/CCC/686a82.gif&text=DS+Style" class="eye-catch" alt="placeholder+image">
        <?php
        }
      } ?>
      <a href="<?php the_permalink();?>">
      <?php
      // 投稿タイトル
      if ( get_the_title() ) { ?>
        <?php the_title();?>
      <?php
      } else { ?>
        タイトルなし
      <?php
      } ?>
      </a>
      </p>
      <?php /* 抜粋文 */ ?>
      <?php
      if ( $instance['recommend_excerpt'] === 'true'){ ?>
         <p class="description"><?php echo  wp_trim_excerpt(); ?></p>
      <?php
      } ?>
    </li>
<?php
    endwhile; ?>
  </ul>
<?php
  else : ?>
  おすすめの投稿が設定されていません。
<?php
  endif; ?>
</div>


