<?php
//admin.php
?>

<div class="widget-at-recommend-post">
	<h4><label for="<?php echo $this->get_field_id('recommend_title'); ?>"><?php echo __( 'タイトル', $this->get_widget_slug() );?></label></h4>
  <p>タイトルを入力してください</p>
	<input class="widefat" value="<?php echo $recommend_title ; ?>" name="<?php echo $this->get_field_name('recommend_title');?>" id="<?php echo $this->get_field_id('recommend_title');?>" />

  <?php /* タイプの選択 */ ?>

  <h4><label for="<?php echo $this->get_field_id('recommend_type'); ?>"><?php echo __( 'おすすめ記事タイプ', $this->get_widget_slug() );?></label></h4>
  <p><a href="<?php echo admin_url('/wp-admin/admin.php?page=at_recommend_option'); ?>">オプションページ</a>で選択した記事、または選択したカテゴリーの記事を表示させるか選択してください。</p>
  <select class="select-recommend-post" name="<?php echo $this->get_field_name('recommend_type');?>" id="<?php echo $this->get_field_id('recommend_type');?>">
    <option value="select" <?php selected( 'select' , $recommend_type , true ) ?>>選択記事を優先</option>
    <option value="category" <?php selected( 'category' , $recommend_type , true ) ?>>カテゴリを優先</option>
  </select>

  <?php /* タイプの選択 */ ?>
  <div class="at-category-select <?php if ( $recommend_type == 'category' ) echo 'show'; ?>">
  <h4><label for="<?php echo $this->get_field_id('recommend_category'); ?>"><?php echo __( 'カテゴリを選択', $this->get_widget_slug() );?></label></h4>
  <?php
  $args = array(
            'show_option_all'    => '',
            'show_option_none'   => '',
            'orderby'            => 'ID',
            'order'              => 'ASC',
            'show_count'         => 0,
            'hide_empty'         => 1,
            'child_of'           => 0,
            'exclude'            => '',
            'echo'               => 1,
            'selected'           => $recommend_category ,
            'hierarchical'       => 0,
            'name'               => $this->get_field_name('recommend_category'),
            'id'                 => $this->get_field_id('recommend_category'),
            'class'              => 'postform',
            'depth'              => 0,
            'tab_index'          => 0,
            'taxonomy'           => 'category',
            'hide_if_empty'      => false,
            'walker'             => ''
          );
  wp_dropdown_categories( $args ); ?>
  </div>


  <?php /* 記事数 */ ?>

  <h4><label for="<?php echo $this->get_field_id('recommend_post_number'); ?>"><?php echo __( '記事数', $this->get_widget_slug() );?></label></h4>
  <p>表示させる記事数を入力してください。</p>
  <input type="number" name="<?php echo $this->get_field_name('recommend_post_number'); ?>" id="<?php echo $this->get_field_id('recommend_post_number'); ?>" value="<?php echo $recommend_post_number;?>" />

  <?php /* 画像の表示 */ ?>

	<h4><label for="<?php echo $this->get_field_id('recommend_thumbnail'); ?>"><?php echo __( 'アイキャッチ画像の表示', $this->get_widget_slug() );?></label></h4>
	<p>アイキャッチ画像を表示させるか選択してください。</p>
  <select name="<?php echo $this->get_field_name('recommend_thumbnail');?>" id="<?php echo $this->get_field_id('recommend_thumbnail');?>">
    <option value="true" <?php selected( 'true' , $recommend_thumbnail , true ) ?>>表示する</option>
    <option value="false" <?php selected( 'false' , $recommend_thumbnail , true ) ?>>表示しない</option>
  </select>

  <?php /* 抜粋文の表示 */ ?>

  <h4><label for="<?php echo $this->get_field_id('recommend_excerpt'); ?>"><?php echo __( '抜粋文の表示', $this->get_widget_slug() );?></label></h4>
  <select name="<?php echo $this->get_field_name('recommend_excerpt');?>" id="<?php echo $this->get_field_id('recommend_excerpt');?>">
    <option value="true" <?php selected( 'true' , $recommend_excerpt , true ) ?>>表示する</option>
    <option value="false" <?php selected( 'false' , $recommend_excerpt , true ) ?>>表示しない</option>
  </select>
</div>
