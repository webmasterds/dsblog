// admin.js
var $ = jQuery;
jQuery(document).ready(function($){
  if ( ! $('.at-category-select').hasClass('show') ) {
    $('.at-category-select').hide();
  };
  $('select.select-recommend-post').change( function() {
    if ( $(this).val() == 'category' ) {
      $('.at-category-select').show();
    } else {
      $('.at-category-select').hide();
    }
  });
  $(document).ajaxSuccess(function(e, xhr, settings) {
    if ( ! $('.at-category-select').hasClass('show') ) {
      $('.at-category-select').hide();
    } else {
      $('.at-category-select').show();
    }
    if ( $('select.select-recommend-post').val() == 'category' ) {
      $('.at-category-select').show();
    } else {
      $('.at-category-select').hide();
    };
    $('select.select-recommend-post').change( function() {
      if ( $(this).val() == 'category' ) {
        $('.at-category-select').show();
      } else {
        $('.at-category-select').hide();
      }
    });
  });
});

