<?php
/**
 * Affiliate Tools.
 *
 * @package   Affilicte_Tools
 * @author    Ishihara Takashi <akeome1369@gmail.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 GrowGroup LLC
 */

/**
 * ウィジェットの追加
 * @package Affiliate_Tools_Recommend_Post_Widget
 * @author  Ishihara Takashi <akeome1369@gmail.com>
 */
class Affiliate_Tools_Recommend_Post_Widget extends WP_Widget {

	// widget slug
	 protected $widget_slug = 'at-recommend-post';

	/**
	 * コンストラクタ
	 *
	 * @return add action
	 */

	public function __construct() {
		parent::__construct(
			$this->get_widget_slug(),
			__( 'おすすめ投稿(商品)ウィジェット', $this->get_widget_slug() ),
			array(
				'class'  => $this->get_widget_slug().'-class',
				'description' => __( '投稿画面で選択したおすすめ記事一覧表示ウィジェットです。', $this->get_widget_slug() )
			)
		);

		// Register admin styles and scripts
		add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		// Register site styles and scripts
		// add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );
		// add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_scripts' ) );

		// Refreshing the widget's cached output with each new post
		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

	}

	/**
	 * Return the widget slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_widget_slug() {
			return $this->widget_slug;
	}

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * widget
	 *
	 * @param array args  The array of form elements
	 * @param array instance The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		// Check if there is a cached output
		$cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset ( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset ( $cache[ $args['widget_id'] ] ) )
			return print $cache[ $args['widget_id'] ];


		extract( $args, EXTR_SKIP );

		$widget_string = $before_widget;
		ob_start();

    // 管理画面オプションの読み込み
    // $select_post_ids =  affiliate_tools_get_option( 'at_recommend_option' , 'at_recommend_option_post_multicheckbox');
		$titan = TitanFramework::getInstance( 'dsblog' );
		$select_post_ids = $titan->getOption( 'affiliate_tools_recommend_post' );

    // 先頭に固定表示
    $sticky = get_option( 'sticky_posts' );
    switch ( $instance['recommend_type'] ) {

      // 選択記事優先の場合
      case 'select':
          if ( $select_post_ids ) {
            $args = array(
                'post__in'       => $select_post_ids,
                'post__not_in'   => $sticky,
                'post_type'      => 'post',
                'posts_per_page' => $instance['recommend_post_number'],
              );
          } else {
            $args = array();
          }
        break;

      // カテゴリーの場合
      case 'category':
            $args = array(
                'post_type'      => 'post',
                'category__in'   => $instance['recommend_category'],
                'posts_per_page' => $instance['recommend_post_number'],
                'widget_id' => '',
              );
        break;

      default:
        # code...
        break;
    }


    $the_query = new WP_Query( $args );

		include( a_tools_dir( 'widgets/at-recommend-post/views/widget.php') );

    // reset psot data
    wp_reset_postdata();
		$widget_string .= ob_get_clean();
		$widget_string .= $after_widget;

		$cache[ $args['widget_id'] ] = $widget_string;

		wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );

		print $widget_string;

	}


	public function flush_widget_cache() {
			wp_cache_delete( $this->get_widget_slug(), 'widget' );
	}

	/**
	 * widget update.
	 *
	 * @param array new_instance The new instance of values to be generated via the update.
	 * @param array old_instance The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
    $instance['recommend_title']       = $new_instance['recommend_title']        ? $new_instance['recommend_title']       : '' ;
    $instance['recommend_type']        = $new_instance['recommend_type']         ? $new_instance['recommend_type']        : 'select' ;
    $instance['recommend_category']    = $new_instance['recommend_category']     ? $new_instance['recommend_category']    : '1' ;
    $instance['recommend_excerpt']     = $new_instance['recommend_excerpt']      ? $new_instance['recommend_excerpt']     : 'false' ;
    $instance['recommend_post_number'] = $new_instance['recommend_post_number']  ? $new_instance['recommend_post_number'] : '5' ;
		$instance['recommend_thumbnail']   = $new_instance['recommend_thumbnail']    ? $new_instance['recommend_thumbnail']   : '' ;
		return $instance;
	}

	/**
	 * admin page generate
	 *
	 * @param array instance The array of keys and values for the widget.
	 */

	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance
		);
		$recommend_title       = ( isset( $instanse['recommend_title'] )       ? $instanse['recommend_title']       : '' );
		$recommend_type        = ( isset( $instanse['recommend_type'] )        ? $instanse['recommend_type']        : '' );
		$recommend_category    = ( isset( $instanse['recommend_category'] )    ? $instanse['recommend_category']    : '' );
		$recommend_excerpt     = ( isset( $instanse['recommend_excerpt'] )     ? $instanse['recommend_excerpt']     : '' );
		$recommend_post_number = ( isset( $instanse['recommend_post_number'] ) ? $instanse['recommend_post_number'] : '' );
		$recommend_thumbnail   = ( isset( $instanse['recommend_thumbnail'] )   ? $instanse['recommend_thumbnail']   : '' );
		include ( a_tools_dir('widgets/at-recommend-post/views/admin.php') );
	}

	/**
	 * admin enqueue_style
	 */
	public function register_admin_styles() {
		wp_enqueue_style( $this->get_widget_slug().'-admin-styles', a_tools_url( 'widgets/at-recommend-post/css/admin.css' ) );
	}

	/**
	 * admin javascript wp_enqueue_script
	 */
	public function register_admin_scripts() {
		wp_enqueue_script( $this->get_widget_slug().'-admin-recommend-post', a_tools_url('widgets/at-recommend-post/js/admin.js' ), array( 'wp-color-picker' ), false, true );
		// wp_enqueue_script( $this->get_widget_slug().'-admin-script', a_tools_url( 'js/admin.js' ), array('jquery') );

	}

	/**
	 * public css wp_enqueue_style
	 */
	public function register_widget_styles() {

		wp_enqueue_style( $this->get_widget_slug().'-widget-styles', a_tools_url( 'widgets/at-recommend-post/css/public.css' ) );

	}

	/**
	 * public javascript wp_enqueue_script
	 */
	public function register_widget_scripts() {

		wp_enqueue_script( $this->get_widget_slug().'-script', a_tools_url( 'widgets/at-recommend-post/js/widget.js' ), array('jquery') );

	}
}

/*
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */

function affiliate_tools_get_option( $option_key , $key = '' ) {
  return cmb_get_option( affiliate_tools_Admin_Page::key( $option_key ) , $key );
}
