<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage スクリプトの埋め込み
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

add_action( 'wp_enqueue_scripts', 'dsblog_script', 100 );
function dsblog_script() {
	// bootstrap
	wp_enqueue_style( 'dsblog_main_bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '' );
	// main
	// wp_enqueue_style('dsblog_main', get_template_directory_uri() . '/assets/css/main.min.css', false, '99972085bc30c435929f5af3cf81d064');
	wp_enqueue_style( 'dsblog_main', get_template_directory_uri() . '/assets/scss/main.scss.php', false );
	// shortcodes
	wp_enqueue_style( 'dsblog_main_shortcodes', get_template_directory_uri() . '/assets/css/shortcodes.min.css', false );
	// plugins
	wp_enqueue_style( 'dsblog_main_plugin', get_template_directory_uri() . '/assets/css/affiliate-tools.min.css', false, '' );
	wp_register_script( 'dsblog_scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), '632995d66dba190b04e58c7bbf9d6222', true );

	if ( ! is_admin()
			 && current_theme_supports( 'jquery-cdn' ) ) {

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false );
		add_filter( 'script_loader_src', 'dsblog_jquery_local_fallback', 10, 2 );

	}

	if ( ( is_page() || is_single() )
	     && comments_open()
	     && get_option( 'thread_comments' ) ) {

		wp_enqueue_script( 'comment-reply' );

	}

	wp_enqueue_script( 'jquery' );
	if ( ! is_admin() ) {
		wp_enqueue_script( 'dsblog_scripts' );
	}
}

// http://wordpress.stackexchange.com/a/12450
add_action( 'wp_head', 'dsblog_jquery_local_fallback' );

function dsblog_jquery_local_fallback( $src, $handle = null ) {
	static $add_jquery_fallback = false;

	if ( $add_jquery_fallback ) {
		echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.0.min.js"><\/script>\')</script>' . "\n";
		$add_jquery_fallback = false;
	}

	if ( $handle === 'jquery' ) {
		$add_jquery_fallback = true;
	}

	return $src;
}
