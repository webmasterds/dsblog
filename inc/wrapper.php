<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ワラッパー 拡張
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

/**
 * Theme wrapper
 *
 * @link http://dsblog.io/an-introduction-to-the-dsblog-theme-wrapper/
 * @link http://scribu.net/wordpress/theme-wrappers.html
 */
function dsblog_template_path() {
	return DSblog_Wrapping::$main_template;
}

function dsblog_sidebar_path() {
	return new DSblog_Wrapping('modules/sidebar.php');
}

class DSblog_Wrapping {
	// Stores the full path to the main template file
	static $main_template;

	// Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
	static $base;

	public function __construct($template = 'base.php') {
		$this->slug = basename($template, '.php');
		$this->templates = array($template);

		if (self::$base) {
			$str = substr($template, 0, -4);
			array_unshift($this->templates, sprintf($str . '-%s.php', self::$base));
		}
	}

	public function __toString() {
		$this->templates = apply_filters('dsblog_wrap_' . $this->slug, $this->templates);
		return locate_template($this->templates);
	}

	static function wrap($main) {
		self::$main_template = $main;
		self::$base = basename(self::$main_template, '.php');

		if (self::$base === 'index') {
			self::$base = false;
		}

		return new DSblog_Wrapping();
	}
}
add_filter('template_include', array('DSblog_Wrapping', 'wrap'), 99);

