<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 基本設定のクリーンアップ
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

define( 'POST_EXCERPT_LENGTH', '140' );

function dsblog_head_cleanup() {
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	global $wp_widget_factory;
	remove_action( 'wp_head', array(
		$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
		'recent_comments_style'
	) );
}

add_action( 'init', 'dsblog_head_cleanup', 10 );


/**
 * Clean up output of stylesheet <link> tags
 */
function dsblog_clean_style_tag( $input ) {
	preg_match_all( "!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches );
	// Only display media if it is meaningful
	$media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';

	return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
}

add_filter( 'style_loader_tag', 'dsblog_clean_style_tag' );

/**
 * Add and remove body_class() classes
 */
function dsblog_body_class( $classes ) {
	// Add post/page slug
	if ( is_single()
	     || is_page()
	        && ! is_front_page() ) {
		$classes[] = basename( get_permalink() );
	}

	// Remove unnecessary classes
	$home_id_class  = 'page-id-' . get_option( 'page_on_front' );
	$remove_classes = array(
		'page-template-default',
		$home_id_class
	);
	$classes        = array_diff( $classes, $remove_classes );

	return $classes;
}

add_filter( 'body_class', 'dsblog_body_class' );

/**
 * Wrap embedded media as suggested by Readability
 *
 * @link https://gist.github.com/965956
 * @link http://www.readability.com/publishers/guidelines#publisher
 */
function dsblog_embed_wrap( $cache, $url, $attr = '', $post_ID = '' ) {
	return '<div class="entry-content-asset">' . $cache . '</div>';
}

add_filter( 'embed_oembed_html', 'dsblog_embed_wrap', 10, 4 );

/**
 * Add Bootstrap thumbnail styling to images with captions
 * Use <figure> and <figcaption>
 *
 * @link http://justintadlock.com/archives/2011/07/01/captions-in-wordpress
 */
function dsblog_caption( $output, $attr, $content ) {
	if ( is_feed() ) {
		return $output;
	}

	$defaults = array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	);

	$attr = shortcode_atts( $defaults, $attr );

	// If the width is less than 1 or there is no caption, return the content wrapped between the [caption] tags
	if ( $attr['width'] < 1 || empty( $attr['caption'] ) ) {
		return $content;
	}

	// Set up the attributes for the caption <figure>
	$attributes = ( ! empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="thumbnail wp-caption ' . esc_attr( $attr['align'] ) . '"';
	$attributes .= ' style="width: ' . ( esc_attr( $attr['width'] ) + 10 ) . 'px"';

	$output = '<figure' . $attributes . '>';
	$output .= do_shortcode( $content );
	$output .= '<figcaption class="caption wp-caption-text">' . $attr['caption'] . '</figcaption>';
	$output .= '</figure>';

	return $output;
}

add_filter( 'img_caption_shortcode', 'dsblog_caption', 10, 3 );

/**
 * Remove unnecessary dashboard widgets
 *
 * @link http://www.deluxeblogtips.com/2011/01/remove-dashboard-widgets-in-wordpress.html
 */
function dsblog_remove_dashboard_widgets() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
}

add_action( 'admin_init', 'dsblog_remove_dashboard_widgets' );

/**
 * Clean up the_excerpt()
 */
function dsblog_excerpt_length( $length ) {
	return POST_EXCERPT_LENGTH;
}

function dsblog_excerpt_more( $more ) {
	return ' &hellip; <a href="' . get_permalink() . '" class="btn btn-sm btn-dimensional">' . __( '続きを読む', 'dsblog' ) . '</a>';
}

add_filter( 'excerpt_length', 'dsblog_excerpt_length' );
add_filter( 'excerpt_more', 'dsblog_excerpt_more' );

/**
 * Remove unnecessary self-closing tags
 */
function dsblog_remove_self_closing_tags( $input ) {
	return str_replace( ' />', '>', $input );
}

add_filter( 'get_avatar', 'dsblog_remove_self_closing_tags' ); // <img />
add_filter( 'comment_id_fields', 'dsblog_remove_self_closing_tags' ); // <input />
add_filter( 'post_thumbnail_html', 'dsblog_remove_self_closing_tags' ); // <img />

/**
 * Don't return the default description in the RSS feed if it hasn't been changed
 */
function dsblog_remove_default_description( $bloginfo ) {
	$default_tagline = 'Just another WordPress site';

	return ( $bloginfo === $default_tagline ) ? '' : $bloginfo;
}

add_filter( 'get_bloginfo_rss', 'dsblog_remove_default_description' );

/**
 * Redirects search results from /?s=query to /search/query/, converts %20 to +
 *
 * @link http://txfx.net/wordpress-plugins/nice-search/
 */
function dsblog_nice_search_redirect() {
	global $wp_rewrite;
	if ( ! isset( $wp_rewrite )
	     || ! is_object( $wp_rewrite )
	     || ! $wp_rewrite->using_permalinks() ) {
		return;
	}

	$search_base = $wp_rewrite->search_base;
	if ( is_search()
	     && ! is_admin()
	     && strpos( $_SERVER['REQUEST_URI'], "/{$search_base}/" ) === false ) {
		wp_redirect( home_url( "/{$search_base}/" . urlencode( get_query_var( 's' ) ) ) );
		exit();
	}
}

if ( current_theme_supports( 'nice-search' ) ) {
	add_action( 'template_redirect', 'dsblog_nice_search_redirect' );
}

/**
 * Fix for empty search queries redirecting to home page
 *
 * @link http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage#post-1772565
 * @link http://core.trac.wordpress.org/ticket/11330
 */
function dsblog_request_filter( $query_vars ) {
	if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
		$query_vars['s'] = ' ';
	}

	return $query_vars;
}

add_filter( 'request', 'dsblog_request_filter' );

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function dsblog_get_search_form( $form ) {
	$form = '';
	locate_template( '/modules/searchform.php', true, false );

	return $form;
}

add_filter( 'get_search_form', 'dsblog_get_search_form' );


