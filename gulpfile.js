// Load plugins
var gulp = require('gulp'),
  zip = require('gulp-zip'),
	plugins = require('gulp-load-plugins')({ camelize: true }),
	lr = require('tiny-lr'),
	server = lr();

// Styles
gulp.task('styles', function() {
  return gulp.src(['assets/scss/main.scss','!assets/scss/bootstrap.scss'])
	.pipe(plugins.rubySass({ style: 'expanded', compass: true , trace: true }))
	.pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
	.pipe(gulp.dest('assets/css'))
	.pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/css'))
	.pipe(plugins.notify({ message: 'Styles task complete' }));
});

gulp.task('styles_bootstrap', function() {
  return gulp.src('assets/scss/bootstrap.scss')
  .pipe(plugins.rubySass({ style: 'expanded', compass: true , trace: true }))
  .pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
  .pipe(gulp.dest('assets/css'))
  .pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
  .pipe(plugins.rename({ suffix: '.min' }))
  .pipe(plugins.livereload(server))
  .pipe(gulp.dest('assets/css'))
  .pipe(plugins.notify({ message: 'Styles task complete' }));
});

gulp.task('styles_shortcodes', function() {
  return gulp.src('assets/scss/shortcodes.scss')
  .pipe(plugins.rubySass({ style: 'expanded', compass: true , trace: true }))
  .pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
  .pipe(gulp.dest('assets/css'))
  .pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
  .pipe(plugins.rename({ suffix: '.min' }))
  .pipe(plugins.livereload(server))
  .pipe(gulp.dest('assets/css'))
  .pipe(plugins.notify({ message: 'Styles task complete' }));
});
// Gulp wp rev
var rev = require("gulp-wp-rev");
	gulp.task('rev', function () {
	  gulp.src('inc/script.php')
    .pipe(rev({
      css: "assets/css/main.min.css",
      cssHandle: "dsblog_main",
      css: "assets/css/bootstrap.min.css",
      cssHandle: "dsblog_main_bootstrap",
      js: "assets/js/scripts.min.js",
      jsHandle: "dsblog_scripts"
	  }))
	    .pipe(gulp.dest('lib'));
});

// Vendor Plugin Scripts
gulp.task('plugins', function() {
  return gulp.src([
                  'assets/js/_*.js',
                  'assets/js/bootstrap/transition.js',
                  'assets/js/bootstrap/alert.js',
                  'assets/js/bootstrap/button.js',
                  'assets/js/bootstrap/carousel.js',
                  'assets/js/bootstrap/collapse.js',
                  'assets/js/bootstrap/dropdown.js',
                  'assets/js/bootstrap/modal.js',
                  'assets/js/bootstrap/tooltip.js',
                  'assets/js/bootstrap/popover.js',
                  'assets/js/bootstrap/scrollspy.js',
                  'assets/js/bootstrap/tab.js',
                  'assets/js/bootstrap/affix.js',
                  'assets/js/bootstrap/affix.js',
                  ])
	.pipe(plugins.concat('scripts.js'))
	.pipe(gulp.dest('assets/js/'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
});

// Site Scripts
gulp.task('scripts', function() {
  return gulp.src(['assets/js/_*.js', '!assets/js/scripts.js'])
	.pipe(plugins.jshint('.jshintrc'))
	.pipe(plugins.jshint.reporter('default'))
	.pipe(plugins.concat('scripts.js'))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src('assets/images/**/*')
	.pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/images'))
	.pipe(plugins.notify({ message: 'Images task complete' }));
});

var DD = new Date();
var zip_file_title = 'archive' +  DD.getYear() + ( DD.getMonth() + 1 ) + DD.getDate() + DD.getHours() + DD.getMinutes() + DD.getSeconds();
gulp.task('zip', function () {
    return gulp.src([
                      '!node_modules',
                      '!.git',
                      '!.sass-cache',
                      '!config.rb',
                      './**/*',
                    ])
        .pipe(zip('../dsblog.zip'))
        .pipe(gulp.dest('./'));
});
// Watch
gulp.task('watch', function() {

  // Listen on port 35729
  server.listen(35729, function (err) {
	if (err) {
	  return console.log(err)
	};

	// Watch .scss files
  // gulp.watch('assets/scss/**/*.scss', ['styles']);
  gulp.watch('assets/scss/**/*.scss', ['styles_bootstrap']);
	// gulp.watch('assets/scss/**/*.scss', ['styles_shortcodes']);

	// Watch .js files
	gulp.watch('assets/js/**/*.js', ['plugins', 'scripts']);

	// Watch image files
	gulp.watch('assets/images/**/*', ['images']);

  });

});

// Default task
gulp.task('default', ['styles', 'styles_bootstrap','styles_shortcodes','plugins', 'scripts', 'images', 'watch', 'rev']);
