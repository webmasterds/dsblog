<?php
/**
 * =====================================================
 * トップページ
 * @package   DS BLOG THEME
 * @author    夢リタ
 * @license   http://creativecommons.org/licenses/by/2.1/jp/
 * @link      http://yumerita.jp/blog
 * @copyright 2014 夢リタ
 * =====================================================
 */
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php
				get_template_part( 'templates/content', 'archive' );
			?>

		<?php endwhile; ?>

		<?php dsblog_paging_nav(); ?>

	<?php else : ?>

		<?php get_template_part( 'content', 'none' ); ?>

	<?php endif; ?>

	</main>
</div>
