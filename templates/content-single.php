<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 投稿ページテンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry_header secondary-border-top">
		<div class="row">
			<h1 class="entry-title col-xs-24 col-lg-16 secondary-color"><?php the_title(); ?></h1>
			<?php if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta col-xs-24 col-lg-8 text-right">
				<?php dsblog_posted_on(); ?>
			</div>
			<?php endif; ?>
		</div>
	</header>
	<p class="entry-author-meta">
		投稿者 :
		<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
			 <?php echo get_the_author();?>
		</a> |
		コメント数 : <a href="<?php comments_link(); ?>"><?php comments_number(); ?></a>
	</p>
	<div class="entry-content">
	<?php if ( has_post_thumbnail() ): ?>

		<div class="thumbnail">
			<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>
		</div>

	<?php endif ?>

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'ページ :', 'dsblog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

		<?php
			$category_list = get_the_category_list( __( ', ', 'dsblog' ) );
			$tag_list = get_the_tag_list( '', __( ', ', 'dsblog' ) );
			if ( ! dsblog_categorized_blog() ) {
				if ( '' != $tag_list ) {
					$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %2$s ', 'dsblog' );
				} else {
					$meta_text = __( '', 'dsblog' );
				}
			} else {
				if ( '' != $tag_list ) {
					$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %1$s | <div class="dashicons dashicons-tag"></div> タグ : %2$s', 'dsblog' );
				} else {
					$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %1$s', 'dsblog' );
				}
			}

			if ( $meta_text ): ?>
	<footer class="entry-footer">
	<?php endif ;
			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		if ( $meta_text ): ?>
	</footer>
	<?php endif ; ?>

	<?php //edit_post_link( __( 'この記事を編集', 'dsblog' ), '<span class="edit-link btn btn-default btn-sm pull-right"><div class="dashicons dashicons-welcome-write-blog"></div> ', '</span>' ); ?>

</article>
