<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 404
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<section class="no-results not-found">
	<header class="page-header entry_header">
		<h2 class="post-title"><?php _e( '記事が見つかりませんでした。', 'dsblog' ); ?></h2>
	</header>

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'dsblog' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( '申し訳ございません。検索キーワードにマッチする記事が見当たりませんでした。もう一度お探しください。', 'dsblog' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( '申し訳ございません。検索キーワードにマッチする記事が見当たりませんでした。もう一度お探しください。', 'dsblog' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
