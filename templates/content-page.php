<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 固定ページ コンテンツテンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header entry_header secondary-border-top ">
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php if ( 'post' == get_post_type() ) : ?>
		<?php endif; ?>
	</header>

	<div class="entry-content">
	<?php if ( has_post_thumbnail() ): ?>
		<div class="thumbnail">
			<?php the_post_thumbnail( $size = 'full', array( 'class' => 'img-responsive' ) ); ?>
		</div>
	<?php endif ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'dsblog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'この記事を編集', 'dsblog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article>
