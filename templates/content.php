<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage コンテンツ ベーステンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry_header clearfix secondary-border-top">
		<h1 class="entry-title col-xs-24 col-lg-16"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta col-xs-24 col-lg-8 text-right">
			<?php dsblog_posted_on(); ?>
		</div>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) :  ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
	<?php if ( has_post_thumbnail() ): ?>

		<div class="thumbnail">
			<?php the_post_thumbnail( $size = 'full', array( 'class' => 'img-responsive' ) ); ?>
		</div>

	<?php endif ?>
		<?php the_content( __( '続きを読む <span class="meta-nav">&rarr;</span>', 'dsblog' ) ); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'ページ :', 'dsblog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-footer">
		<?php if ( 'post' == get_post_type() ) :
				$categories_list = get_the_category_list( __( ', ', 'dsblog' ) );
				if ( $categories_list && dsblog_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'カテゴリー : %1$s', 'dsblog' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php

				$tags_list = get_the_tag_list( '', __( ', ', 'dsblog' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php printf( __( 'タグ : %1$s', 'dsblog' ), $tags_list ); ?>
			</span>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
			<span class="comments-link"><?php comments_popup_link( __( 'コメントする', 'dsblog' ), __( '1 Comment', 'dsblog' ), __( '% Comments', 'dsblog' ) ); ?></span>
		<?php endif; ?>

		<?php edit_post_link( __( '編集', 'dsblog' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
