<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage アーカイブページのコンテンツ
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-group'); ?>>

<?php /** post-header **/ ?>
<header class="entry_header clearfix secondary-border-top ">
	<h2 class="entry-title col-xs-24 col-lg-16 secondary-color">
		<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h2>
	<?php if ( 'post' == get_post_type() ) : ?>
	<div class="entry-meta col-xs-24 col-lg-8 text-right">
		<?php dsblog_posted_on(); ?>
	</div>
	<?php endif; ?>
</header>

<div class="entry-author-meta">
投稿者 : <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"> <?php echo get_the_author(); ?> </a> | コメント数 : <a href="<?php comments_link(); ?>"><?php comments_number(); ?></a>
<?php the_sharebtn_disp(); ?>
</div>

<?php /** post-content **/ ?>
<div class="entry-content row">
	<div class="col-xs-8 col-lg-8">
		<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail( 'large', array( 'class' => 'thumbnail img-responsive' ) );
		} else { ?>
		<img src="<?php echo get_template_directory_uri(). '/assets/img/no-image.jpg';?>" alt="placeholder+image" class="img-responsive thumbnail">
		<?php
		}
		?>
	</div>
	<div class="col-xs-16 col-lg-16">
		<?php echo mb_substr( strip_tags( $post->post_content ), 0, 200); ?>
		<p class="text-right">
		<a href="<?php the_permalink(); ?>" class="btn btn-default more-link btn-dimensional"> 続きを読む</a >
		</p>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'dsblog' ),
				'after'  => '</div>',
			) );
		?>
	</div>
</div>

<footer class="entry-footer">
	<?php
		$category_list = get_the_category_list( __( ', ', 'dsblog' ) );
		$tag_list = get_the_tag_list( '', __( ', ', 'dsblog' ) );
		if ( ! dsblog_categorized_blog() ) {
			if ( '' != $tag_list ) {
				$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %2$s ', 'dsblog' );
			} else {
				$meta_text = __( '', 'dsblog' );
			}
		} else {
			if ( '' != $tag_list ) {
				$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %1$s | <div class="dashicons dashicons-tag"></div> タグ : %2$s', 'dsblog' );
			} else {
				$meta_text = __( '<i class="dashicons dashicons-category"></i> カテゴリー : %1$s', 'dsblog' );
			}
		}
		printf(
			$meta_text,
			$category_list,
			$tag_list,
			get_permalink()
		);
	?>
</footer>

</article><!-- #post-## -->
