<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage フッター テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

global $dsblog_mod;
?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<?php if ( !dynamic_sidebar( 'footer-primary' ) ) : ?>
		<?php endif; ?>
	</div>
</footer><!-- #colophon -->

<section class="footer-copy themecolor-bg">
	<div class="container">
		<div class="site-info text-center">
			<a href="<?php echo site_url('/'); ?>"><?php echo $dsblog_mod['footer_copyright'] ?></a>
		</div>
	</div>
</section>
<?php wp_footer(); ?>
</body>
</html>
