<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage コメント テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */


echo get_avatar($comment, $size = '64'); ?>
<div class="media-body">
	<h4 class="media-heading"><?php echo get_comment_author_link(); ?></h4>
	<time datetime="<?php echo comment_date('c'); ?>"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)); ?>"><?php printf(__('%1$s', 'dsblog'), get_comment_date(),  get_comment_time()); ?></a></time>
	<?php edit_comment_link(__('(編集)', 'dsblog'), '', ''); ?>

	<?php if ($comment->comment_approved == '0') : ?>
		<div class="alert alert-info">
			<?php _e('あなたのコメントは管理者の承認待ちです。', 'dsblog'); ?>
		</div>
	<?php endif; ?>

<?php comment_text(); ?>
<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>

