<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ヘッド テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 Dream夢リタ
 * =====================================================
 */

global $post; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<?php
// OGP 出力
$dsblog = TitanFramework::getInstance( 'dsblog' );
if ( 'true' == $dsblog->getOption( 'ds_blog_post_ogp_disp' ) ) : ?>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<?php  else : ?>
<head>
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--[if lt IE 8]>
<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
