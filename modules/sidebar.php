<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage サイドバー モジュール
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>
<div id="secondary" class="widget-area" role="complementary">
	<?php if ( ! dynamic_sidebar( 'sidebar-primary' ) ) : ?>

	<?php endif; // end sidebar widget area ?>
</div><!-- #secondary -->
