<?php

/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage ヘッダー テンプレート
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */

?>

<header id="masthead" class="site-header" role="banner">
	<div class="site-description themecolor-bg">
		<div class="container">
			<h1 class="h6"><?php bloginfo('description'); ?></h1>
		</div>
	</div>
	<div class="header-inner">
		<div class="container">
			<div class="row">
				<div class="col-xs-24 <?php the_dsblog_header_column('logo'); ?> col-md-16 col-sm-16">
					<a class="" href="<?php echo home_url(); ?>">
						<?php the_dsblog_logo(); ?>
					</a>
				</div>
				<div class="header-widgets col-xs-24 <?php the_dsblog_header_column('logo'); ?> col-md-8 col-sm-16">
					<?php if ( ! dynamic_sidebar( 'header-primary' ) ) : ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php
	if ( 'true' === get_theme_mod( 'layout_navigation', 'false' ) ) { ?>
	<nav class="navbar navbar-<?php echo get_theme_mod( 'navigation_color', 'default' );?>" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar-collapse">
					<span class="sr-only">Toggle </span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<?php
					wp_nav_menu( array(
							'menu'              => 'primary',
							'theme_location'    => 'primary',
							'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'container_id'      => 'header-navbar-collapse',
							'menu_class'        => 'nav navbar-nav',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker()
							)
					);
			?>
			</div>
	</nav>
	<?php } ?>
</header>
