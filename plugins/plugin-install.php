<?php
/**
 * Plugin Install.
 *
 * @package    TGM-Plugin-Activation
 * @version    2.4.0
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/plugin-activate.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the dsblog library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    $plugins = array(
        array(
            'name'      => 'ページビルダー',
            'slug'      => 'siteorigin-panels',
            'required'  => true,
            'source'             => get_stylesheet_directory() . '/plugins/zip/siteorigin-panels.zip',
            'force_activation'   => false,
            'force_deactivation' => false,
        ),
        array(
            'name'      => 'ビジュアルウィジェットエディター',
            'slug'      => 'black-studio-tinymce-widget',
            'required'  => true,
            'source'             => get_stylesheet_directory() . '/plugins/zip/black-studio-tinymce-widget.zip',
            'force_activation'   => false,
            'force_deactivation' => false,
        ),
        array(
            'name'      => 'wp pagenavi',
            'slug'      => 'wp-pagenavi',
            'required'  => true,
            'source'    => 'http://downloads.wordpress.org/plugin/wp-pagenavi.zip',
            'force_activation'   => false,
            'force_deactivation' => false,
        ),
        array(
            'name'      => 'Breadcrumb NavXT',
            'slug'      => 'breadcrumb-navxt',
            'source'    => 'http://downloads.wordpress.org/plugin/breadcrumb-navxt.zip',
            'required'  => true,
        ),
        array(
            'name'      => 'Intuitive Custom Post Order',
            'slug'      => 'intuitive-custom-post-order',
            'source'    => 'http://downloads.wordpress.org/plugin/intuitive-custom-post-order.zip',
            'required'  => false,
        ),
        array(
            'name'      => 'Google XML Sitemaps',
            'slug'      => 'google-sitemap-generator',
            'source'    => 'http://downloads.wordpress.org/plugin/google-sitemap-generator.4.0.5.zip',
            'required'  => false,
        ),
        array(
            'name'      => 'Duplicate Post',
            'slug'      => 'duplicate-post',
            'source'    => 'http://downloads.wordpress.org/plugin/duplicate-post.2.6.zip',
            'required'  => false,
        ),
        array(
            'name'      => 'Duplicate Post',
            'slug'      => 'duplicate-post',
            'source'    => 'http://downloads.wordpress.org/plugin/duplicate-post.2.6.zip',
            'required'  => false,
        ),

    );

    $config = array(
        'id'           => 'dsblog',
        'default_path' => '',
        'menu'         => 'dsblog-install-plugins',
        'has_notices'  => true,
        'dismissable'  => true,
        'dismiss_msg'  => '',
        'is_automatic' => true,
        'message'      => '',
        'strings'      => array(
            'page_title'                      => __( 'インストールが必要なプラグイン', 'dsblog' ),
            'menu_title'                      => __( '必須プラグイン', 'dsblog' ),
            'installing'                      => __( 'プラグイン: %s', 'dsblog' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'dsblog' ),
            'notice_can_install_required'     => _n_noop( 'このテーマには、次のプラグインが必要です :  %1$s.', 'このテーマには、次のプラグインが必要です : %1$s.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'おすすめのプラグインをインストールすることができます。', 'dsblog' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( '次のプラグインがテーマに必要です。 : %1$s.', '次のプラグインがテーマに必要です。: %1$s.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( '次のプラグインがおすすめのプラグインとして指定されています : %1$s.', '次のプラグインがおすすめのプラグインとして指定されています : %1$s.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( '申し訳ございません。あなたには %s プラグインをインストールする権限がありません。プラグインを設定するためには、サイトの管理者にご連絡ください。', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'dsblog' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'dsblog' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'プラグインのインストールを始める', 'プラグインのインストールを始める', 'dsblog' ),
            'activate_link'                   => _n_noop( 'プラグインの有効化を始める', 'プラグインの有効化を始める', 'dsblog' ),
            'return'                          => __( 'プラグインインストーラーに戻る', 'dsblog' ),
            'plugin_activated'                => __( 'プラグインの有効化に成功しました。', 'dsblog' ),
            'complete'                        => __( 'すべてのプラグインのインストールと有効化が完了しました。 : %s', 'dsblog' ), // %s = dashboard link.
            'nag_type'                        => 'updated',
            'dismiss'                         => __( 'このお知らせを削除', 'dsblog' ),
		        'activated_successfully'          => __( '次のプラグインの有効化に成功しました。 : ', 'dsblog' ),
        )
    );

    tgmpa( $plugins, $config );
}
