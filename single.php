<?php
/**
 * =====================================================
 * @package    DS BLOG THEME
 * @subpackage 投稿ページ
 * @author     夢リタ
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * @link       http://yumerita.jp/blog
 * @copyright  2014 夢リタ
 * =====================================================
 */
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'templates/content', 'single' ); ?>

		<?php dsblog_post_nav(); ?>

		<?php
			if ( 'true' === get_theme_mod( 'comment_disp', 'false' ) && ( comments_open() || '0' != get_comments_number() ) ) :
				comments_template('/modules/comments.php');
			endif;
		?>
	<?php endwhile; ?>

	</main>
</div>
