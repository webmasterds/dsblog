# DS Blog - WordPress Themes -

## テーマカスタマイザー使用箇所

* 基本的な設定
	* キーワード設定
		* general_keywords
	* Google analytics コード
		* general_analytics_code

* ロゴの設定
	* ロゴの選択
		* logo_choice
			* text
			* image
	* ロゴ画像アップロード
		* logo_image_uoload
	* ロゴの大きさ
		* logo_size
			* small
			* normal
			* large
	* ロゴの位置
		* logo_align
			* left
			* center
			* right
	* テキスト時のロゴカラー

* テーマカラーの設定
	* テーマカラーの設定
		* themecolor_themecolor
	* プライマリーの設定 ( ボタン・リンク )
		* themecolor_primary
	* 補助色の設定 ( ボタン・リンク )
		* themecolor_secondary

* 背景の設定
	* 背景色のせってい
		* background_color
	* 背景画像アップロード
		* background_image_upload

* レイアウト設定

	* トップページのレイアウト設定
		* layout_top
			* right
			* left
			* one_column

	* 下層ページのレイアウト設定
		* layout_layer
			* right
			* left
			* one_column

* フォント設定

	* ベースベースサイズの設定
		* font_base_size
	* 行間の設定
		* font_line_height
	* 字間の設定
		* font_letter_space

* コメントの設定
	* コメント表示の設定
		* comment_disp
			* true
			* false
	* Facebookコメントを使用しますか？
		* comment_facebook
			* true
			* false
* フッターの設定
	* footer_copyright
	* footer_pagetop_disp
